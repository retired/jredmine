###
# #%L
# JRedmine :: Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Lesser Public License for more details.
# 
# You should have received a copy of the GNU General Lesser Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/lgpl-3.0.html>.
# #L%
###
ActionController::Routing::Routes.draw do |map|

  map.with_options :controller => 'jredmine' do |red|

    # jredmine : actions with no project context
    red.connect 'jredmine/:action.xml' , :action => ['get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'], :format => 'xml'
    red.connect 'jredmine/:action.json', :action => ['get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'], :format => 'json'
    red.connect 'jredmine/:action'     , :action => ['ping', 'login', 'logout', 'get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'], :format => 'xml'
    
    # jredmine/action/:pid : actions with project context
    red.connect 'jredmine/:action.xml/:pid' , :pid => /.+/, :format => 'xml'
    red.connect 'jredmine/:action.json/:pid', :pid => /.+/, :format => 'json'
    red.connect 'jredmine/:action/:pid'     , :pid => /.+/, :format => 'xml'

  end

  # jredmine: ping, login , logout, with no project context...
  #map.connect 'jredmine/:action', :controller => 'jredmine' ,:action => ['ping', 'login', 'logout', 'get_projects', 'get_issue_statuses', 'get_issue_priorities']
  #map.connect 'jredmine/:action.:format', :controller => 'jredmine' ,:action => ['get_projects', 'get_issue_statuses', 'get_issue_priorities'], :format => ['xml', 'json']

  # jredmine/action/?? (:project_id)
  #map.connect 'jredmine/:action/:project_id', :controller => 'jredmine'
  #map.connect 'jredmine/:action.:format/:project_id', :controller => 'jredmine', :format => ['xml', 'json']
end
