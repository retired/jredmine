###
# #%L
# JRedmine :: Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Lesser Public License for more details.
# 
# You should have received a copy of the GNU General Lesser Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/lgpl-3.0.html>.
# #L%
###
RedmineApp::Application.routes.draw do

  # jredmine : actions with no project context
  match 'jredmine/:action.xml',  :to => 'jredmine#:action', :action => ['get_user', 'get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'],         :format => 'xml',  :via => ['get']
  match 'jredmine/:action.json', :to => 'jredmine#:action', :action => ['get_user', 'get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'],         :format => 'json', :via => ['get']
  match 'jredmine/:action',      :to => 'jredmine#:action', :action => ['login'],                                                                                                                  :format => 'xml',  :via => ['post']
  match 'jredmine/:action',      :to => 'jredmine#:action', :action => ['get_user', 'ping', 'logout', 'get_projects', 'get_user_projects', 'get_issue_statuses', 'get_issue_priorities', 'get_enumeration'], :format => 'xml',  :via => ['get']

  # jredmine/action/:pid : actions with project context
  match 'jredmine/:action.xml/:pid',  :to=> 'jredmine#:action', :pid => /.+/, :format => 'xml',  :via => ['get', 'post']
  match 'jredmine/:action/:pid',      :to=> 'jredmine#:action', :pid => /.+/, :format => 'xml',  :via => ['get', 'post']
  match 'jredmine/:action.json/:pid', :to=> 'jredmine#:action', :pid => /.+/, :format => 'json', :via => ['get', 'post']

end
