###
# #%L
# JRedmine :: Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Lesser Public License for more details.
# 
# You should have received a copy of the GNU General Lesser Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/lgpl-3.0.html>.
# #L%
###
class JredmineController  < ActionController::Base

  # post request
  before_filter :check_post, :only => [:add_version, :update_version, :next_version, :add_news, :add_attachment, :add_version_attachment, :add_issue_time, :update_issue_time]

  # try to get user from request (if found, then put it in User.current_user)
  before_filter :try_find_current_user

  # check user exists
  before_filter :check_current_user, :only => [:get_user, :get_user_projects, :add_version, :update_version, :next_version, :add_news, :add_attachment, :add_version_attachment, :add_issue_time, :update_issue_time]

  # find project and check permission
  before_filter :find_project, :except => [:ping, :login, :logout, :get_user, :get_projects, :get_user_projects, :get_issue_statuses, :get_issue_priorities, :get_enumeration]

  # find project's version
  before_filter :find_version, :only => [:get_version, :get_version_issues, :get_version_opened_issues, :get_version_attachments, :add_version_attachment]

  # check version definition is ok in request
  before_filter :check_version_definition, :only => [:add_version, :update_version, :next_version]

  # find project's issue
  before_filter :find_issue, :only => [:get_issue_times, :add_issue_time, :update_time_issue]

  # remove User.current
  after_filter :remove_current_user

  # ping service (just to test if service is reachable)
  def ping
    render :text => "ping", :status => 200  
  end

  # Login request and validation
  # Deprecated since 1.7, will be removed in version 2.0
  def login
    if !request.post?
      # Logout user : get method not possible
      self.logged_user = nil
      render_status 405, "POST method required"
    else
      # Authenticate user
      #user = User.try_to_login(params[:username], params[:password])
      user = find_current_user
      if user.nil?
        # Invalid credentials
        render_status 401, "Invalid credentials"
        #elsif user.new_record?
        #  # Onthefly creation failed,
        #  render_status 401, "User not activated"
      else
        # Valid user
        self.logged_user = user
        # always generate a key and set autologin cookie
        token = Token.create(:user => user, :action => 'autologin')
        cookies[:autologin] = { :value => token.value, :expires => 1.year.from_now }
        render_status 200, "User logged in"
      end
    end
  end

  # Log out current user and redirect to welcome page
  # Deprecated since 1.7, will be removed in version 2.0
  def logout
    cookies.delete :autologin
    Token.delete_all(["user_id = ? AND action = ?", User.current.id, 'autologin']) if User.current.logged?
    # user is no more connected
    self.logged_user = nil
    render_status 200, "User logged out"
  end

  # recuperation des projets
  def get_projects
    @projects = Project.all().to_a
    render_array_result @projects, "projects"
  end

  # recuperation des projets dont l'utilisateur connecte est membre
  def get_user_projects
    projectsIds = User.current.memberships.collect{|m| m.project_id}
    result = []
    for id in projectsIds
      p = @project = Project.find(id)
      result << p
    end
    render_array_result result, "projects"
  end

  def get_user
    render_result User.current
  end

  # recuperation des priorities d'issues
  def get_issue_priorities
    get_enumeration "IssuePriority"
  end
  
  # recuperation des differentes categories d'issues
  def  get_issue_categories
    @issue_categories = @project.issue_categories.all().to_a
    render_array_result @issue_categories, "issue-categories"
  end

  # recuperation des differentes statuts d'issues
  def  get_issue_statuses
    @issue_status = IssueStatus.all().to_a
    render_array_result @issue_status, "issue-statuses"
  end  
  # recuperation du projet
  def get_project
    render_result @project    
  end

  # recuperation de tous les membres d'un projet
  def  get_project_users
    @members = @project.members.all().to_a
    @r = []
    for m in @members
      u = User.find(m[:user_id])
      # u[:roles] = m[:roles] FIXME No more exists in Redmine 3.0.0
      # u[:member_id] = m[:id] FIXME No more exists in Redmine 3.0.0
      #TODO - should remove the hashed password ?
      @r << u
    end
    render_array_result @r, "users"
  end

  # recuperation de toutes les annonces d'un projet
  def  get_project_news
    @news = @project.news.all().to_a
    render_array_result @news, "news"
  end

  # recuperation des trakers d'un projet
  def  get_project_trackers
    @trackers = @project.trackers.all().to_a
    render_array_result @trackers, "trackers"
  end
 
  # recuperation des versions d'un projet
  def  get_project_versions
    @versions = @project.versions.all().to_a
    render_array_result @versions, "versions"
  end

  # recuperation des versions fermees d'un projet
  def  get_project_closed_versions
    @versions = @project.versions.where( status: 'closed').to_a
    render_array_result @versions, "versions"
  end

  # recuperation de toutes les issues d'un projet
  def  get_project_issues
    r =@project.issues.all().to_a
    render_array_result r, "issues"
  end

  # recuperation de toutes les issues ouvertes d'un projet
  def  get_project_opened_issues
    r = []
    for i in @project.issues.all().to_a
      if !i.closed? && i.status_id != 3
        r << i
      end    
    end
    render_array_result r, "issues"
  end

  # recuperation de toutes les issues fermees d'un projet
  def  get_project_closed_issues
    r = []
    for i in @project.issues.all().to_a
      if i.closed? || i.status_id == 3
        r << i
      end
    end
    render_array_result r, "issues"
  end

  # recuperation des temps d'une issue
  def get_issue_times
    render_array_result @issue.time_entries, "time-entries"
  end

  # recuperation de la version d'un projet
  def  get_version    
    render_result @version
  end
  
  # recuperation des issues d'un projet pour une version donnee
  def  get_version_issues
    @issues = @version.fixed_issues.all().to_a
    render_array_result @issues, "issues"
  end

  # recuperation des issues ouvertes d'un projet pour une version donnee
  def  get_version_opened_issues
    issues = @version.fixed_issues.all().to_a
    result = []
    issues.each do |issue|
      if !issue.closed? && issue.status_id != 3
          result << issue
      end
    end
    render_array_result result, "issues"
  end
  
  # recuperation des pieces jointes d'un projet pour une version donnee
  def get_version_attachments
    @files = @version.attachments.all().to_a
    render_array_result @files, "attachments"
  end

  def  show_permissions
    allowed_permissions
    render_result @allowed_permissions
  end

  def  show_actions
    allowed_actions
    render_result @allowed_actions
  end

  # add a new version for a given project
  def add_version(version = params["version"])
    @version = @project.versions.find_by_name(version[:name])
    if @version
      # version already exists
      render_status 505, "Version #{@version.name} already exists for project #{@project.name}, can not create it"
      return false
    end
    allowed = User.current.allowed_to?(:manage_versions, @project)
    if !allowed
      render_status 401, "No permission to add a version in project #{@project.name}"
      return false
    end

    # create the new version
    @version = Version.create(:project => @project, :name => version[:name])

    # do update the version
    if !update_version0(version)
      # something was wrong
      return false
    end
    render_result @version
  end

  # update a existing version for a given project
  def update_version(version=params["version"])
    # get version
    @version = @project.versions.find_by_name(version[:name])
    if !@version
      render_status 404, "#{version['name']} is not a version for project #{@project.name}"
      return false
    end
    # check permissions
    allowed = User.current.allowed_to?({:controller => 'versions', :action => "edit"}, @project)
    if !allowed
      render_status 401, "No permission to edit a version on project #{@project.name}"
      return false
    end
    # do update the version
    if !update_version0(version)
      # something was wrong
      return false
    end
    render_result @version
  end

  # add or update a new version for a given project and move all opened issue
  # to next version from a preivous version
  def next_version(version = params["version"],old_version_name=params[:oldVersionName])
    # get version
    @version = @project.versions.find_by_name(version[:name])
    if !@version
      # version does does exists
      allowed = User.current.allowed_to?(:manage_versions, @project)
      if !allowed
        render_status 401, "No permission to add a version on project #{@project.name}"
        return false
      end

      # create the new version
      @version = Version.create(:project => @project, :name => version[:name])
    end

    # check update permission
    allowed = User.current.allowed_to?({:controller => 'versions', :action => "edit"}, @project)
    if !allowed
      render_status 401, "No permission to edit a version on project #{@project.name}"
      return false
    end

    # do update the version
    if !update_version0(version)

      # something was wrong while updating the version
      return false
    end

    if old_version_name
      old_version = @project.versions.find_by_name(old_version_name)
      if !old_version
        render_status 505, "Could not find old version #{old_version_name} for project #{@project.name}"
        return false
      end
      old_issues = old_version.fixed_issues.all().to_a
      old_issues.each do |issue|
        sid = issue.status_id
        if !issue.closed? || ( sid == 1 || sid == 2 )
          journal = issue.init_journal(User.current, params[:notes])

          # move to new version
          issue.fixed_version_id = @version[:id]
          issue.save
          #Mailer.deliver_issue_edit(journal) if Setting.notified_events.include?('issue_updated')
        end
      end
    end
    render_result @version
  end

  # add a new news for a given project
  def add_news(news = params["news"])
    if !request.post?
      render_status 405, "POST method required for action add_news"
      return false
    end
    allowed = User.current.allowed_to?({:controller => 'news', :action => "new"}, @project)
    if !allowed
      msg = "No permission to add a news on project "+ @project.name
      render_status 401, msg
    else
      @news = News.new(:project => @project, :author => User.current)
      @news.attributes = news
      if @news.save
        Mailer.deliver_news_added(@news) if Setting.notified_events.include?('news_added')
        render_result @news
      else
        render_status 505, "Could not add the news..."
      end
    end
  end

  def add_version_attachment
    add_attachment
  end

  def add_attachment(attachment = params["attachment"])
    if !request.post?
      render_status 405, "POST method required for action add_attachment"
      return false
    end
    container = !@version ? @project : @version
    if attachment
      file = attachment['file']
      if file && file.size > 0
        a = Attachment.new(:container => container,
          :file => file,
          :description => attachment['description'].to_s.strip,
          :author => User.current)
      end
    end
    if a.save
      if Setting.notified_events.include?('file_added')
        array =[]
        array << a
        Mailer.deliver_attachments_added(array)
      end
    else
      render_status 505, "Could not save the file " + attachment.to_s
    end
    render_result a
  end

  def add_issue_time(timeEntry = params["time_entry"])
    if !request.post?
      render_status 405, "POST method required for action add_issue_time"
      return false
    end
    allowed = User.current.allowed_to?(:log_time, @project)
    if !allowed
      msg = "No permission to add a issue time entry on project "+ @project.name
      render_status 401, msg
    end
    @notes = params[:notes]
    journal = @issue.init_journal(User.current, @notes)
    @time_entry = TimeEntry.new(:project => @project, :issue => @issue, :user => User.current, :spent_on => Date.today)
    @time_entry.attributes = timeEntry

    call_hook(:controller_issues_edit_before_save, { :params => params, :issue => @issue, :time_entry => @time_entry, :journal => journal})

    if (@time_entry.hours.nil? || @time_entry.valid?) && @issue.save
      # Log spend time
      @time_entry.save
    else
      render_status 505, "Could not save the time entry " + @time_entry
    end
    if !journal.new_record?
      # Only send notification if something was actually changed
      Mailer.deliver_issue_edit(journal) if Setting.notified_events.include?('issue_updated')
    end
    render_result @time_entry
  end

  def update_issue_time(timeEntry = params["time_entry"])
    if !request.post?
      render_status 405, "POST method required for action update_issue_time"
      return false
    end
    
  end

  # recuperation des valeurs d'une enumeration
  def get_enumeration(type = params[:type])
    #@result = Enumeration.get_values(type)
    @result = Enumeration.where("type = ?", type)
    render_array_result @result, "enumerations"
  end
  
  private

  # Checks that action is using post method
  def check_post
    if !request.post?
      render_status 405, "POST method required for action #{params[:action]}"
    end
  end

  # Checks that request has a version[name] attribute
  def check_version_definition(version = params[:version])
    if version.nil?
      render_status 404, "No version definition in request"
    else
       if version[:name].nil?
         render_status 404, "No version name given in request"
       end
    end
  end

  # Check if User.current is loggued
  def check_current_user
    if User.current.anonymous?
      render_status 401, "No user connected"
    end
  end

  # Clean current user
  def remove_current_user
    User.current = nil
  end

  # Returns the current user or nil if no user is logged in
  def try_find_current_user
    # Check the settings cache for each request
    Setting.check_cache
    # Find the current user
    user = nil
    if Setting.rest_api_enabled?
      if (key = api_key_from_request)
        # Use API key
        user = User.find_by_api_key(key)
      else
        # HTTP Basic, either username/password or API key/random
        authenticate_with_http_basic do |username, password|
          user = User.try_to_login(username, password) || User.find_by_api_key(username)
        end
      end
    end
    if user.nil?
      # Last chance using request params
      user = User.try_to_login(params[:username], params[:password])
    end
    User.current = user
  end

  # Returns the current user or nil if no user is logged in
  # Deprecated since 1.7, will be removed in version 2.0
  def find_current_user
    user = nil
    if session[:user_id]
      # existing session
      user = (User.active.find(session[:user_id]) rescue nil)
    elsif cookies[:autologin]
      # auto-login feature
      user = User.find_by_autologin_key(cookies[:autologin])
    end
    if user.nil? && Setting.rest_api_enabled?
      if (key = api_key_from_request)
        # Use API key
        user = User.find_by_api_key(key)
      else
        # HTTP Basic, either username/password or API key/random
        authenticate_with_http_basic do |username, password|
          user = User.try_to_login(username, password) || User.find_by_api_key(username)
        end
      end
    end
    if user.nil?
      # Last chance using request params
      user = User.try_to_login(params[:username], params[:password])
    end
    user
  end

  # Returns the API key present in the request
  def api_key_from_request
    if params[:key].present?
      params[:key].to_s
    elsif request.headers["X-Redmine-API-Key"].present?
      request.headers["X-Redmine-API-Key"].to_s
    end
  end

  def find_project( pid = params[:pid] )
    begin
      @project = Project.find(pid)
      allowed = User.current.allowed_to?({:controller => 'jredmine', :action => "allow_jredmine"}, @project)
      if !allowed
        render_status 401, "No permission to access project #{pid} nor jredmine service configured for this project"
      end
    rescue ActiveRecord::RecordNotFound
      render_status 404, "#{pid} is not a project"
      return false
    end
  end

  def check_edit
    # check if user can edit the project
    allowed = User.current.allowed_to?({:controller => 'projects', :action => "edit"}, @project)
    if !allowed
      render_status 401, "No permission to edit the project"
    end
  end

  def find_version(version = params[:version_name])
    if version.nil?
      render_status 404, "No version name given in request"
    else
      @version = @project.versions.find_by_name(version)
      if !@version
        render_status 404, "#{version} is not a version for project #{@project.name}"
      end
    end
  end

  def find_issue(issue = params[:issue_id])
    @issue = @project.issues.find(issue)
    if !@issue
      render_status 404, "#{issue} is not an issue for project #{@project.name}"
    end
  end

  # Update the version with the pa given in parameters.
  # To use this method the @project and @version must defined
  def update_version0(version)
    attributes = version.dup
    attributes.delete('sharing') unless @version.allowed_sharings.include?(attributes['sharing'])
    begin
      valid = @version.update_attributes(attributes)
      if !valid
        render_status 505, "Could not update the version #{@version.name} on project #{@project.name} for unknown reason..."
      end
    rescue => msg
      valid = false
      render_status 505, "Could not update the version #{@version.name} on project #{@project.name} for reason #{msg}"
    end
    valid
  end

  def render_status(code,message)
    render :text => message, :status => code
  end

  def render_array_result(result,tag)
    if !result.any?
      respond_to do |format|
        format.json { render :text => "[]" }
        format.xml  { render :text => "<"+tag+"></"+tag+">" }
      end
    else
      respond_to do |format|
        format.json { render :text => result.to_json }
        format.xml  { render :text => result.to_xml }
      end
    end
  end
  
  def render_result(result)
    respond_to do |format|
      format.json { render :text => result.to_json }
      format.xml { render :text => result.to_xml }
    end
  end
  
  def logged_user=(user)
    if user && user.is_a?(User)
      User.current = user
      session[:user_id] = user.id
    else
      User.current = User.anonymous
      session[:user_id] = nil
    end
  end

  def allowed_permissions
    @allowed_permissions ||= begin
      module_names = @project.enabled_modules.collect {|m| m.name}
      Redmine::AccessControl.modules_permissions(module_names).collect {|p| p.name}
    end
  end
  
  def allowed_actions
    @allowed_actions ||= allowed_permissions.inject([]) { |actions, permission| actions += Redmine::AccessControl.allowed_actions(permission) }.flatten
  end

end
