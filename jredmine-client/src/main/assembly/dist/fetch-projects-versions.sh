#!/bin/sh

java  -Dlog4j.configuration=file://$(pwd)/log4j.properties -jar ${project.build.finalName}.jar --fetch-project-versions "$@"
