package org.nuiton.jredmine.model.io.xpp3.api;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.xml.pull.XmlPullParser;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import java.util.Set;

/**
 * A abstract object to map an xml value (tag or attribute, or esle?) to a pojo property.
 *
 * Two implementations are given :
 *
 * <ul>
 * <li>{@link TagTextContentToProperty} to map the text content of a tag
 * to a pojo's property</li>
 * <li>{@link AttributeValueToProperty} to map the text content of a tag
 * to a pojo's property</li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
public abstract class PropertyMapper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PropertyMapper.class);

    /** name of tag. */
    protected final String name;

    /** the pojo's property to set */
    protected final String propertyName;

    /** the converter from xml to pojo's property type */
    protected final DataConverter type;

    /** the type of the pojo container of the property */
    protected final Class<?> containerType;

    /** the pojo's property descriptor */
    protected final PropertyDescriptor descriptor;

    /**
     * a flag to check to use only once the mapper. (should not be used for
     * attributes implementations).
     */
    protected final boolean onlyOne;

    protected PropertyMapper(String tagName,
                             String propertyName,
                             Class<?> containerType,
                             DataConverter type,
                             boolean onlyOne,
                             PropertyDescriptor descriptor) {
        name = tagName;
        this.propertyName = propertyName;
        this.type = type;
        this.onlyOne = onlyOne;
        this.containerType = containerType;
        this.descriptor = descriptor;
    }

    protected String getXmlName() {
        return name;
    }

    protected abstract Object getDataFromXml(XmlPullParser parser)
            throws Exception;

    public void setProperty(Object src,
                            XmlPullParser parser,
                            Set<String> parsed, boolean strict)
            throws XmlPullParserException, IOException {
        if (onlyOne && parsed.contains(getXmlName())) {
            throw new XmlPullParserException(
                    "Duplicated tag: \'" + parser.getName() + "\'",
                    parser,
                    null
            );
        }
        parsed.add(getXmlName());
        try {
            if (log.isDebugEnabled()) {
                log.debug(getXmlName());
            }
            Object r = getDataFromXml(parser);
            if (r != null) {
                descriptor.getWriteMethod().invoke(src, r);
            }
        } catch (XmlPullParserException e) {
            throw e;
        } catch (NumberFormatException e) {
            if (strict) {
                throw new XmlPullParserException(e.getMessage());
            }
        } catch (ParseException e) {
            if (strict) {
                throw new XmlPullParserException(e.getMessage());
            }
        } catch (Exception e) {
            throw new XmlPullParserException(e.getMessage(), parser, e);
        }
    }

    public PropertyDescriptor getDescriptor() {
        return descriptor;
    }

    public boolean isOnlyOne() {
        return onlyOne;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getTagName() {
        return name;
    }

    public DataConverter getType() {
        return type;
    }

    public Class<?> getContainerType() {
        return containerType;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        ToStringBuilder toStringBuilder = new ToStringBuilder(this);
        toStringBuilder.append("name", name);
        toStringBuilder.append("propertyName", propertyName);
        toStringBuilder.append("onlyOne", onlyOne);
        toStringBuilder.append("type", type);
        toStringBuilder.append("containerType", containerType);
        return toStringBuilder.toString();
    }

    public static final Function<String, String> TAG_NAME_TRANSFORMER = new Function<String, String>() {
        @Override
        public String apply(String input) {
            String[] parts = input.split("-");
            StringBuilder buffer = new StringBuilder();
            for (int i = 0, j = parts.length; i < j; i++) {
                if (i == 0) {
                    buffer.append(parts[i]);
                } else {
                    buffer.append(StringUtils.capitalize(parts[i]));
                }
            }
            parts = buffer.toString().split("_");
            buffer = new StringBuilder();
            for (int i = 0, j = parts.length; i < j; i++) {
                if (i == 0) {
                    buffer.append(parts[i]);
                } else {
                    buffer.append(StringUtils.capitalize(parts[i]));
                }
            }
            String result = buffer.toString();
            return result;
        }
    };

    public static String getMapperForTag(Class<?> type) {
        return type.getName() + "#";
    }

    public static String getMapperForAttribute(Class<?> type, String attributeName) {
        return type.getName() + "#" + attributeName + "#";
    }

    protected static PropertyDescriptor getDescriptor(Class<?> containerType,
                                                      String propertyName) throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(containerType);
        PropertyDescriptor descriptor = null;
        for (PropertyDescriptor propertyDescriptor :
                beanInfo.getPropertyDescriptors()) {
            if (propertyDescriptor.getName().equals(propertyName)) {
                descriptor = propertyDescriptor;
                break;
            }
        }
        if (descriptor == null) {
            throw new IllegalArgumentException(
                    "could not find a property descriptor for property " +
                    propertyName + " of " + containerType);
        }
        return descriptor;
    }

    protected void registerMapper(Map<String, PropertyMapper> allMappers,
                                  Class<?> containerType,
                                  String tagName,
                                  String attributeName) {
        String key;
        if (attributeName == null) {
            // tag scope
            key = getMapperForTag(containerType) + tagName;
        } else {
            // attribute scope
            key = getMapperForAttribute(containerType, tagName) + attributeName;
        }
        allMappers.put(key, this);
    }
}
