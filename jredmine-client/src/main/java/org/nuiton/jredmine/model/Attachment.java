/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

import java.io.File;
import java.util.Date;

/**
 * A file attachment from redmine server.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class Attachment implements IdAble {

    protected int id;

    protected int authorId;

    protected int containerId;

    protected int filesize;

    protected int downloads;

    protected Date createdOn;

    protected String containerType;

    protected String contentType;

    protected String description;

    protected String digest;

    protected String diskFilename;

    protected String filename;

    /** not from redmine but to redmine (file to attach) */
    protected File toUpload;

    public int getAuthorId() {
        return authorId;
    }

    public int getContainerId() {
        return containerId;
    }

    public String getContainerType() {
        return containerType;
    }

    public String getContentType() {
        return contentType;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getDescription() {
        return description;
    }

    public String getDigest() {
        return digest;
    }

    public String getDiskFilename() {
        return diskFilename;
    }

    public int getDownloads() {
        return downloads;
    }

    public String getFilename() {
        return filename;
    }

    public int getFilesize() {
        return filesize;
    }

    @Override
    public int getId() {
        return id;
    }

    public File getToUpload() {
        return toUpload;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public void setDiskFilename(String diskFilename) {
        this.diskFilename = diskFilename;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFilesize(int filesize) {
        this.filesize = filesize;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToUpload(File toUpload) {
        this.toUpload = toUpload;
    }
}
