/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An issue on redmine's server.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class Issue implements IdAble, I18nAble {

    protected int assignedToId;

    protected int authorId;

    protected int categoryId;

    protected int doneRatio;

    protected int lockVersion;

    protected int priorityId;

    protected int projectId;

    protected int statusId;

    protected int trackerId;

    protected int fixedVersionId;

    protected int id;

    protected int parentId;

    protected int rootId;

    protected int lft;

    protected int rgt;

    protected float estimatedHours;

    protected Date createdOn;

    protected Date updatedOn;

    protected Date dueDate;

    protected Date startDate;

    protected String description;

    protected String subject;

    protected boolean isPrivate;

    public static Issue[] byTrackerId(int trackerId, Issue... issues) {
        List<Issue> result = new ArrayList<Issue>();
        for (Issue i : issues) {
            if (i.getTrackerId() == trackerId) {
                result.add(i);
            }
        }
        return result.toArray(new Issue[result.size()]);
    }

    public static Issue[] byVersionId(int versionId, Issue... issues) {
        List<Issue> result = new ArrayList<Issue>();
        for (Issue i : issues) {
            if (i.getFixedVersionId() == versionId) {
                result.add(i);
            }
        }
        return result.toArray(new Issue[result.size()]);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return getSubject();
    }

    public int getAssignedToId() {
        return assignedToId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getDescription() {
        return description;
    }

    public int getDoneRatio() {
        return doneRatio;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public float getEstimatedHours() {
        return estimatedHours;
    }

    public int getFixedVersionId() {
        return fixedVersionId;
    }

    public int getLockVersion() {
        return lockVersion;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public int getProjectId() {
        return projectId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public int getStatusId() {
        return statusId;
    }

    public String getSubject() {
        return subject;
    }

    public int getTrackerId() {
        return trackerId;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public int getLft() {
        return lft;
    }

    public int getRgt() {
        return rgt;
    }

    public int getParentId() {
        return parentId;
    }

    public int getRootId() {
        return rootId;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public void setAssignedToId(int assignedToId) {
        this.assignedToId = assignedToId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDoneRatio(int doneRatio) {
        this.doneRatio = doneRatio;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setEstimatedHours(float estimatedHours) {
        this.estimatedHours = estimatedHours;
    }

    public void setFixedVersionId(int fixedVersionId) {
        this.fixedVersionId = fixedVersionId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLockVersion(int lockVersion) {
        this.lockVersion = lockVersion;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTrackerId(int trackerId) {
        this.trackerId = trackerId;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setLft(int lft) {
        this.lft = lft;
    }

    public void setRgt(int rgt) {
        this.rgt = rgt;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setRootId(int rootId) {
        this.rootId = rootId;
    }

    public void setIsPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
