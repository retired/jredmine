/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

import java.util.Date;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class User implements IdAble, I18nAble {

    protected boolean admin;

    protected boolean mailNotification;

    protected int id;

    protected int authSourceId;

    protected int memberId;

    protected int roleId;

    protected int status;

    protected Date createdOn;

    protected Date updatedOn;

    protected Date lastLoginOn;

    protected String firstname;

    protected String lastname;

    protected String login;

    protected String language;

    protected String mail;

    protected String hashedPassword;

    protected String identityUrl;

    protected String salt;

    public static User byLogin(String login, User... users) {
        for (User u : users) {
            if (u.getLogin().equals(login)) {
                return u;
            }
        }
        return null;
    }

    public boolean isAdmin() {
        return admin;
    }

    public int getAuthSourceId() {
        return authSourceId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getMail() {
        return mail;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }

    public Date getLastLoginOn() {
        return lastLoginOn;
    }

    public String getLastname() {
        return lastname;
    }

    public String getLogin() {
        return login;
    }

    public boolean isMailNotification() {
        return mailNotification;
    }

    public int getMemberId() {
        return memberId;
    }

    public int getRoleId() {
        return roleId;
    }

    public int getStatus() {
        return status;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public String getIdentityUrl() {
        return identityUrl;
    }

    public String getSalt() {
        return salt;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public void setAuthSourceId(int authSourceId) {
        this.authSourceId = authSourceId;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLastLoginOn(Date lastLoginOn) {
        this.lastLoginOn = lastLoginOn;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setMailNotification(boolean mailNotification) {
        this.mailNotification = mailNotification;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String getName() {
        return firstname + " " + lastname;
    }

    public void setIdentityUrl(String identityUrl) {
        this.identityUrl = identityUrl;
    }
}
