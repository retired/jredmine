package org.nuiton.jredmine.model.io.xpp3.api;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.codehaus.plexus.util.xml.pull.XmlPullParser;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.IOException;
import java.io.Reader;

/**
 * A simple contract to mark all xpp readers.
 *
 * a such reader is associated to a {@link #getType()}.
 *
 * The implementations of such readers must be register in a file
 * <pre>
 * META-INF/services.org.nuiton.io.xpp3.Xpp3Reader
 * </pre>
 *
 * to make possible auto-discovering of availables reader at runtime.
 *
 * See {@link Xpp3Helper#getReader(Class)} and
 * {@link Xpp3Helper#getReaders()}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @param <O> the type of object to be build by the reader.
 * @since 1.0.3
 */
public interface Xpp3Reader<O> {

    /** @return the type of main object to read */
    Class<O> getType();

    /**
     * Read a single instance of the typed object and return it.
     *
     * Note : this is a convinient method to call
     * {@link #read(Reader, boolean)} in strict mode.
     *
     * In the xml stream, the root tag must be the {@link #getRootTagName()}.
     *
     * Example :
     * <pre>
     * &lt;issue&gt;
     *    ...
     * &lt;/issue&gt;
     * </pre>
     *
     * @param reader the xml input reader
     * @return Settings
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if parsing error
     */
    O read(Reader reader) throws IOException, XmlPullParserException;

    /**
     * Read a single instance of the typed object and return it.
     *
     * In the xml stream, the root tag must be the {@link #getRootTagName()}.
     *
     * Example :
     * <pre>
     * &lt;issue&gt;
     *    ...
     * &lt;/issue&gt;
     * </pre>
     *
     * @param reader the xml input reader
     * @param strict flag to be strict while parsing
     * @return the read object
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if any parsing pb
     */
    O read(Reader reader, boolean strict) throws IOException,
            XmlPullParserException;

    /**
     * Read some instances of the typed object and return it.
     *
     * In the xml stream, the root tag must be the
     * {@link #getArrayRootTagName()}.
     *
     * Note : this is a convinient method to call :
     * {@link #readArray(Reader, boolean)} in stritc mode.
     *
     * Example :
     * <pre>
     * &lt;issues&gt;
     *   &lt;issue&gt;
     *    ...
     *   &lt;/issue&gt;
     * &lt;/issues&gt;
     * </pre>
     *
     * @param reader the xml input reader
     * @return the array of read objects.
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if any parsing pb
     */
    O[] readArray(Reader reader) throws IOException, XmlPullParserException;

    /**
     * Read some instances of the typed object and return it.
     *
     * In the xml stream, the root tag must be the
     * {@link #getArrayRootTagName()}.
     *
     * Example :
     * <pre>
     * &lt;issues&gt;
     *   &lt;issue&gt;
     *    ...
     *   &lt;/issue&gt;
     * &lt;/issues&gt;
     * </pre>
     *
     * @param reader the xml input reader
     * @param strict flag to be strict while parsing
     * @return the array of read objects.
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if any parsing pb
     */
    O[] readArray(Reader reader, boolean strict) throws IOException,
            XmlPullParserException;

    /** @return the name of the root tag of a object to read */
    String getRootTagName();

    /**
     * Set the name of the root tag of an object to read.
     *
     * @param rootTagName the name of the tag
     */
    void setRootTagName(String rootTagName);

    /** @return the name of the root tag of an array of objets to read */
    String getArrayRootTagName();

    /**
     * Set the name of the root tag for an array of object to read.
     *
     * @param parentRootTagName the name of the tag
     */
    void setParentRootTagName(String parentRootTagName);

    /**
     * @return {@code true} if parser will load the default entities,
     *         {@code false} otherwise.
     */
    boolean isAddDefaultEntities();

    /**
     * Set the new value of the {@code defaultEntities} flag.
     *
     * @param addDefaultEntities the new value.
     */
    void setAddDefaultEntities(boolean addDefaultEntities);

    <T> T[] readArray(String parentRootTagName,
                      String rootTagName,
                      Class<T> type,
                      XmlPullParser parser,
                      boolean strict)
            throws XmlPullParserException, IOException;

    <T> T read(String rootTagName,
               Class<T> type,
               XmlPullParser parser,
               boolean strict)
            throws XmlPullParserException, IOException;
}
