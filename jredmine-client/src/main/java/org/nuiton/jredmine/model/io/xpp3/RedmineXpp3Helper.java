/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model.io.xpp3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.ReaderFactory;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.nuiton.jredmine.model.io.xpp3.api.Xpp3Helper;
import org.nuiton.jredmine.model.io.xpp3.api.Xpp3Reader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Pour construire le modèle à partir de fichiers xml contenant les données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class RedmineXpp3Helper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RedmineXpp3Helper.class);

    public <O> O readObject(Class<O> klass,
                            String txt,
                            boolean strict) throws IOException, XmlPullParserException {
        O result = readObject(klass,
                              new ByteArrayInputStream(txt.getBytes()),
                              strict
        );
        return result;
    }

    public <O> O[] readObjects(Class<O> klass,
                               String txt,
                               boolean strict) throws IOException, XmlPullParserException {
        O[] results = readObjects(klass,
                                  new ByteArrayInputStream(txt.getBytes()),
                                  strict
        );
        return results;
    }

    public <O> O readObject(Class<O> klass,
                            File file,
                            boolean strict) throws IOException, XmlPullParserException {
        FileInputStream stream = new FileInputStream(file);
        try {
            O result = readObject(klass, stream, strict);
            return result;
        } finally {
            stream.close();
        }
    }

    public <O> O[] readObjects(Class<O> klass,
                               File file,
                               boolean strict) throws IOException, XmlPullParserException {
        FileInputStream stream = new FileInputStream(file);
        try {
            O[] results = readObjects(klass, stream, strict);
            return results;
        } finally {
            stream.close();
        }
    }

    public <O> O readObject(Class<O> klass,
                            InputStream stream,
                            boolean strict) throws IOException, XmlPullParserException {

        O result = readObject(klass,
                              ReaderFactory.newXmlReader(stream),
                              strict
        );

        return result;
    }

    public <O> O[] readObjects(Class<O> klass,
                               InputStream stream,
                               boolean strict) throws IOException, XmlPullParserException {

        O[] results = readObjects(klass,
                                  ReaderFactory.newXmlReader(stream),
                                  strict
        );
        return results;
    }

    /**
     * Read an array of objects from a xml stream.
     *
     * @param <O>    the type of objects to return
     * @param klass  the type of object to read
     * @param reader the reader where to parse the xml
     * @param strict a flag to have a strict reading of input source
     * @return the loaded objects
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if any parsing pb
     */
    public <O> O[] readObjects(Class<O> klass, Reader reader, boolean strict)
            throws IOException, XmlPullParserException {

        if (klass == null) {
            throw new NullPointerException("klass parameter can not be null");
        }

        if (reader == null) {
            throw new NullPointerException("reader parameter can not be null");
        }

        Xpp3Reader<O> modelReader = Xpp3Helper.getReader(klass);

        if (modelReader == null) {
            throw new IllegalArgumentException(
                    "could not find xpp3Reader for type " + klass);
        }

        O[] result = null;

        try {

            StringWriter sWriter = new StringWriter();

            IOUtil.copy(reader, sWriter);

            String rawInput = sWriter.toString();
            if (log.isDebugEnabled()) {
                log.debug("content to read : \n" + rawInput);
            }
            StringReader sReader = new StringReader(rawInput);

            result = modelReader.readArray(sReader, strict);

        } finally {
            IOUtil.close(reader);
        }

        return result;
    }

    /**
     * Read a single object from a xml stream.
     *
     * @param <O>    the type of object to read
     * @param klass  the type of object to read
     * @param reader the reader where to parse the xml
     * @param strict a flag to have a strict reading of input source
     * @return the loaded object
     * @throws IOException            if any io pb
     * @throws XmlPullParserException if any parsing pb
     */
    public <O> O readObject(Class<O> klass, Reader reader, boolean strict)
            throws IOException, XmlPullParserException {

        if (klass == null) {
            throw new NullPointerException("klass parameter can not be null");
        }

        if (reader == null) {
            throw new NullPointerException("reader parameter can not be null");
        }

        Xpp3Reader<O> modelReader = Xpp3Helper.getReader(klass);

        if (modelReader == null) {
            throw new IllegalArgumentException(
                    "could not find xpp3Reader for type " + klass);
        }

        O result = null;

        try {

            StringWriter sWriter = new StringWriter();

            IOUtil.copy(reader, sWriter);

            String rawInput = sWriter.toString();
            if (log.isDebugEnabled()) {
                log.debug("content to read : \n" + rawInput);
            }
            StringReader sReader = new StringReader(rawInput);

            result = modelReader.read(sReader, strict);

        } finally {
            IOUtil.close(reader);
        }

        return result;
    }
}
