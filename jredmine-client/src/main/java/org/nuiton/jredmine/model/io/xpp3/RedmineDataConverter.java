/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model.io.xpp3;

import org.nuiton.jredmine.model.io.xpp3.api.DataConverter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * A enumaration to convert some data from a string representation.
 *
 * Can not use directly a converter from commons-beans library, since we have
 * several different converter to use for same type (see {@link #Date} and
 * {@link #Datetime}).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public enum RedmineDataConverter implements DataConverter {

    Boolean {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Boolean.valueOf(t);
            return r;
        }
    }, Short {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Short.valueOf(t);
            return r;
        }
    }, Integer {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Integer.valueOf(t);
            return r;
        }
    }, Long {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Long.valueOf(t);
            return r;
        }
    }, Float {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Float.valueOf(t);
            return r;
        }
    }, Double {
        @Override
        public Object convert(String t) throws Exception {
            Object r = java.lang.Double.valueOf(t);
            return r;
        }
    }, Date {
        @Override
        public Object convert(String t) throws Exception {
            Object r = dateParser.parse(t);
            return r;
        }
    }, Datetime {
        @Override
        public Object convert(String t) throws Exception {
            Object r = datetimeParser.parse(t);
            return r;
        }
    }, Text {
        @Override
        public Object convert(String t) throws Exception {
            return t;
        }
    };

    //FIXME TC20090907 this a a big hack (the + party) redmine returns us a timezone
    // in a format we can not deal with (+hh:mm), so actually, ignore it...
    static final DateFormat datetimeParser = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'+\'SS\':\'SS", Locale.US);

    static final DateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
}
