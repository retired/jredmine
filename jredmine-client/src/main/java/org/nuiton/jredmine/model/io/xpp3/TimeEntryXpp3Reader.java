/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model.io.xpp3;

import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.io.xpp3.api.AbstractXpp3Reader;

import java.beans.IntrospectionException;

/**
 * Created: 31 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TimeEntryXpp3Reader extends AbstractXpp3Reader<TimeEntry> {

    public TimeEntryXpp3Reader() {
        super(TimeEntry.class, "time-entries", "time-entry");
    }

    @Override
    protected void initMappers() throws IntrospectionException {

        addTagTextContentMappers(RedmineDataConverter.Integer, true,
                                 "id",
                                 "activity-id",
                                 "issue-id",
                                 "project-id",
                                 "user-id",
                                 "tyear",
                                 "tmonth",
                                 "tweek"
        );

        addTagTextContentMappers(RedmineDataConverter.Float, true,
                                 "hours"
        );

        addTagTextContentMappers(RedmineDataConverter.Datetime, true,
                                 "created-on",
                                 "updated-on");

        addTagTextContentMappers(RedmineDataConverter.Date, true,
                                 "spent-on");

        addTagTextContentMappers(RedmineDataConverter.Text, true,
                                 "comments");
    }
}
