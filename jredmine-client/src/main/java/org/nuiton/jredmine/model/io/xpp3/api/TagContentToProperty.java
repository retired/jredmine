package org.nuiton.jredmine.model.io.xpp3.api;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.codehaus.plexus.util.xml.pull.XmlPullParser;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.util.Map;

/**
 * Property mapper from a tag content to an object.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class TagContentToProperty<O> extends PropertyMapper {

    private final Xpp3Reader<O> reader;

    private final Class<O> datatype;

    private final boolean multiple;

    public TagContentToProperty(String tagName,
                                String propertyName,
                                Class<?> containerType,
                                Class<O> type,
                                boolean onlyOne,
                                PropertyDescriptor descriptor,
                                Xpp3Reader<O> reader,
                                boolean multiple) {
        super(tagName,
              propertyName,
              containerType,
              null,
              onlyOne,
              descriptor
        );
        datatype = type;
        this.reader = reader;
        this.multiple = multiple;
    }

    @Override
    protected Object getDataFromXml(XmlPullParser parser)
            throws Exception {
        Object result;
        if (multiple) {
            result = reader.readArray(
                    reader.getArrayRootTagName(),
                    reader.getRootTagName(),
                    datatype,
                    parser,
                    true
            );
        } else {
            result = reader.read(
                    reader.getRootTagName(),
                    datatype,
                    parser,
                    true
            );
        }
        return result;
    }

    public static <O> void addMapper(Class<?> containerType,
                                     Class<O> type,
                                     boolean onlyOne,
                                     Map<String, PropertyMapper> allMappers,
                                     String tagName,
                                     Xpp3Reader<O> reader,
                                     boolean multiple
    ) throws IntrospectionException,
            IllegalArgumentException {

        // the tag-name is transformed to tagName
        String propertyName = TAG_NAME_TRANSFORMER.apply(tagName);

        PropertyDescriptor descriptor = getDescriptor(containerType,
                                                      propertyName);

        PropertyMapper mapper = new TagContentToProperty<O>(
                tagName,
                propertyName,
                containerType,
                type,
                onlyOne,
                descriptor,
                reader,
                multiple
        );
        mapper.registerMapper(allMappers, containerType, tagName, null);
    }
}
