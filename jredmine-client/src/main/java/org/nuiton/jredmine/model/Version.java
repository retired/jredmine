/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

import java.util.Date;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class Version implements IdAble, I18nAble {

    protected Date createdOn;

    protected Date updatedOn;

    protected Date effectiveDate;

    protected String name;

    protected String description;

    protected String wikiPageTitle;

    protected String sharing;

    protected String status;

    protected int id;

    protected int projectId;

    public static Version byVersionName(String name, Version... versions) {
        for (Version u : versions) {
            if (u.getName().equals(name)) {
                return u;
            }
        }
        return null;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getDescription() {
        return description;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getProjectId() {
        return projectId;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public String getWikiPageTitle() {
        return wikiPageTitle;
    }

    public String getSharing() {
        return sharing;
    }

    public String getStatus() {
        return status;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setWikiPageTitle(String wikiPageTitle) {
        this.wikiPageTitle = wikiPageTitle;
    }

    public void setSharing(String sharing) {
        this.sharing = sharing;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isOpen() {
        return VersionStatusEnum.open.name().equals(getStatus());
    }

    public boolean isClosed() {
        return VersionStatusEnum.closed.name().equals(getStatus()) && getEffectiveDate() != null;
    }
}
