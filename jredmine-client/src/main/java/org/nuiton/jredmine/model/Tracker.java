/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class Tracker implements IdAble, I18nAble {

    protected int id;

    protected int projectId;

    protected int trackerId;

    protected int position;

    protected boolean isInChlog;

    protected boolean isInRoadmap;

    protected String name;

    @Override
    public int getId() {
        return id;
    }

    public boolean isIsInChlog() {
        return isInChlog;
    }

    public boolean isIsInRoadmap() {
        return isInRoadmap;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public int getProjectId() {
        return projectId;
    }

    public int getTrackerId() {
        return trackerId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIsInChlog(boolean isInChlog) {
        this.isInChlog = isInChlog;
    }

    public void setIsInRoadmap(boolean isInRoadmap) {
        this.isInRoadmap = isInRoadmap;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void setTrackerId(int trackerId) {
        this.trackerId = trackerId;
    }
}
