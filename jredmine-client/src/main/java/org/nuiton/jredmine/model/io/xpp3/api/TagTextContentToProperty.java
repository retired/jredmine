package org.nuiton.jredmine.model.io.xpp3.api;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.codehaus.plexus.util.xml.pull.XmlPullParser;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.util.Map;

/**
 * Property mapp from a tag text content.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class TagTextContentToProperty extends PropertyMapper {

    public TagTextContentToProperty(String tagName,
                                    String propertyName,
                                    Class<?> containerType,
                                    DataConverter type,
                                    boolean onlyOne,
                                    PropertyDescriptor descriptor) {
        super(tagName,
              propertyName,
              containerType,
              type,
              onlyOne,
              descriptor
        );
    }

    @Override
    protected Object getDataFromXml(XmlPullParser parser)
            throws Exception {
        String t = parser.nextText();
        Object result = null;
        if (t != null && !(t = t.trim()).isEmpty()) {
            result = type.convert(t);
        }
        return result;
    }

    public static void addMapper(Class<?> containerType,
                                 DataConverter type,
                                 boolean onlyOne,
                                 Map<String, PropertyMapper> allMappers,
                                 String... tagNames) throws IntrospectionException {
        for (String tagName : tagNames) {

            // the tag-name is transformed to tagName
            String propertyName = TAG_NAME_TRANSFORMER.apply(tagName);

            PropertyDescriptor descriptor = getDescriptor(containerType,
                                                          propertyName);


            PropertyMapper mapper = new TagTextContentToProperty(
                    tagName,
                    propertyName,
                    containerType,
                    type,
                    onlyOne,
                    descriptor
            );
            mapper.registerMapper(allMappers, containerType, tagName, null);
        }
    }
}
