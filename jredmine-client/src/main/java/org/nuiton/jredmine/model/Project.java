/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model;

import java.util.Date;

/**
 * redmine project
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class Project implements IdAble, I18nAble {

    protected Date createdOn;

    protected Date updatedOn;

    protected String description;

    protected String homepage;

    protected String identifier;

    protected String name;

    protected int id;

    protected int lft;

    protected int rgt;

    protected int parentId;

    protected int projectsCount;

    protected int status;

    protected boolean isPublic;

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getDescription() {
        return description;
    }

    public String getHomepage() {
        return homepage;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public boolean isIsPublic() {
        return isPublic;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getParentId() {
        return parentId;
    }

    public int getProjectsCount() {
        return projectsCount;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public int getStatus() {
        return status;
    }

    public int getLft() {
        return lft;
    }

    public int getRgt() {
        return rgt;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setProjectsCount(int projectsCount) {
        this.projectsCount = projectsCount;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setLft(int lft) {
        this.lft = lft;
    }

    public void setRgt(int rgt) {
        this.rgt = rgt;
    }

    public boolean isOpen() {
        return status != 9;
    }
}
