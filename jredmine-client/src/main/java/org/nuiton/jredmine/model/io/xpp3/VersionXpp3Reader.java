/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model.io.xpp3;

import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.model.io.xpp3.api.AbstractXpp3Reader;

import java.beans.IntrospectionException;

/**
 * {@link Version} reader.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class VersionXpp3Reader extends AbstractXpp3Reader<Version> {

    public VersionXpp3Reader() {
        super(Version.class, "versions", "version");
    }

    @Override
    protected void initMappers() throws IntrospectionException {

        addTagTextContentMappers(RedmineDataConverter.Integer, true,
                                 "id",
                                 "project-id");

        addTagTextContentMappers(RedmineDataConverter.Datetime, true,
                                 "created-on",
                                 "updated-on");
        addTagTextContentMappers(RedmineDataConverter.Date, true,
                                 "effective-date");

        addTagTextContentMappers(RedmineDataConverter.Text, true,
                                 "name",
                                 "description",
                                 "wiki-page-title",
                                 "sharing",
                                 "status");
    }
}
