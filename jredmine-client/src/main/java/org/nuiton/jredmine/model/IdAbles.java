package org.nuiton.jredmine.model;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Helper methods around {@link IdAble}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class IdAbles {
    public static final Comparator<IdAble> ID_ABLE_COMPARATOR = new Comparator<IdAble>() {

        @Override
        public int compare(IdAble o1, IdAble o2) {
            return o1.getId() - o2.getId();
        }
    };

    public static <T extends IdAble> T byId(int id, T... datas) {
        for (T data : datas) {
            if (data.getId() == id) {
                return data;
            }
        }
        return null;
    }

    public static <T extends IdAble> T[] byIds(Class<T> type, T[] result, Integer... ids) {

        List<Integer> lIds = Arrays.asList(ids);
        List<T> filter = new ArrayList<T>(lIds.size());
        for (T v : result) {
            if (lIds.contains(v.getId())) {
                filter.add(v);
            }
        }
        return filter.toArray((T[]) Array.newInstance(type, filter.size()));
    }
}
