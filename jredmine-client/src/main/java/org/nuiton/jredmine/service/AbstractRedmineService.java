package org.nuiton.jredmine.service;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.nuiton.jredmine.client.RedmineClient;
import org.nuiton.jredmine.client.RedmineRequest;

import java.io.IOException;

/**
 * Abstract redmine service which boxes the technical {@link RedmineClient}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public abstract class AbstractRedmineService {

    /**
     * Redmine client.
     *
     * @since 1.5
     */
    private RedmineClient client;

    public boolean isInit() {
        return client != null && client.isOpen();
    }

    public void init(RedmineServiceConfiguration configuration) throws RedmineServiceException {
        Preconditions.checkNotNull(configuration, "the client configuration was not be null!");
        Preconditions.checkState(!isInit(), "the client " + this + " was already init!");
        client = new RedmineClient(configuration);
        try {

            client.open();
        } catch (Exception e) {
            throw new RedmineServiceException(
                    "could not init service for reason " + e.getMessage(), e);
        }
    }

    public void destroy() throws RedmineServiceException {
        if (isInit()) {
            try {
                if (client.isOpen()) {
                    try {
                        client.close();
                    } catch (IOException e) {
                        throw new RedmineServiceException(
                                "has problem while closing Rest client " +
                                e.getMessage(), e);
                    }
                }
            } finally {
                if (client != null) {
                    client = null;
                }
            }
        }
    }

    public RedmineClient getClient() {
        return client;
    }

    protected <T> T executeRequestAndReturnData(RedmineRequest<T> request) throws RedmineServiceException {
        checkInit();
        try {
            T result = client.executeRequest(request);
            return result;
        } catch (Exception ex) {
            throw new RedmineServiceException(
                    "could not obtain datas of type " + request.getType() + " for reason " +
                    ex.getMessage(), ex);
        }
    }

    protected <T> T[] executeRequestAndReturnDatas(RedmineRequest<T> request) throws RedmineServiceException {
        checkInit();
        try {
            T[] result = client.executeRequests(request);
            return result;
        } catch (Exception ex) {
            throw new RedmineServiceException(
                    "could not obtain datas of type " + request.getType() + " for reason " +
                    ex.getMessage(), ex);
        }
    }

    protected void checkInit() throws IllegalStateException {
        if (!isInit()) {
            throw new IllegalStateException(
                    "the client " + this + " is not init!");
        }
    }

    protected void checkSessionNotNull() {
        if (client == null) {
            throw new NullPointerException("session can not be null");
        }
    }

    protected void checkSessionConfigurationNotNull() {
        if (client.getConfiguration() == null) {
            throw new NullPointerException(
                    "session configuration can not be null");
        }
    }

    protected void checkLoggued() throws IllegalStateException, RedmineServiceLoginException, NullPointerException {
        checkInit();
        checkSessionNotNull();
        checkSessionConfigurationNotNull();
        boolean anonymous = !RedmineConfigurationUtil.isLoggued(client.getConfiguration());
        if (anonymous) {
            throw new RedmineServiceLoginException(
                    "can not access this service in anonymous mode");
        }
    }

}
