package org.nuiton.jredmine.service;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jredmine.client.RedmineRequest;
import org.nuiton.jredmine.client.RedmineRequestHelper;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.model.VersionStatusEnum;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Default implementation of the {@link RedmineService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.jredmine.service.RedmineService" role-hint="default"
 * @since 1.5
 */
public class DefaultRedmineService extends AbstractRedmineService implements RedmineService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DefaultRedmineService.class);

    @Override
    public void init(RedmineServiceConfiguration configuration) throws RedmineServiceException {
        if (log.isDebugEnabled()) {
            log.debug("init service with configuration: " + configuration);
        }
        if (!isInit()) {

            // always clone the configuration to keep it in the service
            RedmineServiceConfiguration serviceConfiguration =
                    RedmineConfigurationUtil.cloneConfiguration(configuration);

            // init the service using the configuration
            super.init(serviceConfiguration);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineAnonymousService implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public IssueStatus[] getIssueStatuses() throws RedmineServiceException {
        RedmineRequest<IssueStatus> request =
                RedmineRequestHelper.action("get_issue_statuses.xml",
                                            IssueStatus.class);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public IssuePriority[] getIssuePriorities() throws RedmineServiceException {
        RedmineRequest<IssuePriority> request = RedmineRequestHelper.action(
                "get_issue_priorities.xml", IssuePriority.class);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Project[] getProjects() throws RedmineServiceException {
        RedmineRequest<Project> request = RedmineRequestHelper.action(
                "get_projects.xml", Project.class);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public IssueCategory[] getIssueCategories(String projectName) throws RedmineServiceException {
        RedmineRequest<IssueCategory> request =
                RedmineRequestHelper.actionWithProject(
                        "get_issue_categories.xml",
                        IssueCategory.class,
                        projectName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Project getProject(String projectName) throws RedmineServiceException {
        RedmineRequest<Project> request =
                RedmineRequestHelper.actionWithProject("get_project.xml",
                                                       Project.class,
                                                       projectName);
        return executeRequestAndReturnData(request
        );
    }

    @Override
    public Tracker[] getTrackers(String projectName) throws RedmineServiceException {
        RedmineRequest<Tracker> request =
                RedmineRequestHelper.actionWithProject("get_project_trackers.xml",
                                                       Tracker.class,
                                                       projectName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public News[] getNews(String projectName) throws RedmineServiceException {
        RedmineRequest<News> request =
                RedmineRequestHelper.actionWithProject("get_project_news.xml",
                                                       News.class,
                                                       projectName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public User[] getProjectMembers(String projectName) throws RedmineServiceException {
        RedmineRequest<User> request =
                RedmineRequestHelper.actionWithProject("get_project_users.xml",
                                                       User.class,
                                                       projectName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Version[] getVersions(String projectName) throws RedmineServiceException {
        RedmineRequest<Version> request =
                RedmineRequestHelper.actionWithProject("get_project_versions.xml",
                                                       Version.class,
                                                       projectName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Version getVersion(String projectName,
                              String versionName) throws RedmineServiceException {
        RedmineRequest<Version> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version.xml",
                                                                 Version.class,
                                                                 projectName,
                                                                 versionName);
        return executeRequestAndReturnData(request);
    }

    @Override
    public Version getLastestClosedVersion(String projectName) throws RedmineServiceException {

        // get all versions
        Version[] versions = getVersions(projectName);
        List<Version> closedVersions = new ArrayList<Version>();
        for (Version version : versions) {
            if (version.isClosed()) {
                closedVersions.add(version);
            }
        }

        Version result = null;
        if (!closedVersions.isEmpty()) {
            Collections.sort(closedVersions, new Comparator<Version>() {
                @Override
                public int compare(Version o1, Version o2) {
                    return o2.getEffectiveDate().compareTo(o1.getEffectiveDate());
                }
            });
            result = closedVersions.get(0);
        }
        return result;
    }

    @Override
    public Attachment[] getAttachments(String projectName,
                                       String versionName) throws RedmineServiceException {
        RedmineRequest<Attachment> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version_attachments.xml",
                                                                 Attachment.class,
                                                                 projectName,
                                                                 versionName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Issue[] getIssues(String projectName,
                             String versionName) throws RedmineServiceException {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version_issues.xml",
                                                                 Issue.class,
                                                                 projectName,
                                                                 versionName);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public TimeEntry[] getIssueTimeEntries(String projectName,
                                           String issueId) throws RedmineServiceException {
        RedmineRequest<TimeEntry> request =
                RedmineRequestHelper.actionWithProjectAndIssue("get_issue_times.xml",
                                                               TimeEntry.class,
                                                               projectName,
                                                               issueId);
        return executeRequestAndReturnDatas(request);
    }

    @Override
    public Issue[] getIssues(String projectName) throws RedmineServiceException {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProject("get_project_issues.xml",
                                                       Issue.class,
                                                       projectName);
        Issue[] result = executeRequestAndReturnDatas(request);
        return result;
    }

    @Override
    public Issue[] getOpenedIssues(String projectName) throws RedmineServiceException {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProject("get_project_opened_issues.xml",
                                                       Issue.class,
                                                       projectName);
        Issue[] result = executeRequestAndReturnDatas(request);
        return result;
    }

    @Override
    public Issue[] getClosedIssues(String projectName) throws RedmineServiceException {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProject("get_project_closed_issues.xml",
                                                       Issue.class,
                                                       projectName);
        Issue[] result = executeRequestAndReturnDatas(request);
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineLogguedService implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public User getCurrentUser() throws RedmineServiceException {
        checkLoggued();
        RedmineRequest<User> request =
                RedmineRequestHelper.action("get_user.xml", User.class);
        return executeRequestAndReturnData(request);
    }

    @Override
    public Project[] getUserProjects() throws RedmineServiceException {
        checkLoggued();
        RedmineRequest<Project> request = RedmineRequestHelper.action(
                "get_user_projects.xml", Project.class);
        Project[] result = executeRequestAndReturnDatas(request);
        return result;
    }

    @Override
    public Version addVersion(String projectName,
                              Version version) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<Version> request =
                RedmineRequestHelper.postWithProject("add_version.xml",
                                                     Version.class,
                                                     projectName);
        String date = getVersionEffectiveDate(version);
        String status = getVersionStatus(version);

        request.parameter("version[name]", version.getName());
        request.parameter("version[description]", version.getDescription());
        request.parameter("version[effective_date]", date);
        request.parameter("version[status]", status);

        // send data and obtain created version
        Version result = executeRequestAndReturnData(request);
        return result;
    }

    @Override
    public Version updateVersion(String projectName,
                                 Version version) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<Version> request =
                RedmineRequestHelper.postWithProject("update_version.xml",
                                                     Version.class,
                                                     projectName);

        String date = getVersionEffectiveDate(version);
        String status = getVersionStatus(version);

        request.parameter("version[name]", version.getName());
        request.parameter("version[description]", version.getDescription());
        request.parameter("version[effective_date]", date);
        request.parameter("version[status]", status);

        // send data and obtain updated version
        Version result = executeRequestAndReturnData(request);
        return result;
    }

    @Override
    public Version nextVersion(String projectName,
                               String oldVersionName,
                               Version version) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<Version> request =
                RedmineRequestHelper.postWithProject("next_version.xml",
                                                     Version.class,
                                                     projectName);
        String date = getVersionEffectiveDate(version);
        String status = getVersionStatus(version);

        request.parameter("oldVersionName", oldVersionName);
        request.parameter("version[name]", version.getName());
        request.parameter("version[description]", version.getDescription());
        request.parameter("version[effective_date]", date);
        request.parameter("version[status]", status);

        // send data and obtain updated or created new version
        Version result = executeRequestAndReturnData(request);
        return result;
    }

    @Override
    public Attachment addAttachment(String projectName,
                                    String versionName,
                                    Attachment attachment) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<Attachment> request =
                RedmineRequestHelper.postWithProjectAndVersion("add_version_attachment.xml",
                                                               Attachment.class,
                                                               projectName,
                                                               versionName);

        request.parameter("attachment[description]", attachment.getDescription());
        request.attachment("attachment[file]", attachment.getToUpload());

        // send data and obtain created attachment
        Attachment result = executeRequestAndReturnData(request);
        return result;
    }

    @Override
    public News addNews(String projectName,
                        News news) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<News> request =
                RedmineRequestHelper.postWithProject("add_news.xml",
                                                     News.class,
                                                     projectName);

        request.parameter("news[title]", news.getTitle());
        request.parameter("news[summary]", news.getSummary());
        request.parameter("news[description]", news.getDescription());

        // send data and obtain created news
        News result = executeRequestAndReturnData(request);
        return result;
    }

    @Override
    public TimeEntry addIssueTimeEntry(String projectName,
                                       String issueId,
                                       TimeEntry timeEntry) throws RedmineServiceException {
        checkLoggued();

        RedmineRequest<TimeEntry> request =
                RedmineRequestHelper.postWithProjectAndIssue("add_issue_time.xml",
                                                             TimeEntry.class,
                                                             projectName,
                                                             issueId);

        Date d = timeEntry.getSpentOn();
        if (d == null) {
            d = new Date();
        }
        String date = DATE_FORMAT.format(d);

        request.parameter("time_entry[activity_id]", timeEntry.getActivityId() + "");
        request.parameter("time_entry[spent_on]", date);
        request.parameter("time_entry[hours]", timeEntry.getHours() + "");
        request.parameter("time_entry[comments]", timeEntry.getComments() == null ? "" : timeEntry.getComments());

        // send data and obtain created time entry
        TimeEntry result = executeRequestAndReturnData(request);
        return result;
    }

    protected final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    protected String getVersionStatus(Version version) {
        String status = version.getStatus();
        if (Strings.isNullOrEmpty(status)) {

            // use default open status
            status = VersionStatusEnum.open.name();
        }
        return status;
    }

    protected String getVersionEffectiveDate(Version version) {
        return version.getEffectiveDate() == null ? "" :
               DATE_FORMAT.format(version.getEffectiveDate());
    }

}
