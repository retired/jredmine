package org.nuiton.jredmine.service;
/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jredmine.client.RedmineClient;
import org.nuiton.jredmine.client.RedmineClientAuthConfiguration;
import org.nuiton.jredmine.client.RedmineClientConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * Helper methods about configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineConfigurationUtil {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RedmineConfigurationUtil.class);

    public static final String PROPERTY_PREFIX = "jredmine-test.";

    protected RedmineConfigurationUtil() {
        // avoid instance
    }

    public static RedmineServiceConfiguration obtainRedmineConfiguration(
            RedmineServiceConfiguration anoConf) throws IOException {

        RedmineServiceConfiguration conf = newLogguedConfiguration(anoConf);

        boolean ok = false;

        RedmineClient client = new RedmineClient(conf);

        try {

            client.open();
            ok = client.isOpen();
        } catch (Exception e) {

            if (log.isDebugEnabled()) {
                log.debug("Could not connect to redmine with configuration: " + conf, e);
            }

        } finally {
            try {
                client.close();
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close session", e);
                }
            }
        }

        if (!ok) {

            // try to connect anonymous
            conf = cloneConfiguration(anoConf);

            client = new RedmineClient(conf);

            try {

                client.open();
                ok = client.isOpen();
            } catch (Exception e) {

                if (log.isDebugEnabled()) {
                    log.debug("Could not connect to redmine with configuration: " + conf, e);
                }
            } finally {
                try {
                    client.close();
                } catch (IOException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not close session", e);
                    }
                }
            }
        }

        if (!ok) {

            // no conf available
            conf = null;
        }
        return conf;
    }

    public static RedmineServiceConfiguration newAnonymousConfiguration(String propertiesFromEnv,
                                                                        String classPathPropertiesLocation)
            throws IOException {

        Properties props = new Properties();

        InputStream in = null;
        try {
            String jredmineConfiguration = System.getenv(propertiesFromEnv);
            if (jredmineConfiguration == null) {
                if (log.isInfoEnabled()) {
                    log.info("Could not find environement variable " +
                             "'jredmine-test.properties' will use " +
                             "default test configuration");
                }

                in = RedmineConfigurationUtil.class.getResourceAsStream(classPathPropertiesLocation);
            } else {

                File file = new File(jredmineConfiguration);

                if (!file.exists()) {
                    throw new IllegalStateException("Could not find " + jredmineConfiguration +
                                                    " file");
                }
                in = FileUtils.openInputStream(file);
            }
            props.load(in);
            in.close();
        } finally {
            IOUtils.closeQuietly(in);
        }

        RedmineServiceConfiguration result = new SimpleRedmineServiceConfiguration();

        RedmineClientAuthConfiguration authConfiguration =
                new RedmineClientAuthConfiguration();
        result.setAuthConfiguration(authConfiguration);
        overridePropertyFromProperties(result, "url", props);
        overridePropertyFromProperties(result, "encoding", props);
        overridePropertyFromProperties(result, "verbose", props);
        // Anonymous means no credential filled
        overridePropertyFromProperties(authConfiguration, "username", props);
        overridePropertyFromProperties(authConfiguration, "password", props);
        overridePropertyFromProperties(authConfiguration, "apiKey", props);

        overridePropertyFromEnv(result, "url");
        overridePropertyFromEnv(result, "encoding");
        overridePropertyFromEnv(result, "verbose");
        return result;
    }

    public static RedmineServiceConfiguration newAnonymousConfiguration(File file)
            throws IOException {

        Properties props = new Properties();

        InputStream in = null;
        try {

            if (!file.exists()) {
                throw new IllegalStateException("Could not find " + file + " file");
            }
            in = FileUtils.openInputStream(file);

            props.load(in);
            in.close();
        } finally {
            IOUtils.closeQuietly(in);
        }

        RedmineServiceConfiguration result = new SimpleRedmineServiceConfiguration();

        RedmineClientAuthConfiguration authConfiguration =
                new RedmineClientAuthConfiguration();
        result.setAuthConfiguration(authConfiguration);
        overridePropertyFromProperties(result, "url", props);
        overridePropertyFromProperties(result, "encoding", props);
        overridePropertyFromProperties(result, "verbose", props);
        // Anonymous means no credential filled
        overridePropertyFromProperties(authConfiguration, "username", props);
        overridePropertyFromProperties(authConfiguration, "password", props);
        overridePropertyFromProperties(authConfiguration, "apiKey", props);

        overridePropertyFromEnv(result, "url");
        overridePropertyFromEnv(result, "encoding");
        overridePropertyFromEnv(result, "verbose");
        return result;
    }

    public static RedmineServiceConfiguration newLogguedConfiguration(RedmineServiceConfiguration anoConf)
            throws IOException {

        // use anonymous configuration

        RedmineServiceConfiguration result = cloneConfiguration(anoConf);

        RedmineClientAuthConfiguration authConfiguration =
                result.getAuthConfiguration();
        overridePropertyFromEnv(authConfiguration, "username");
        overridePropertyFromEnv(authConfiguration, "password");
        overridePropertyFromEnv(authConfiguration, "apiKey");

        return result;
    }

    public static RedmineServiceConfiguration cloneConfiguration(
            RedmineServiceConfiguration src) {
        RedmineServiceConfiguration dst = new SimpleRedmineServiceConfiguration();
        copyConfiguration(src, dst);
        return dst;
    }

    public static void copyConfiguration(RedmineServiceConfiguration src,
                                         RedmineServiceConfiguration dst) {
        dst.setUrl(src.getUrl());
        dst.setEncoding(src.getEncoding());
        dst.setVerbose(src.isVerbose());

        RedmineClientAuthConfiguration authConfiguration = new RedmineClientAuthConfiguration();
        authConfiguration.setApiKey(src.getAuthConfiguration().getApiKey());
        authConfiguration.setUsername(src.getAuthConfiguration().getUsername());
        authConfiguration.setPassword(src.getAuthConfiguration().getPassword());
        dst.setAuthConfiguration(authConfiguration);
    }

    protected static void overridePropertyFromProperties(Object conf,
                                                         String prop,
                                                         Properties props) throws IOException {
        String value = props.getProperty(PROPERTY_PREFIX + prop);
        if (StringUtils.isNotEmpty(value)) {
            try {
                BeanUtilsBean.getInstance().setProperty(conf, prop, value);
            } catch (Exception e) {
                throw new IOException(
                        "Could not set property '" + prop +
                        "' with value '" + value + "' to configuration"
                );
            }
        }
    }

    protected static void overridePropertyFromEnv(Object conf,
                                                  String prop) throws IOException {
        String value = System.getenv(PROPERTY_PREFIX + prop);
        if (StringUtils.isNotEmpty(value) && !"null".equals(value) && !value.startsWith("${")) {
            try {
                BeanUtilsBean.getInstance().setProperty(conf, prop, value);
            } catch (Exception e) {
                throw new IOException(
                        "Could not set property '" + prop +
                        "' with value '" + value + "' to configuration"
                );
            }
        }
    }

    public static boolean isLoggued(RedmineClientConfiguration configuration) {
        RedmineClientAuthConfiguration authConfiguration = configuration.getAuthConfiguration();
        return authConfiguration.isUseApiKey() || authConfiguration.isUseLoginPassword();
    }

    /**
     * Configuration of a redmine service for test purposes.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 1.4
     */
    public static class SimpleRedmineServiceConfiguration implements RedmineServiceConfiguration {

        URL url;

        boolean verbose;

        String encoding;

        RedmineClientAuthConfiguration authConfiguration;

        @Override
        public RedmineClientAuthConfiguration getAuthConfiguration() {
            return authConfiguration;
        }

        @Override
        public void setAuthConfiguration(RedmineClientAuthConfiguration authConfiguration) {
            this.authConfiguration = authConfiguration;
        }

        @Override
        public String getEncoding() {
            return encoding;
        }

        @Override
        public void setEncoding(String encoding) {
            this.encoding = encoding;
        }

        @Override
        public URL getUrl() {
            return url;
        }

        @Override
        public void setUrl(URL url) {
            this.url = url;
        }

        @Override
        public boolean isVerbose() {
            return verbose;
        }

        @Override
        public void setVerbose(boolean verbose) {
            this.verbose = verbose;
        }

        @Override
        public String toString() {
            ToStringBuilder b = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
            b.append("redmineUrl", url);
            b.append("apiKey", authConfiguration.getApiKey());
            b.append("username", authConfiguration.getUsername());
            b.append("password", "***");
            b.append("encoding", encoding);
            b.append("verbose", verbose);
            return b.toString();
        }
    }
}
