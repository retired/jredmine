/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.service;

/**
 * An exception to throw when something is wrong in a {@link RedmineService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class RedmineServiceException extends Exception {

    private static final long serialVersionUID = 1L;

    public RedmineServiceException(Throwable cause) {
        super(cause);
    }

    public RedmineServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public RedmineServiceException(String message) {
        super(message);
    }

    public RedmineServiceException() {
    }
}
