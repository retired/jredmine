/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.service;

import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;


/**
 * Contract of all redmine operations that needs a login to be performed (for both public  or private projects).
 *
 * A default implementation is offered in {@link DefaultRedmineService}.
 *
 * Created: 2 janv. 2010
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see RedmineAnonymousService
 * @see RedmineService
 * @since 1.0.3
 */
public interface RedmineLogguedService {

    /**
     * Obtain the redmine user from the credentials given in the service configuration.
     *
     * @return the current user using the credentials of the service configuration
     * @throws RedmineServiceException if any pb
     * @since 1.7
     */
    User getCurrentUser() throws RedmineServiceException;

    /**
     * Obtain for the loggued user, all projets where he belongs.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @return all the projects belonged by the loggued user
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Project
     * @since 1.0.3
     */
    Project[] getUserProjects() throws RedmineServiceException;

    /**
     * Add a version for a given project.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName the name of the project
     * @param version     the version to add
     * @return the added version
     * @throws RedmineServiceException if any pb while sending and retriving
     *                                 datas to redmine server
     * @see Version
     */
    Version addVersion(String projectName,
                       Version version) throws RedmineServiceException;

    /**
     * Update a version for a given project.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName the name of the project
     * @param version     the version to update
     * @return the updated version
     * @throws RedmineServiceException if any pb while sending and retriving
     *                                 datas to redmine server
     * @see Version
     */
    Version updateVersion(String projectName,
                          Version version) throws RedmineServiceException;

    /**
     * Prepare a new version (create it or update it).
     *
     * If the {@code oldVersionName} is given, then all issues unclosed from this
     * old version will be move to the new version.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName    the name of the project
     * @param oldVersionName the name of the old version (optional)
     * @param newVersion     the newVersion to create or update
     * @return the created version
     * @throws RedmineServiceException if any pb while sending and retriving
     *                                 datas to redmine server
     * @see Version
     */
    Version nextVersion(String projectName,
                        String oldVersionName,
                        Version newVersion) throws RedmineServiceException;

    /**
     * Add a news for a given project.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName the name of the project
     * @param news        the news to add
     * @return the added news.
     * @throws RedmineServiceException if any pb while sending and retriving
     *                                 datas to redmine server
     * @see News
     */
    News addNews(String projectName, News news) throws RedmineServiceException;

    /**
     * Add a attachment for a given version of a given project.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName the name of the project
     * @param versionName the name of the version
     * @param attachement the attachment to add
     * @return the added attachment
     * @throws RedmineServiceException if any pb while sending and retriving
     *                                 datas to redmine server
     * @see Attachment
     */
    Attachment addAttachment(String projectName,
                             String versionName,
                             Attachment attachement) throws RedmineServiceException;

    /**
     * Add a new time entry to the given issue for the given project and
     * return the updated time entry.
     *
     * <b>Note:</b> This method requires to be loggued on redmine server.
     *
     * @param projectName the name of the project
     * @param issueId     the id of the issue to update
     * @param entry       time entry to add
     * @return the created time entry
     * @throws RedmineServiceException if any pb while sending or retrieving
     *                                 datas to redmine server
     * @see Issue
     * @see TimeEntry
     * @since 1.0.3
     */
    TimeEntry addIssueTimeEntry(String projectName,
                                String issueId,
                                TimeEntry entry) throws RedmineServiceException;
}
