/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.service;

import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;

/**
 * Contract of all redmine operations which do not requires any login to server (if project is public).
 *
 *
 * Created: 2 janv. 2010
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
public interface RedmineAnonymousService {

    /**
     * Initialize the service.
     *
     * @param configuration the configuration to be used to init the internal redmine client
     * @throws RedmineServiceException if any pb
     */
    void init(RedmineServiceConfiguration configuration) throws RedmineServiceException;

    /**
     * Tells if service was successful initialized.
     *
     * @return {@code true} if service was successfull initialized via
     * method {@link #init(RedmineServiceConfiguration)}, {@code false} otherwise.
     */
    boolean isInit();

    /**
     * Close the service and destroy any connexion to the redmine service.
     *
     * @throws RedmineServiceException if any pb
     */
    void destroy() throws RedmineServiceException;

    /**
     * Obtain all accessible projects.
     *
     * @return all the projects
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Project
     */
    Project[] getProjects() throws RedmineServiceException;

    /**
     * Obtain all the priorities defined on a {@link Issue}.
     *
     * <b>Note : </b> The priorities are common for all projects.
     *
     * @return all the issue properties
     * @throws RedmineServiceException if any pb while retriving datas
     * @see IssuePriority
     */
    IssuePriority[] getIssuePriorities() throws RedmineServiceException;

    /**
     * Obtain all the statuses defined on a {@link Issue}.
     *
     * <b>Note : </b> The statuses are common for all projects.
     *
     * @return all the issue statuses
     * @throws RedmineServiceException if any pb while retriving datas
     * @see IssueStatus
     */
    IssueStatus[] getIssueStatuses() throws RedmineServiceException;

    /**
     * Obtain a project given his name.
     *
     * @param projectName the name of the project
     * @return the project
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Project
     */
    Project getProject(String projectName) throws RedmineServiceException;

    /**
     * Obtain all categories defined on issues for a given project.
     *
     * @param projectName the name of the project
     * @return the categories of issues for the given project.
     * @throws RedmineServiceException if any pb while retriving datas
     * @see IssueCategory
     */
    IssueCategory[] getIssueCategories(String projectName) throws RedmineServiceException;

    /**
     * Obtain all trackers defined on a given project.
     *
     * @param projectName the name of the project
     * @return the trackers for the given project.
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Tracker
     */
    Tracker[] getTrackers(String projectName) throws RedmineServiceException;

    /**
     * Obtain all news defined on a given project.
     *
     * @param projectName the name of the project
     * @return the news for the given project.
     * @throws RedmineServiceException if any pb while retriving datas
     * @see News
     */
    News[] getNews(String projectName) throws RedmineServiceException;

    /**
     * Obtain all users defined on a given project.
     *
     * @param projectName the name of the project
     * @return the users for the given project.
     * @throws RedmineServiceException if any pb while retriving datas
     * @see User
     */
    User[] getProjectMembers(String projectName) throws RedmineServiceException;

    /**
     * Obtain all versions defined on a given project.
     *
     * @param projectName the name of the project
     * @return the versions of the given project.
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Version
     */
    Version[] getVersions(String projectName) throws RedmineServiceException;

    /**
     * Obtain a specific version for a given project.
     *
     * @param projectName the name of the project
     * @param versionName the name of the version
     * @return the version
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Version
     */
    Version getVersion(String projectName,
                       String versionName) throws RedmineServiceException;

    /**
     * Obtain the lastest closed version for a given project.
     *
     * @param projectName the name of the project
     * @return the version (or {@code null} if no version is matching)
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Version
     * @since 1.6
     */
    Version getLastestClosedVersion(String projectName) throws RedmineServiceException;

    /**
     * Obtain all issues for a given project.
     *
     * @param projectName the name of the project
     * @return the issues
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Issue
     * @since 1.0.3
     */
    Issue[] getIssues(String projectName) throws RedmineServiceException;

    /**
     * Obtain all opened issues for a given project.
     *
     * @param projectName the name of the project
     * @return the issues
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Issue
     * @since 1.0.3
     */
    Issue[] getOpenedIssues(String projectName) throws RedmineServiceException;

    /**
     * Obtain all closed issues for a given project.
     *
     * @param projectName the name of the project
     * @return the issues
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Issue
     * @since 1.0.3
     */
    Issue[] getClosedIssues(String projectName) throws RedmineServiceException;

    /**
     * Obtain all issues for a specific version on a given project.
     *
     * @param projectName the name of the project
     * @param versionName the name of the version
     * @return the issues
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Issue
     */
    Issue[] getIssues(String projectName,
                      String versionName) throws RedmineServiceException;

    /**
     * Obtain for a given issue of a given project all the time entries.
     *
     * @param projectName the name of the project
     * @param issueId     the id of the issue
     * @return the time entries of the issue
     * @throws RedmineServiceException if any pb while retrieving time entries of the issue
     * @since 1.0.3
     */
    TimeEntry[] getIssueTimeEntries(String projectName,
                                    String issueId) throws RedmineServiceException;

    /**
     * Obtain all attachments for a specific version on a given project.
     *
     * @param projectName the name of the project
     * @param versionName the name of the version
     * @return the attachments
     * @throws RedmineServiceException if any pb while retriving datas
     * @see Attachment
     */
    Attachment[] getAttachments(String projectName,
                                String versionName) throws RedmineServiceException;
}
