package org.nuiton.jredmine.tool;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigActionDef;

/**
 * Created on 7/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.8.3
 */
public enum JRedmineToolAction implements ConfigActionDef {

    FETCH_PROJECT_VERSION("To fetch all version of all projects of a forge.\nEach project will be sotred in a file, each line correspond to a version.\nUsage: JRedmineTool --fetch-project-versions redmine.config",
                          "FetchProjectsVersionsTool#run",
                          "--fetch-project-versions");

    public String description;

    public String action;

    public String[] aliases;

    JRedmineToolAction(String description, String action, String... aliases) {
        this.description = description;
        this.action = action;
        this.aliases = aliases;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public String[] getAliases() {
        return aliases;
    }

}
