package org.nuiton.jredmine.tool;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import java.lang.reflect.InvocationTargetException;

/**
 * Created on 7/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.8.3
 */
public class JRedmineToolCommandLine {

    public static void main(String[] args) throws ArgumentsParserException, IllegalAccessException, InstantiationException, InvocationTargetException {

        ApplicationConfig config = new ApplicationConfig();

        config.loadActions(JRedmineToolAction.values());

        config.parse(args);

        config.doAction(0);
    }
}
