package org.nuiton.jredmine.tool;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.service.DefaultRedmineService;
import org.nuiton.jredmine.service.RedmineConfigurationUtil;
import org.nuiton.jredmine.service.RedmineService;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.jredmine.service.RedmineServiceException;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * A tool to fetch all versions of all projects of a redmine server and store it in a file.
 *
 * We use it to generate maven technical sites.
 *
 * Created on 6/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.8.3
 */
public class FetchProjectsVersionsTool {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FetchProjectsVersionsTool.class);

    public void run(File configFile) throws RedmineServiceException, IOException {
        RedmineServiceConfiguration configuration = RedmineConfigurationUtil.newAnonymousConfiguration(configFile);

        RedmineService service = new DefaultRedmineService();

        service.init(configuration);

        DateFormat df = new SimpleDateFormat("yyy-MM-dd");

        File tempDirectory = new File(FileUtils.getTempDirectory(), "project-versions_" + System.nanoTime());
        if (log.isInfoEnabled()) {
            log.info("Generate in " + tempDirectory);
        }

        Project[] projects = service.getProjects();
        for (Project project : projects) {

            if (!project.isOpen() || !project.isIsPublic()) {
                continue;
            }

            String projectName = project.getIdentifier();
            if (log.isDebugEnabled()) {
                log.debug("Load versions of " + projectName);
            }

            Version[] versions = service.getVersions(projectName);
            printProjectVersions(df, tempDirectory, projectName, versions);

        }
    }

    protected void printProjectVersions(DateFormat df, File tempDirectory, String projectName, Version... versions) throws IOException {

        StringBuilder sb = new StringBuilder();

        for (Version version : versions) {

            if (version.isClosed()) {
                sb.append(version.getName()).append(":").append(df.format(version.getEffectiveDate())).append("\n");
            }

        }

        File versionFile = new File(tempDirectory, projectName + ".versions");
        if (log.isInfoEnabled()) {
            log.info("Write " + versionFile);
        }

        FileUtils.write(versionFile, sb.toString());

    }
}
