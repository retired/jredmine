package org.nuiton.jredmine.client;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Simple way to create a redmine request to be consumed by the {@link RedmineClient}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineRequest<T> {

    public enum Method {
        GET,
        PUT,
        POST,
        DELETE,
        HEAD,
        OPTION
    }

    protected final Method method;

    protected final Class<T> type;

    protected final List<String> path;

    protected final Map<String, String> params;

    protected final Map<String, File> attachments;


    public static <T> RedmineRequest<T> get(Class<T> type, String... path) {
        return on(Method.GET, type, path);
    }

    public static <T> RedmineRequest<T> put(Class<T> type, String... path) {
        return on(Method.PUT, type, path);
    }

    public static <T> RedmineRequest<T> delete(Class<T> type, String... path) {
        return on(Method.DELETE, type, path);
    }

    public static <T> RedmineRequest<T> post(Class<T> type, String... path) {
        return on(Method.POST, type, path);
    }

    public static <T> RedmineRequest<T> head(Class<T> type, String... path) {
        return on(Method.HEAD, type, path);
    }

    public static <T> RedmineRequest<T> option(Class<T> type, String... path) {
        return on(Method.OPTION, type, path);
    }

    public static <T> RedmineRequest<T> on(Method method, Class<T> type, String... path) {
        RedmineRequest<T> result = new RedmineRequest<T>(method, type);
        result.path(path);
        return result;
    }

    public RedmineRequest(Method method, Class<T> type) {
        this.method = method;
        this.type = type;
        params = Maps.newHashMap();
        attachments = Maps.newHashMap();
        path = Lists.newArrayList();
    }

    public RedmineRequest<T> path(String... path) {
        Collections.addAll(this.path, path);
        return this;
    }

    public RedmineRequest<T> parameter(String name, String value) {
        params.put(name, value);
        return this;
    }

    public RedmineRequest<T> attachment(String name, File value) {
        attachments.put(name, value);
        return this;
    }

    public String[] getPath() {
        return path.toArray(new String[path.size()]);
    }

    public Method getMethod() {
        return method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public Map<String, File> getAttachments() {
        return attachments;
    }

    public Class<T> getType() {
        return type;
    }

    public String toPath(String uri) {
        String result = uri + "/" +
                        Joiner.on('/').join(getPath());
        return result;
    }

}
