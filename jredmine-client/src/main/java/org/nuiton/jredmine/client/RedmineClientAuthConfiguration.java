package org.nuiton.jredmine.client;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

/**
 * Configuration of the authentication for the {@link RedmineClient}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineClientAuthConfiguration {

    public static RedmineClientAuthConfiguration newConf(String apiKey,
                                                         String username,
                                                         String password) {
        RedmineClientAuthConfiguration result =
                new RedmineClientAuthConfiguration();
        result.setApiKey(apiKey);
        result.setUsername(username);
        result.setPassword(password);
        return result;
    }

    protected String username;

    protected String password;

    protected String apiKey;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isUseApiKey() {
        return StringUtils.isNotBlank(apiKey);
    }

    public boolean isUseUsername() {
        return StringUtils.isNotBlank(username);
    }

    public boolean isUseLoginPassword() {
        return StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password);
    }

}
