package org.nuiton.jredmine.client;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Helper around {@link RedmineRequest}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineRequestHelper {

    protected RedmineRequestHelper() {
        // do not instanciate helper
    }

    public static <T> RedmineRequest<T> action(String action, Class<T> type) {
        return RedmineRequest.get(type, "jredmine", action);
    }

    public static <T> RedmineRequest<T> actionWithProject(String action, Class<T> type, String projectName) {
        return RedmineRequest.get(type, "jredmine", action, projectName);
    }

    public static <T> RedmineRequest<T> actionWithProjectAndVersion(String action, Class<T> type, String projectName, String versionName) {
        return RedmineRequest.get(type, "jredmine", action, projectName).
                parameter("version_name", versionName);
    }

    public static <T> RedmineRequest<T> actionWithProjectAndIssue(String action, Class<T> type, String projectName, String issueId) {
        return RedmineRequest.get(type, "jredmine", action, projectName).
                parameter("issue_id", issueId);
    }

    public static <T> RedmineRequest<T> postAction(String action, Class<T> type) {
        return RedmineRequest.post(type, "jredmine", action);
    }

    public static <T> RedmineRequest<T> postWithProject(String action, Class<T> type, String projectName) {
        return RedmineRequest.post(type, "jredmine", action, projectName);
    }

    public static <T> RedmineRequest<T> postWithProjectAndIssue(String action, Class<T> type, String projectName, String issueId) {
        return RedmineRequest.post(type, "jredmine", action, projectName).
                parameter("issue_id", issueId);
    }

    public static <T> RedmineRequest<T> postWithProjectAndVersion(String action, Class<T> type, String projectName, String versionName) {
        return RedmineRequest.post(type, "jredmine", action, projectName).
                parameter("version_name", versionName);
    }
}
