/*
 * #%L
 * Maven helper plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jredmine.client;

import java.net.URL;

/**
 * Configuration for a {@link RedmineClient}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public interface RedmineClientConfiguration {

    /** @return base url of server */
    URL getUrl();

    /** @return the encoding used to encode request to send */
    String getEncoding();

    /** @return {@code true} to make verbose client (show request and parameters) */
    boolean isVerbose();

//    /** @return {@code true} if rest client does not need login */
//    boolean isAnonymous();

    /** @param url the url of server to set */
    void setUrl(URL url);

    /** @param encoding the encodng used to encode request to set */
    void setEncoding(String encoding);

    /** @param verbose the flag verbose to set */
    void setVerbose(boolean verbose);

//    /** @param anonymous the flag anonymous to set */
//    void setAnonymous(boolean anonymous);

    RedmineClientAuthConfiguration getAuthConfiguration();

    void setAuthConfiguration(RedmineClientAuthConfiguration authConfiguration);

}
