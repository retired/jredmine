package org.nuiton.jredmine.client;

/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.nuiton.jredmine.model.io.xpp3.RedmineXpp3Helper;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A simple redmine client.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineClient implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RedmineClient.class);

    /** xpp3 xpp3Helper to transform xml stream to pojo */
    protected final RedmineXpp3Helper xpp3Helper;

    protected final RedmineClientConfiguration configuration;

    protected final HttpClient client;

    protected final URI serverURI;

    protected final HttpClientConnectionManager connectionManager;

    protected boolean showRequest;

    protected boolean open;

    public RedmineClient(RedmineClientConfiguration configuration) {
        this.configuration = configuration;
        xpp3Helper = new RedmineXpp3Helper();
        showRequest = configuration.isVerbose();

        Collection<Header> headers = new ArrayList<Header>();

        RedmineClientAuthConfiguration authConfiguration =
                configuration.getAuthConfiguration();
        if (authConfiguration.isUseApiKey()) {

            if (log.isDebugEnabled()) {
                log.debug("Will use api key for authentication");
            }

            // keep apiKey to put to every request
            headers.add(new BasicHeader("X-Redmine-API-Key", authConfiguration.getApiKey()));
        }

        connectionManager = new BasicHttpClientConnectionManager();
        client = HttpClientBuilder.
                create().
                setDefaultHeaders(headers).
                setConnectionManager(connectionManager).
                disableRedirectHandling().
                disableAuthCaching().
                disableCookieManagement().
                disableConnectionState().
                build();
        // Get server uri
        try {
            serverURI = configuration.getUrl().toURI();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Could not get uri from " + configuration.getUrl());
        }
    }

    public boolean isOpen() {
        return open;
    }

    public void open() throws IOException {

        if (!isOpen()) {

            // first ping
            RedmineRequest<String> pingRequest = RedmineRequestHelper.action("ping", String.class);

            open = true;

            try {
                String content = executeRequest(pingRequest);
                boolean ok = "ping".equals(content);
                if (!ok) {
                    throw new IOException("can not connect to " + configuration.getUrl());
                }
            } catch (RuntimeException e) {
                open = false;
                throw e;
            } catch (IOException e) {
                open = false;
                throw e;
            }

            open = true;
        }
    }

    /**
     * Ask some data from the server
     *
     * @param request request used for asking data
     * @return the stream of the response
     * @throws IOException if any pb while do request or receive result
     */
    public <T> T executeRequest(RedmineRequest<T> request) throws IOException {
        if (!isOpen()) {
            throw new IllegalStateException("the client is not opened");
        }

        try {

            HttpRequestBase gm = prepareRequest(request);

            ResponseHandler<T> responseHandler = new RedmineSimpleResponseHandler<T>(false, request.getType(), xpp3Helper, gm.getURI().toString());

            T result = client.execute(gm, responseHandler);
            return result;
        } finally {
            releaseConnection();
        }
    }

    /**
     * Ask some data from the server
     *
     * @param request request used for asking data
     * @return the stream of the response
     * @throws IOException if any pb while do request or receive result
     */
    public <T> T[] executeRequests(RedmineRequest<T> request) throws IOException {
        if (!isOpen()) {
            throw new IllegalStateException("the client is not opened");
        }

        try {

            HttpRequestBase gm = prepareRequest(request);

            ResponseHandler<T[]> responseHandler = new RedmineArrayResponseHandler<T>(false, request.getType(), xpp3Helper, gm.getURI().toString());

            T[] result = client.execute(gm, responseHandler);
            return result;
        } finally {
            releaseConnection();
        }
    }

    public RedmineClientConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void close() throws IOException {
        if (open) {

            try {

                connectionManager.shutdown();
            } finally {

                open = false;
            }
        }
    }

    protected <T> HttpRequestBase prepareRequest(RedmineRequest<T> request) throws IOException {

        if (showRequest) {
            log.info("prepareRequest " + getRequestUrl(request));
        }
        if (log.isDebugEnabled()) {
            log.debug("prepareRequest with parameters: " + request.getParams());
        }

        HttpRequestBase gm;
        switch (request.getMethod()) {

            case GET:
                gm = prepareGetRequest(request);
                break;
            case POST:
                gm = preparePostRequest(request);
                break;
            case PUT:
                gm = preparePutRequest(request);
                break;
            case DELETE:
                gm = prepareDeleteRequest(request);
                break;
            case HEAD:
            default:
                throw new IllegalStateException("Can not deal with method " + request.getMethod());
        }
        return gm;
    }


    protected <T> String getRequestUrl(RedmineRequest<T> request) {
        String result = request.toPath(serverURI.toString());
        return result;
    }

    protected <T> HttpGet prepareGetRequest(RedmineRequest<T> request) throws IOException {

        String uri = getRequestUrl(request);
        Map<String, String> parameters = request.getParams();
        Map<String, File> attachments = request.getAttachments();

        // multi-part request
        //not possible with a simple Get method
        Preconditions.checkState(
                MapUtils.isEmpty(attachments),
                "Can not do a GET request with multi-parts, use a POST or UPDATE request");

        HttpGet gm = new HttpGet(uri);
        addParams(gm, parameters);
        return gm;
    }

    protected <T> HttpDelete prepareDeleteRequest(RedmineRequest<T> request) throws IOException {

        String uri = getRequestUrl(request);
        Map<String, String> parameters = request.getParams();
        Map<String, File> attachments = request.getAttachments();

        // multi-part request
        //not possible with a simple Get method
        Preconditions.checkState(
                MapUtils.isEmpty(attachments),
                "Can not do a GET request with multi-parts, use a POST or UPDATE request");

        HttpDelete gm = new HttpDelete(uri);
        addParams(gm, parameters);

        return gm;
    }

    protected <T> HttpPost preparePostRequest(RedmineRequest<T> request) throws IOException {

        String uri = getRequestUrl(request);
        Map<String, String> parameters = request.getParams();
        Map<String, File> attachments = request.getAttachments();

        HttpPost gm = new HttpPost(uri);

        if (MapUtils.isEmpty(attachments)) {

            // not a multi-part request
            addParamsAsEntity(gm, parameters);
        } else {

            // multi-part request
            prepareMultiPart(gm, attachments, parameters);
        }
        return gm;
    }

    protected <T> HttpPut preparePutRequest(RedmineRequest<T> request) throws IOException {

        String uri = getRequestUrl(request);
        Map<String, String> parameters = request.getParams();
        Map<String, File> attachments = request.getAttachments();

        HttpPut gm = new HttpPut(uri);

        if (MapUtils.isEmpty(attachments)) {

            // not a multi-part request
            addParamsAsEntity(gm, parameters);

        } else {

            // multi-part request
            prepareMultiPart(gm, attachments, parameters);
        }
        return gm;
    }

    protected void addParams(HttpRequestBase gm,
                             Map<String, String> parameters) throws IOException {

        if (MapUtils.isNotEmpty(parameters)) {

            // add parameters

            URIBuilder uriBuilder = new URIBuilder(gm.getURI());
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("skip null parameter " + key);
                    }
                    continue;
                }
                uriBuilder.addParameter(key, value);
            }

            try {
                URI uri = uriBuilder.build();
                gm.setURI(uri);
            } catch (URISyntaxException e) {
                throw new IOException("Could not build uri", e);
            }
        }
    }

    protected void addParamsAsEntity(HttpEntityEnclosingRequest gm,
                                     Map<String, String> parameters) throws IOException {

        if (MapUtils.isNotEmpty(parameters)) {

            // add parameters

            List<NameValuePair> params = Lists.newArrayList();

            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("skip null parameter " + key);
                    }
                    continue;
                }
                params.add(new BasicNameValuePair(key, value));
            }

            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(
                    params,
                    configuration.getEncoding());
            gm.setEntity(entity);
        }
    }

    protected void prepareMultiPart(HttpEntityEnclosingRequestBase gm,
                                    Map<String, File> attachments,
                                    Map<String, String> parameters) throws IOException {

        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        if (MapUtils.isNotEmpty(parameters)) {

            // add parameters as part of multi-part

            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("skip null parameter " + key);
                    }
                    continue;
                }
                multipartEntityBuilder.addTextBody(key, value);
            }
        }

        // add file parts
        for (Map.Entry<String, File> entry : attachments.entrySet()) {
            String key = entry.getKey();
            File file = entry.getValue();
            if (log.isDebugEnabled()) {
                log.debug("add attachment " + key + "=" + file);
            }
            multipartEntityBuilder.addBinaryBody(key, file);
        }
        if (attachments.isEmpty()) {
            log.warn("no attachment in a multi-part request!");
        }

        HttpEntity entity = multipartEntityBuilder.build();
        if (log.isDebugEnabled()) {
            entity.writeTo(System.out);
        }
        gm.setEntity(entity);
        gm.addHeader("content-type", entity.getContentType().getValue());
        if (showRequest) {
            log.info("content-type : " + entity.getContentType() +
                     ", content-length : " + entity.getContentLength());
        }
    }

    protected void releaseConnection() {
        connectionManager.closeExpiredConnections();
    }

    protected static abstract class AbstractRedmineResponseHandler<T> {

        final boolean strict;

        final Class<T> type;

        protected final RedmineXpp3Helper xpp3Helper;

        final String uri;

        AbstractRedmineResponseHandler(boolean strict,
                                       Class<T> type,
                                       RedmineXpp3Helper xpp3Helper, String uri) {
            this.strict = strict;
            this.type = type;
            this.xpp3Helper = xpp3Helper;
            this.uri = uri;
        }

        protected void checkResponse(HttpResponse response) throws IOException {
            StatusLine sl = response.getStatusLine();
            int statusCode = sl.getStatusCode();

            if (log.isDebugEnabled()) {
                log.debug("status code " + statusCode + " for " + uri);
            }

            HttpEntity entity = response.getEntity();

            if (statusCode == HttpStatus.SC_NOT_FOUND) {
                String responseAsString = EntityUtils.toString(entity);
                throw new IOException("could not retreave some datas : " + responseAsString);
            }

            if (statusCode != HttpStatus.SC_OK) {
                String responseAsString = EntityUtils.toString(entity);
                log.error("Error = " + responseAsString);
                throw new IOException("Got error code <" + statusCode + ":" + sl.getReasonPhrase() + "> on " + uri);
            }
        }
    }


    protected static class RedmineSimpleResponseHandler<T> extends AbstractRedmineResponseHandler<T> implements ResponseHandler<T> {

        RedmineSimpleResponseHandler(boolean strict,
                                     Class<T> type,
                                     RedmineXpp3Helper xpp3Helper,
                                     String uri) {
            super(strict, type, xpp3Helper, uri);
        }

        @Override
        public T handleResponse(HttpResponse response) throws IOException {
            checkResponse(response);
            T result;

            HttpEntity entity = response.getEntity();

            if (Void.class == type || void.class == type) {

                EntityUtils.consume(entity);
                result = null;
            } else if (String.class == type) {

                result = (T) EntityUtils.toString(entity);
            } else {
                InputStream inputStream = entity.getContent();
                try {
                    result = this.xpp3Helper.readObject(
                            type, inputStream, strict);
                    inputStream.close();
                } catch (XmlPullParserException e) {
                    throw new IOException(
                            "Could not parse response fro type " + type, e);
                } finally {
                    IOUtils.closeQuietly(inputStream);
                }
            }
            return result;
        }
    }

    protected static class RedmineArrayResponseHandler<T> extends AbstractRedmineResponseHandler<T> implements ResponseHandler<T[]> {

        RedmineArrayResponseHandler(boolean strict,
                                    Class<T> type,
                                    RedmineXpp3Helper xpp3Helper, String uri) {
            super(strict, type, xpp3Helper, uri);
        }

        @Override
        public T[] handleResponse(HttpResponse response) throws IOException {
            checkResponse(response);

            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            try {
                T[] result = this.xpp3Helper.readObjects(
                        type, inputStream, strict);
                inputStream.close();
                return result;
            } catch (XmlPullParserException e) {
                throw new IOException(
                        "Could not parse response fro type " + type, e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
    }
}
