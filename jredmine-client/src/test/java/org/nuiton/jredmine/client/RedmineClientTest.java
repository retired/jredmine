/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jredmine.RedmineFixtures;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.service.RedmineConfigurationUtil;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.jredmine.test.RedmineFixtureClassRule;

import java.util.Arrays;

/**
 * Tests the {@link RedmineClient}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class RedmineClientTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RedmineClientTest.class);

    @ClassRule
    public static final RedmineFixtureClassRule<RedmineFixtures> checkConfigRule =
            RedmineFixtureClassRule.newFixtures(RedmineFixtures.class);

    protected RedmineFixtures getFixtures() {
        return checkConfigRule.getFixtures();
    }

    protected RedmineClient client;

    protected String projectName;

    protected String versionName;

    protected String issueId;

    @Before
    public void setUp() throws Exception {

        RedmineFixtures fixtures = checkConfigRule.getFixtures();

        client = new RedmineClient(getConfiguration());
        client.open();

        projectName = fixtures.projectName();
        versionName = fixtures.versionName();
        issueId = fixtures.issueId();
    }

    @After
    public void tearDown() throws Exception {

        if (client != null) {
            client.close();
        }
    }

    @Test
    public void isOpen() throws Exception {
        Assert.assertTrue(client.isOpen());
    }

    @Test
    public void isClose() throws Exception {
        Assert.assertTrue(client.isOpen());
        client.close();
        Assert.assertFalse(client.isOpen());
    }

    @Test
    public void getProjects() throws Exception {
        RedmineRequest<Project> request = RedmineRequestHelper.action(
                "get_projects.xml", Project.class);
        askDatas(request);
    }

    @Test
    public void getIssuePriorities() throws Exception {
        RedmineRequest<IssuePriority> request = RedmineRequestHelper.action(
                "get_issue_priorities.xml", IssuePriority.class);
        askDatas(request);
    }

    @Test
    public void getIssueStatuses() throws Exception {
        RedmineRequest<IssueStatus> request =
                RedmineRequestHelper.action("get_issue_statuses.xml",
                                            IssueStatus.class);
        askDatas(request);
    }

    @Test
    public void getProject() throws Exception {
        RedmineRequest<Project> request =
                RedmineRequestHelper.actionWithProject("get_project.xml",
                                                       Project.class,
                                                       projectName);
        askData(request);
    }

    @Test
    public void getIssueCategories() throws Exception {
        RedmineRequest<IssueCategory> request =
                RedmineRequestHelper.actionWithProject(
                        "get_issue_categories.xml",
                        IssueCategory.class,
                        projectName);
        askDatas(request);
    }

    @Test
    public void getTrackers() throws Exception {
        RedmineRequest<Tracker> request =
                RedmineRequestHelper.actionWithProject("get_project_trackers.xml",
                                                       Tracker.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getNews() throws Exception {
        RedmineRequest<News> request =
                RedmineRequestHelper.actionWithProject("get_project_news.xml",
                                                       News.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getProjectMembers() throws Exception {
        RedmineRequest<User> request =
                RedmineRequestHelper.actionWithProject("get_project_users.xml",
                                                       User.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getProjectIssues() throws Exception {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version_issues.xml",
                                                                 Issue.class,
                                                                 projectName,
                                                                 versionName);
        askDatas(request);
    }

    @Test
    public void getVersions() throws Exception {
        RedmineRequest<Version> request =
                RedmineRequestHelper.actionWithProject("get_project_versions.xml",
                                                       Version.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getVersion() throws Exception {
        RedmineRequest<Version> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version.xml",
                                                                 Version.class,
                                                                 projectName,
                                                                 versionName);
        askData(request);
    }

    @Test
    public void getVersionIssues() throws Exception {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProjectAndVersion("get_version_issues.xml",
                                                                 Issue.class,
                                                                 projectName,
                                                                 versionName);
        askDatas(request);
    }

    @Test
    public void getOpenedIssues() throws Exception {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProject("get_project_opened_issues.xml",
                                                       Issue.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getClosedIssues() throws Exception {
        RedmineRequest<Issue> request =
                RedmineRequestHelper.actionWithProject("get_project_closed_issues.xml",
                                                       Issue.class,
                                                       projectName);
        askDatas(request);
    }

    @Test
    public void getIssueTimeEntries() throws Exception {
        RedmineRequest<TimeEntry> request =
                RedmineRequestHelper.actionWithProjectAndIssue("get_issue_times.xml",
                                                               TimeEntry.class,
                                                               projectName,
                                                               issueId);
        askDatas(request);
    }

    @Test
    public void getAttachments() throws Exception {
        RedmineRequest<Attachment> request = RedmineRequestHelper.actionWithProjectAndVersion(
                "get_version_attachments.xml", Attachment.class, projectName, versionName);
        askDatas(request);
    }

    @Test
    public void getUser() throws Exception {
        assumeIsLoggued();
        RedmineRequest<User> request = RedmineRequestHelper.action(
                "get_user.xml", User.class);
        askData(request);
    }

    @Test
    public void getUserProjects() throws Exception {

        assumeIsLoggued();
        RedmineRequest<Project> request = RedmineRequestHelper.action(
                "get_user_projects.xml", Project.class);
        askDatas(request);
    }

    @Test
    public void addVersion() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    @Test
    public void addAttachment() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    @Test
    public void addNews() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    public void assumeIsLoggued() {
        boolean loggued = RedmineConfigurationUtil.isLoggued(getConfiguration());
        Assume.assumeTrue(loggued);
    }

    @Test
    public void updateVersion() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    @Test
    public void nextVersion() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    @Test
    public void addIssueTime() throws Exception {
        assumeIsLoggued();
        //TODO
    }

    protected <T> void askData(RedmineRequest<T> request) throws Exception {
        T askData = client.executeRequest(request);
        if (getConfiguration().isVerbose() && log.isInfoEnabled()) {
            String toString;
            if (askData == null) {
                toString = "Return is null!";
            } else {
                toString = askData.toString();
            }
            log.info(toString);
        }
    }

    protected <T> void askDatas(RedmineRequest<T> request) throws Exception {

        T[] askData = client.executeRequests(request);
        if (getConfiguration().isVerbose() && log.isInfoEnabled()) {
            String toString;
            if (askData == null) {
                toString = "Return is null!";
            } else {
                toString = Arrays.toString((Object[]) askData);
            }
            log.info(toString);
        }
    }

    protected RedmineServiceConfiguration getConfiguration() {
        return checkConfigRule.getConf();
    }
}
