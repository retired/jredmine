package org.nuiton.jredmine.test.tck;
/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.FileUtils;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.test.AbstractRedmineFixtures;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RedmineTCKFixtures extends AbstractRedmineFixtures {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RedmineTCKFixtures.class);

    public String projectName() {
        return "project1";
    }

    public String versionName() {
        return "1.0";
    }

    public String versionId() {
        return "1";
    }

    public String issueId() {
        return "1";
    }

    public Version versionToAdd() {
        Version version = new Version();
        version.setId(TIMESTAMP);
        version.setName(versionName() + "_" + TIMESTAMP);
        version.setDescription("Version to add");
        return version;
    }

    public Version versionToUpdate() {
        Version version = new Version();
        version.setName(versionName());
        version.setId(Integer.valueOf(versionId()));
        version.setDescription("Version to update");
        return version;
    }

    public String oldVersionName() {
        return "1.2";
    }

    public News newsToAdd() {
        News news = new News();
        news.setTitle("Title");
        news.setDescription("Description");
        news.setSummary("Summary");
        return news;
    }

    public TimeEntry timeEntryToAdd() {
        TimeEntry timeEntry = new TimeEntry();
        timeEntry.setComments("Comments");
        Date date = new Date();
        DateUtils.setYears(date, 2012);
        date = DateUtils.setMonths(date, 6);
        date = DateUtils.setDays(date, 15);
        timeEntry.setSpentOn(date);
        return timeEntry;
    }

    public Attachment attachmentToAdd() {
        Attachment attachmentToAdd = new Attachment();
        attachmentToAdd.setDescription("Description");
        File tmpDir = FileUtils.createTempFile("tmpDir", null, null);
        File file = new File(tmpDir, TIMESTAMP + ".txt");
        try {
            FileUtils.mkdir(file.getParent());
            FileUtils.fileWrite(file, "Content of file " + TIMESTAMP);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not write file content to " + file, e);
            }
        }
        attachmentToAdd.setToUpload(file);
        return attachmentToAdd;
    }
}
