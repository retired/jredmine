package org.nuiton.jredmine.test;
/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.jredmine.service.RedmineAnonymousService;
import org.nuiton.jredmine.service.RedmineService;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.jredmine.service.RedmineServiceException;

import java.io.IOException;

/**
 * A redmine server resource used by tests.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class RedmineServer<S extends RedmineAnonymousService, F> extends TestWatcher {

    public static <F extends AbstractRedmineFixtures> RedmineServer<RedmineAnonymousService, F> newAnonymousServer(RedmineAnonymousFixtureClassRule<F> rule) {
        return new RedmineServer<RedmineAnonymousService, F>(rule.getFixtures()) {

            @Override
            protected RedmineAnonymousService createService(
                    RedmineServiceConfiguration configuration) throws IOException, RedmineServiceException {
                return fixtures.newRedmineAnonymousService(configuration);
            }

            @Override
            protected RedmineServiceConfiguration createConfiguration() throws IOException {
                return fixtures.newAnonymousConfiguration();
            }
        };
    }

    public static <F extends AbstractRedmineFixtures> RedmineServer<RedmineService, F> newLogguedServerAsAnonymous(RedmineAnonymousFixtureClassRule<F> rule) {
        return new RedmineServer<RedmineService, F>(rule.getFixtures()) {

            @Override
            protected RedmineService createService(
                    RedmineServiceConfiguration configuration) throws IOException, RedmineServiceException {
                return fixtures.newRedmineService(configuration);
            }

            @Override
            protected RedmineServiceConfiguration createConfiguration() throws IOException {
                return fixtures.newAnonymousConfiguration();
            }
        };
    }

    public static <F extends AbstractRedmineFixtures> RedmineServer<RedmineService, F> newLogguedServer(RedmineFixtureClassRule<F> rule) {
        return new RedmineServer<RedmineService, F>(rule.getFixtures()) {

            @Override
            protected RedmineService createService(
                    RedmineServiceConfiguration configuration) throws IOException, RedmineServiceException {
                return fixtures.newRedmineService(configuration);
            }

            @Override
            protected RedmineServiceConfiguration createConfiguration() throws IOException {
                return fixtures.newLogguedConfiguration();
            }
        };
    }

    /**
     * Fixtures.
     *
     * @since 1.4
     */
    protected final F fixtures;

    /**
     * Service configuration to use.
     *
     * @since 1.4
     */
    protected RedmineServiceConfiguration configuration;

    /**
     * Redmine service to use.
     *
     * @since 1.4
     */
    protected S service;

    public RedmineServer(F fixtures) {
        this.fixtures = fixtures;
    }

    protected abstract S createService(RedmineServiceConfiguration configuration) throws IOException, RedmineServiceException;

    protected abstract RedmineServiceConfiguration createConfiguration() throws IOException;

    public F getFixtures() {
        return fixtures;
    }

    public RedmineServiceConfiguration getConfiguration() {
        return configuration;
    }

    public S getService() {
        return service;
    }

    @Override
    protected void starting(Description description) {

        try {
            configuration = createConfiguration();
        } catch (IOException e) {

            throw new IllegalStateException("Could not create configuration", e);
        }

        try {
            service = createService(configuration);
        } catch (Exception e) {
            throw new IllegalStateException("Could not create service", e);
        }
    }

    @Override
    protected void finished(Description description) {

        if (service != null) {
            try {
                service.destroy();
            } catch (RedmineServiceException e) {
                throw new IllegalStateException("Could not close service ", e);
            }
        }
    }

}
