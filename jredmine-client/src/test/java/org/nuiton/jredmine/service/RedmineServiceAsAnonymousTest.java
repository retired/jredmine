package org.nuiton.jredmine.service;
/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.jredmine.RedmineFixtures;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.test.RedmineAnonymousFixtureClassRule;
import org.nuiton.jredmine.test.RedmineServer;

/**
 * Tests the {@link RedmineService} service with anonymous configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RedmineServiceAsAnonymousTest {

    @ClassRule
    public static final RedmineAnonymousFixtureClassRule<RedmineFixtures> classRule =
            RedmineAnonymousFixtureClassRule.newFixtures(RedmineFixtures.class);

    @Rule
    public final RedmineServer<RedmineService, RedmineFixtures> server =
            RedmineServer.newLogguedServerAsAnonymous(classRule);

    protected RedmineFixtures getFixtures() {
        return server.getFixtures();
    }

    protected RedmineService getService() {
        return server.getService();
    }

    @Test
    public void getProjects() throws Exception {
        Project[] projects = getService().getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.length > 0);
    }

    @Test
    public void getProject() throws Exception {
        Project project = getService().getProject(getFixtures().projectName());
        Assert.assertNotNull(project);
    }

    @Test
    public void getNews() throws Exception {
        News[] news = getService().getNews(getFixtures().projectName());
        Assert.assertNotNull(news);
        Assert.assertTrue(news.length > 0);
    }

    @Test
    public void getProjectIssues() throws Exception {
        Issue[] issues = getService().getIssues(getFixtures().projectName());
        Assert.assertNotNull(issues);
        Assert.assertTrue(issues.length > 0);
    }

    @Test
    public void getVersions() throws Exception {
        Version[] versions = getService().getVersions(getFixtures().projectName());
        Assert.assertNotNull(versions);
        Assert.assertTrue(versions.length > 0);
    }

    @Test
    public void getVersion() throws Exception {
        Version version = getService().getVersion(getFixtures().projectName(), getFixtures().versionName());
        Assert.assertNotNull(version);
    }

    @Test
    public void getIssueCategories() throws Exception {
        // Need to be loggued ? don't know why :( Need to report a bug
        IssueCategory[] issueCategories = getService().getIssueCategories(getFixtures().projectName());
        Assert.assertNotNull(issueCategories);
        Assert.assertTrue(issueCategories.length > 0);
    }

    @Test
    public void getProjectMembers() throws Exception {
        // Need to be loggued ? don't know why :( Need to report a bug
        User[] users = getService().getProjectMembers(getFixtures().projectName());
        Assert.assertNotNull(users);
        Assert.assertTrue(users.length > 0);
    }

    @Test
    public void getOpenedIssues() throws Exception {
        Issue[] issues = getService().getOpenedIssues(getFixtures().projectName());
        Assert.assertNotNull(issues);
        Assert.assertTrue(issues.length > 0);
    }

    @Test
    public void getClosedIssues() throws Exception {
        Issue[] issues = getService().getClosedIssues(getFixtures().projectName());
        Assert.assertNotNull(issues);
        Assert.assertTrue(issues.length > 0);
    }

    @Test
    public void getIssueTimeEntries() throws Exception {
        TimeEntry[] timeEntries = getService().getIssueTimeEntries(getFixtures().projectName(), getFixtures().issueId());
        Assert.assertNotNull(timeEntries);
        Assert.assertTrue(timeEntries.length > 0);
    }

    @Test
    public void getIssuePriorities() throws Exception {
        IssuePriority[] issuePriorities = getService().getIssuePriorities();
        Assert.assertNotNull(issuePriorities);
        Assert.assertTrue(issuePriorities.length > 0);
    }

    @Test
    public void getIssueStatuses() throws Exception {
        IssueStatus[] issueStatuses = getService().getIssueStatuses();
        Assert.assertNotNull(issueStatuses);
        Assert.assertTrue(issueStatuses.length > 0);
    }

    @Test
    public void getTrackers() throws Exception {
        Tracker[] trackers = getService().getTrackers(getFixtures().projectName());
        Assert.assertNotNull(trackers);
        Assert.assertTrue(trackers.length > 0);
    }

    @Test
    public void getVersionIssues() throws Exception {
        Issue[] issues = getService().getIssues(getFixtures().projectName(), getFixtures().versionName());
        Assert.assertNotNull(issues);
        Assert.assertTrue(issues.length > 0);
    }

    @Test
    public void getAttachments() throws Exception {
        Attachment[] attachments = getService().getAttachments(getFixtures().projectName(), getFixtures().versionName());
        Assert.assertNotNull(attachments);
        Assert.assertTrue(attachments.length > 0);
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void getUserProjects() throws Exception {
        getService().getUserProjects();
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void addVersion() throws Exception {
        getService().addVersion(getFixtures().projectName(), new Version());
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void addAttachment() throws Exception {
        getService().addAttachment(getFixtures().projectName(), getFixtures().versionName(), new Attachment());
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void addNews() throws Exception {
        getService().addNews(getFixtures().projectName(), new News());
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void updateVersion() throws Exception {
        getService().updateVersion(getFixtures().projectName(), new Version());
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void nextVersion() throws Exception {
        getService().nextVersion(getFixtures().projectName(), getFixtures().versionName(), new Version());
    }

    @Test(expected = RedmineServiceLoginException.class)
    public void addIssueTime() throws Exception {
        getService().addIssueTimeEntry(getFixtures().projectName(), getFixtures().issueId(), new TimeEntry());
    }
}
