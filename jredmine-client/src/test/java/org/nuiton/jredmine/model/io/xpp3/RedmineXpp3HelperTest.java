/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.model.io.xpp3;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.model.io.xpp3.api.AbstractXpp3Reader;
import org.nuiton.jredmine.model.io.xpp3.api.PropertyMapper;
import org.nuiton.jredmine.model.io.xpp3.api.Xpp3Helper;

import java.beans.Introspector;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@RunWith(Parameterized.class)
public class RedmineXpp3HelperTest<T> {

    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"single", Attachment.class, newSingleData(Attachment.class)},
                {"single", Issue.class, newSingleData(Issue.class)},
                {"single", IssueCategory.class, newSingleData(IssueCategory.class)},
                {"single", IssuePriority.class, newSingleData(IssuePriority.class)},
                {"single", IssueStatus.class, newSingleData(IssueStatus.class)},
                {"single", News.class, newSingleData(News.class)},
                {"single", Project.class, newSingleData(Project.class)},
                {"single", TimeEntry.class, newSingleData(TimeEntry.class)},
                {"single", Tracker.class, newSingleData(Tracker.class)},
                {"single", User.class, newSingleData(User.class)},
                {"single", Version.class, newSingleData(Version.class)},
                {"single2", Issue.class, new SingleDataSupplier<Issue>(Issue.class) {

                    @Override
                    public List<Issue> get() {
                        List<Issue> issues = super.get();
                        issues.get(0).setIsPrivate(true);
                        return issues;
                    }

                    @Override
                    public void close() throws IOException {
                        super.close();
                        List<Issue> issues = super.get();
                        issues.get(0).setIsPrivate(false);
                    }
                }},
                {"single2", User.class, new SingleDataSupplier<User>(User.class) {

                    @Override
                    public List<User> get() {
                        List<User> users = super.get();
                        users.get(0).setSalt("38006729a049cd820aafd6c2bb3b193f");
                        return users;
                    }

                    @Override
                    public void close() throws IOException {
                        super.close();
                        List<User> users = get();
                        users.get(0).setSalt(null);
                    }
                }},

                {"array-empty", Attachment.class, newEmptyListData(Attachment.class)},
                {"array-empty", Issue.class, newEmptyListData(Issue.class)},
                {"array-empty", IssueCategory.class, newEmptyListData(Attachment.class)},
                {"array-empty", IssuePriority.class, newEmptyListData(IssuePriority.class)},
                {"array-empty", IssueStatus.class, newEmptyListData(IssueStatus.class)},
                {"array-empty", News.class, newEmptyListData(News.class)},
                {"array-empty", Project.class, newEmptyListData(Project.class)},
                {"array-empty", TimeEntry.class, newEmptyListData(TimeEntry.class)},
                {"array-empty", Tracker.class, newEmptyListData(Tracker.class)},
                {"array-empty", User.class, newEmptyListData(User.class)},
                {"array-empty", Version.class, newEmptyListData(Version.class)},

                {"array-empty2", Attachment.class, newEmptyListData(Attachment.class)},
                {"array-empty2", Issue.class, newEmptyListData(Issue.class)},
                {"array-empty2", IssueCategory.class, newEmptyListData(IssueCategory.class)},
                {"array-empty2", IssuePriority.class, newEmptyListData(IssuePriority.class)},
                {"array-empty2", IssueStatus.class, newEmptyListData(IssueStatus.class)},
                {"array-empty2", News.class, newEmptyListData(News.class)},
                {"array-empty2", Project.class, newEmptyListData(Project.class)},
                {"array-empty2", TimeEntry.class, newEmptyListData(TimeEntry.class)},
                {"array-empty2", Tracker.class, newEmptyListData(Tracker.class)},
                {"array-empty2", User.class, newEmptyListData(User.class)},
                {"array-empty2", Version.class, newEmptyListData(Version.class)},


                {"array-singleton", Attachment.class, newSingletonListData(Attachment.class)},
                {"array-singleton", Issue.class, newSingletonListData(Issue.class)},
                {"array-singleton", IssueCategory.class, newSingletonListData(IssueCategory.class)},
                {"array-singleton", IssuePriority.class, newSingletonListData(IssuePriority.class)},
                {"array-singleton", IssueStatus.class, newSingletonListData(IssueStatus.class)},
                {"array-singleton", News.class, newSingletonListData(News.class)},
                {"array-singleton", Project.class, newSingletonListData(Project.class)},
                {"array-singleton", TimeEntry.class, newSingletonListData(TimeEntry.class)},
                {"array-singleton", Tracker.class, newSingletonListData(Tracker.class)},
                {"array-singleton", User.class, newSingletonListData(User.class)},
                {"array-singleton", Version.class, newSingletonListData(Version.class)},

                {"array-multi", Attachment.class, newMultiListData(Attachment.class)},
                {"array-multi", Issue.class, newMultiListData(Issue.class)},
                {"array-multi", IssueCategory.class, newMultiListData(IssueCategory.class)},
                {"array-multi", IssuePriority.class, newMultiListData(IssuePriority.class)},
                {"array-multi", IssueStatus.class, newMultiListData(IssueStatus.class)},
                {"array-multi", News.class, newMultiListData(News.class)},
                {"array-multi", Project.class, newMultiListData(Project.class)},
                {"array-multi", TimeEntry.class, newMultiListData(TimeEntry.class)},
                {"array-multi", Tracker.class, newMultiListData(Tracker.class)},
                {"array-multi", User.class, newMultiListData(User.class)},
                {"array-multi", Version.class, newMultiListData(Version.class)},
        });
    }

    private static RedmineXpp3Helper builder;

    private static RedmineModelFixtures fixtures;

    private final Class<T> type;

    private final Supplier<List<T>> result;

    private final String resourcePath;

    public RedmineXpp3HelperTest(String path,
                                 Class<T> type,
                                 Supplier<List<T>> result) {
        Preconditions.checkNotNull(path);
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(result);
        this.type = type;
        this.result = result;
        String dotPath = "." + getClass().getPackage().getName() +
                         "." + path +
                         "." + Introspector.decapitalize(type.getSimpleName());
        resourcePath = dotPath.replaceAll("\\.", "/") + ".xml";
    }

    @BeforeClass
    public static void setUpClass() throws Exception {

        builder = new RedmineXpp3Helper();

        // setup memory model
        fixtures = new RedmineModelFixtures();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        builder = null;
        fixtures = null;
    }

    @After
    public void tearDown() throws IOException {
        if (result instanceof Closeable) {
            ((Closeable) result).close();
        }
    }

    /**
     * Test the method {@code read} of class ModelBuilder.
     *
     * @throws Exception for any exceptions
     */
    @Test
    public void testRead() throws Exception {

        List<T> expected = result.get();

        InputStream stream = getClass().getResourceAsStream(resourcePath);
        try {

            if (result instanceof SingleDataSupplier<?>) {

                T actual = builder.readObject(type, stream, false);
                assertMyEquals(type, expected.get(0), actual);

            } else {

                T[] actual = builder.readObjects(type, stream, false);
                assertMyListEquals(type, expected, actual);
            }
        } catch (Exception e) {
            throw new IllegalStateException(
                    "Could not read from resource " + resourcePath, e);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    static class SingleDataSupplier<T> implements Supplier<List<T>>, Closeable {

        protected final Class<T> type;

        SingleDataSupplier(Class<T> type) {
            this.type = type;
        }

        @Override
        public List<T> get() {
            T t = fixtures.get(type, 0);
            List<T> result = Lists.newArrayList();
            result.add(t);
            return result;
        }

        @Override
        public void close() throws IOException {
        }
    }

    static class EmptyListDataSupplier<T> implements Supplier<List<T>>, Closeable {

        protected final Class<T> type;

        EmptyListDataSupplier(Class<T> type) {
            this.type = type;
        }

        @Override
        public List<T> get() {
            return Collections.emptyList();
        }

        @Override
        public void close() throws IOException {
        }
    }

    static class SingletonListDataSupplier<T> implements Supplier<List<T>>, Closeable {

        protected final Class<T> type;

        SingletonListDataSupplier(Class<T> type) {
            this.type = type;
        }

        @Override
        public List<T> get() {
            T model = fixtures.get(type, 0);
            List<T> result = Lists.newArrayList();
            result.add(model);
            return result;
        }

        @Override
        public void close() throws IOException {
        }
    }

    static class MultiListDataSupplier<T> implements Supplier<List<T>>, Closeable {

        protected final Class<T> type;

        MultiListDataSupplier(Class<T> type) {
            this.type = type;
        }

        @Override
        public List<T> get() {
            return fixtures.get(type);
        }

        @Override
        public void close() throws IOException {
        }
    }

    static <T> SingleDataSupplier<T> newSingleData(Class<T> type) {
        return new SingleDataSupplier<T>(type);
    }

    static <T> EmptyListDataSupplier<T> newEmptyListData(Class<T> type) {
        return new EmptyListDataSupplier<T>(type);
    }

    static <T> SingletonListDataSupplier<T> newSingletonListData(Class<T> type) {
        return new SingletonListDataSupplier<T>(type);
    }

    static <T> MultiListDataSupplier<T> newMultiListData(Class<T> type) {
        return new MultiListDataSupplier<T>(type);
    }

    <T> void assertMyListEquals(Class<T> type, List<T> expecteds, T... actuals)
            throws Exception {

        Assert.assertNotNull(actuals);
        Assert.assertEquals(expecteds.size(), actuals.length);
        for (int i = 0, j = expecteds.size(); i < j; i++) {
            T actual = actuals[i];
            T expected = expecteds.get(i);
            assertMyEquals(type, expected, actual);
        }

    }

    public void assertMyEquals(Class<?> type, Object expected, Object actual, String... mapperNames)
            throws Exception {

        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        AbstractXpp3Reader<?> reader = (AbstractXpp3Reader<?>) Xpp3Helper.getReader(type);
        Map<String, PropertyMapper> mappers = reader.getMappers(type);
        List<String> mappersIncludes;
        if (mapperNames != null && mapperNames.length > 0) {
            mappersIncludes = Lists.newArrayList(mapperNames);
        } else {
            mappersIncludes = null;
        }
        for (PropertyMapper m : mappers.values()) {

            if (mappersIncludes == null || mappersIncludes.contains(m.getPropertyName())) {

                Object expectedValue = m.getDescriptor().getReadMethod().invoke(expected);
                Object actualValue = m.getDescriptor().getReadMethod().invoke(actual);

                Assert.assertEquals("error in attribute " + m.getPropertyName() + " for " + expected, expectedValue,
                                    actualValue);
            }
        }
    }
}
