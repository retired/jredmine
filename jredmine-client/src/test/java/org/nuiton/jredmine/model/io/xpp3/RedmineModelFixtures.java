package org.nuiton.jredmine.model.io.xpp3;
/*
 * #%L
 * JRedmine :: Client
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.TimeEntry;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.test.AbstractRedmineFixtures;

import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RedmineModelFixtures extends AbstractRedmineFixtures {

    private ArrayListMultimap<Class<?>, Object> model;

    public <T> List<T> get(Class<T> modelType) {
        if (model == null) {
            try {
                loadModel();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return (List<T>) model.get(modelType);
    }

    public <T> T get(Class<T> type, int pos) {
        List<T> ts = get(type);
        return ts.get(pos);
    }

    private void loadModel()
            throws Exception {
        model = ArrayListMultimap.create();

        Attachment tempA;
        tempA = new Attachment();
        tempA.setAuthorId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempA.setContainerId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempA.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempA.setFilesize((Integer) RedmineDataConverter.Integer.convert("411"));
        tempA.setDownloads((Integer) RedmineDataConverter.Integer.convert("0"));
        tempA.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T12:56:41+02:00"));
        tempA.setContainerType((String) RedmineDataConverter.Text.convert("Version"));
        tempA.setContentType((String) RedmineDataConverter.Text.convert("application/json"));
        tempA.setDigest((String) RedmineDataConverter.Text.convert("6ea84342c7475c05fb077b4aca832f9a"));
        tempA.setDiskFilename((String) RedmineDataConverter.Text.convert("090905125641_get_issue.json"));
        tempA.setFilename((String) RedmineDataConverter.Text.convert("get_issue.json"));
        model.put(Attachment.class, tempA);
        tempA = new Attachment();
        tempA.setAuthorId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempA.setContainerId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempA.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempA.setFilesize((Integer) RedmineDataConverter.Integer.convert("411"));
        tempA.setDownloads((Integer) RedmineDataConverter.Integer.convert("0"));
        tempA.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T12:56:41+02:00"));
        tempA.setContainerType((String) RedmineDataConverter.Text.convert("Version"));
        tempA.setContentType((String) RedmineDataConverter.Text.convert("application/json"));
        tempA.setDigest((String) RedmineDataConverter.Text.convert("6ea84342c7475c05fb077b4aca832f9a"));
        tempA.setDiskFilename((String) RedmineDataConverter.Text.convert("090905125641_get_issue.json2"));
        tempA.setFilename((String) RedmineDataConverter.Text.convert("get_issue.json2"));
        model.put(Attachment.class, tempA);

        Issue tempI;
        tempI = new Issue();
        tempI.setAuthorId((Integer) RedmineDataConverter.Integer.convert("5"));
        tempI.setCategoryId((Integer) RedmineDataConverter.Integer.convert("2"));
        tempI.setDoneRatio((Integer) RedmineDataConverter.Integer.convert("0"));
        tempI.setLockVersion((Integer) RedmineDataConverter.Integer.convert("7"));
        tempI.setPriorityId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempI.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setStatusId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempI.setTrackerId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setFixedVersionId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempI.setParentId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempI.setRootId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempI.setLft((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setRgt((Integer) RedmineDataConverter.Integer.convert("2"));
        tempI.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T20:11:52+02:00"));
        tempI.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T00:37:40+02:00"));
        tempI.setStartDate((Date) RedmineDataConverter.Date.convert("2009-09-04"));
        tempI.setDescription((String) RedmineDataConverter.Text.convert("avec une description !"));
        tempI.setSubject((String) RedmineDataConverter.Text.convert("yes!"));
        model.put(Issue.class, tempI);
        tempI = new Issue();
        tempI.setAuthorId((Integer) RedmineDataConverter.Integer.convert("5"));
        tempI.setCategoryId((Integer) RedmineDataConverter.Integer.convert("2"));
        tempI.setDoneRatio((Integer) RedmineDataConverter.Integer.convert("0"));
        tempI.setLockVersion((Integer) RedmineDataConverter.Integer.convert("7"));
        tempI.setPriorityId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempI.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setStatusId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempI.setTrackerId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setFixedVersionId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempI.setId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempI.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T20:11:52+02:00"));
        tempI.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T00:37:40+02:00"));
        tempI.setStartDate((Date) RedmineDataConverter.Date.convert("2009-09-04"));
        tempI.setDescription((String) RedmineDataConverter.Text.convert("avec une description !2"));
        tempI.setSubject((String) RedmineDataConverter.Text.convert("yes!2"));
        model.put(Issue.class, tempI);

        Project tempP;
        tempP = new Project();
        tempP.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T18:11:54+02:00"));
        tempP.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T18:11:54+02:00"));
        tempP.setIdentifier((String) RedmineDataConverter.Text.convert("one"));
        tempP.setName((String) RedmineDataConverter.Text.convert("one"));
        tempP.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempP.setLft((Integer) RedmineDataConverter.Integer.convert("1"));
        tempP.setRgt((Integer) RedmineDataConverter.Integer.convert("2"));
        tempP.setProjectsCount((Integer) RedmineDataConverter.Integer.convert("0"));
        tempP.setStatus((Integer) RedmineDataConverter.Integer.convert("1"));
        tempP.setIsPublic((Boolean) RedmineDataConverter.Boolean.convert("true"));
        model.put(Project.class, tempP);
        tempP = new Project();
        tempP.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T16:22:14+02:00"));
        tempP.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T16:22:29+02:00"));
        tempP.setIdentifier((String) RedmineDataConverter.Text.convert("two"));
        tempP.setName((String) RedmineDataConverter.Text.convert("two"));
        tempP.setId((Integer) RedmineDataConverter.Integer.convert("2"));
        tempP.setProjectsCount((Integer) RedmineDataConverter.Integer.convert("0"));
        tempP.setStatus((Integer) RedmineDataConverter.Integer.convert("1"));
        tempP.setIsPublic((Boolean) RedmineDataConverter.Boolean.convert("false"));
        model.put(Project.class, tempP);

        Tracker tempT;
        tempT = new Tracker();
        tempT.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setTrackerId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setPosition((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setIsInChlog((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempT.setIsInRoadmap((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempT.setName((String) RedmineDataConverter.Text.convert("Anomalie"));
        model.put(Tracker.class, tempT);
        tempT = new Tracker();
        tempT.setId((Integer) RedmineDataConverter.Integer.convert("2"));
        tempT.setIsInChlog((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempT.setIsInRoadmap((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempT.setName((String) RedmineDataConverter.Text.convert("Evolution"));
        tempT.setPosition((Integer) RedmineDataConverter.Integer.convert("2"));
        tempT.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setTrackerId((Integer) RedmineDataConverter.Integer.convert("2"));
        model.put(Tracker.class, tempT);
        tempT = new Tracker();
        tempT.setId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempT.setIsInChlog((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempT.setIsInRoadmap((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempT.setName((String) RedmineDataConverter.Text.convert("Assistance"));
        tempT.setPosition((Integer) RedmineDataConverter.Integer.convert("3"));
        tempT.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempT.setTrackerId((Integer) RedmineDataConverter.Integer.convert("3"));
        model.put(Tracker.class, tempT);

        User tempU;
        tempU = new User();
        tempU.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T17:24:46+02:00"));
        tempU.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T01:23:59+02:00"));
        tempU.setLastLoginOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T01:23:59+02:00"));
        tempU.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempU.setMemberId((Integer) RedmineDataConverter.Integer.convert("5"));
        tempU.setRoleId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempU.setStatus((Integer) RedmineDataConverter.Integer.convert("1"));
        tempU.setAdmin((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempU.setMailNotification((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempU.setFirstname((String) RedmineDataConverter.Text.convert("Redmine"));
        tempU.setHashedPassword(
                (String) RedmineDataConverter.Text.convert("70c881d4a26984ddce795f6f71817c9cf4480e79"));
        tempU.setLanguage((String) RedmineDataConverter.Text.convert("fr"));
        tempU.setLastname((String) RedmineDataConverter.Text.convert("Admin"));
        tempU.setLogin((String) RedmineDataConverter.Text.convert("admin"));
        tempU.setMail((String) RedmineDataConverter.Text.convert("dummy@codelutin.com"));
        tempU.setIdentityUrl((String) RedmineDataConverter.Text.convert("yo"));
        model.put(User.class, tempU);
        tempU = new User();
        tempU.setAdmin((Boolean) RedmineDataConverter.Boolean.convert("true"));
        tempU.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T19:49:02+02:00"));
        tempU.setFirstname((String) RedmineDataConverter.Text.convert("tony"));
        tempU.setHashedPassword(
                (String) RedmineDataConverter.Text.convert("8aed1322e5450badb078e1fb60a817a1df25a2ca"));
        tempU.setId((Integer) RedmineDataConverter.Integer.convert("5"));
        tempU.setLanguage((String) RedmineDataConverter.Text.convert("fr"));
        tempU.setLastLoginOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T19:49:38+02:00"));
        tempU.setLastname((String) RedmineDataConverter.Text.convert("chemit2"));
        tempU.setLogin((String) RedmineDataConverter.Text.convert("tchemit2"));
        tempU.setMail((String) RedmineDataConverter.Text.convert("chemit@codelutin.com"));
        tempU.setMailNotification((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempU.setMemberId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempU.setRoleId((Integer) RedmineDataConverter.Integer.convert("3"));
        tempU.setStatus((Integer) RedmineDataConverter.Integer.convert("1"));
        tempU.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T19:49:38+02:00"));
        model.put(User.class, tempU);
        tempU = new User();
        tempU.setAdmin((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempU.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T16:24:11+02:00"));
        tempU.setFirstname((String) RedmineDataConverter.Text.convert("dev"));
        tempU.setHashedPassword(
                (String) RedmineDataConverter.Text.convert("70c881d4a26984ddce795f6f71817c9cf4480e79"));
        tempU.setId((Integer) RedmineDataConverter.Integer.convert("7"));
        tempU.setLanguage((String) RedmineDataConverter.Text.convert("fr"));
        tempU.setLastLoginOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T16:34:39+02:00"));
        tempU.setLastname((String) RedmineDataConverter.Text.convert("dev"));
        tempU.setLogin((String) RedmineDataConverter.Text.convert("dev"));
        tempU.setMail((String) RedmineDataConverter.Text.convert("dev3@ynot-home.info"));
        tempU.setMailNotification((Boolean) RedmineDataConverter.Boolean.convert("false"));
        tempU.setMemberId((Integer) RedmineDataConverter.Integer.convert("9"));
        tempU.setRoleId((Integer) RedmineDataConverter.Integer.convert("4"));
        tempU.setStatus((Integer) RedmineDataConverter.Integer.convert("1"));
        tempU.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T16:34:39+02:00"));
        model.put(User.class, tempU);

        Version tempV;
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T02:47:39+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("yo"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("9"));
        tempV.setName((String) RedmineDataConverter.Text.convert("yor"));
        tempV.setSharing((String) RedmineDataConverter.Text.convert("none"));
        tempV.setStatus((String) RedmineDataConverter.Text.convert("open"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T02:50:49+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:05:09+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("ysssoye"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("13"));
        tempV.setName((String) RedmineDataConverter.Text.convert("rrrrrrrrrouuuuuua"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:05:09+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:07:58+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("ysssoye"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("15"));
        tempV.setName((String) RedmineDataConverter.Text.convert("aaaauuuuuua"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:07:58+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T04:12:25+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("ysssoyeppppppppppppppppp"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("16"));
        tempV.setName((String) RedmineDataConverter.Text.convert("aaaau"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T04:13:20+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:05:40+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("ysssoye"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("14"));
        tempV.setName((String) RedmineDataConverter.Text.convert("aaaaaaaaaarrrrrrrrrouuuuuua"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:05:40+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T00:39:15+02:00"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("5"));
        tempV.setName((String) RedmineDataConverter.Text.convert("2"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-05T00:39:15+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T18:13:05+02:00"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setName((String) RedmineDataConverter.Text.convert("1.0.0"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-04T18:13:05+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:00:12+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("yoye"));
        tempV.setEffectiveDate((Date) RedmineDataConverter.Date.convert("2009-09-06"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("11"));
        tempV.setName((String) RedmineDataConverter.Text.convert("yaouuuuuua"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:00:12+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T02:50:59+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("yoye"));
        tempV.setEffectiveDate((Date) RedmineDataConverter.Date.convert("2009-09-06"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("10"));
        tempV.setName((String) RedmineDataConverter.Text.convert("ya"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T02:54:16+02:00"));
        model.put(Version.class, tempV);
        tempV = new Version();
        tempV.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:00:37+02:00"));
        tempV.setDescription((String) RedmineDataConverter.Text.convert("ysssoye"));
        tempV.setEffectiveDate((Date) RedmineDataConverter.Date.convert("2009-09-06"));
        tempV.setId((Integer) RedmineDataConverter.Integer.convert("12"));
        tempV.setName((String) RedmineDataConverter.Text.convert("ouuuuuua"));
        tempV.setProjectId((Integer) RedmineDataConverter.Integer.convert("1"));
        tempV.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-06T03:00:42+02:00"));
        model.put(Version.class, tempV);

        IssueStatus tempIS;
        tempIS = new IssueStatus();
        tempIS.setId(1);
        tempIS.setName("Nouveau");
        tempIS.setPosition(1);
        tempIS.setDefaultDoneRatio(10);
        tempIS.setIsClosed(false);
        tempIS.setIsDefault(true);
        model.put(IssueStatus.class, tempIS);
        tempIS = new IssueStatus();
        tempIS.setId(2);
        tempIS.setName("Assigné");
        tempIS.setPosition(2);
        tempIS.setIsClosed(false);
        tempIS.setIsDefault(false);
        model.put(IssueStatus.class, tempIS);
        tempIS = new IssueStatus();
        tempIS.setId(3);
        tempIS.setName("Résolu");
        tempIS.setPosition(3);
        tempIS.setIsClosed(false);
        tempIS.setIsDefault(false);
        model.put(IssueStatus.class, tempIS);
        tempIS = new IssueStatus();
        tempIS.setId(4);
        tempIS.setName("Commentaire");
        tempIS.setPosition(4);
        tempIS.setIsClosed(false);
        tempIS.setIsDefault(false);
        model.put(IssueStatus.class, tempIS);
        tempIS = new IssueStatus();
        tempIS.setId(5);
        tempIS.setName("Fermé");
        tempIS.setPosition(5);
        tempIS.setIsClosed(true);
        tempIS.setIsDefault(false);
        model.put(IssueStatus.class, tempIS);
        tempIS = new IssueStatus();
        tempIS.setId(6);
        tempIS.setPosition(6);
        tempIS.setName("Rejeté");
        tempIS.setIsClosed(true);
        tempIS.setIsDefault(false);
        model.put(IssueStatus.class, tempIS);

        IssuePriority tempIP;
        tempIP = new IssuePriority();
        tempIP.setId(3);
        tempIP.setParentId(1);
        tempIP.setProjectId(2);
        tempIP.setName("Bas");
        tempIP.setPosition(1);
        tempIP.setOpt("IPRI");
        tempIP.setIsDefault(false);
        tempIP.setActive(true);
        model.put(IssuePriority.class, tempIP);
        tempIP = new IssuePriority();
        tempIP.setId(4);
        tempIP.setName("Normal");
        tempIP.setPosition(2);
        tempIP.setOpt("IPRI");
        tempIP.setIsDefault(true);
        model.put(IssuePriority.class, tempIP);
        tempIP = new IssuePriority();
        tempIP.setId(5);
        tempIP.setName("Haut");
        tempIP.setPosition(3);
        tempIP.setOpt("IPRI");
        tempIP.setIsDefault(false);
        model.put(IssuePriority.class, tempIP);
        tempIP = new IssuePriority();
        tempIP.setId(6);
        tempIP.setName("Urgent");
        tempIP.setPosition(4);
        tempIP.setOpt("IPRI");
        tempIP.setIsDefault(false);
        model.put(IssuePriority.class, tempIP);
        tempIP = new IssuePriority();
        tempIP.setId(7);
        tempIP.setName("Immédiat");
        tempIP.setPosition(5);
        tempIP.setOpt("IPRI");
        tempIP.setIsDefault(false);
        model.put(IssuePriority.class, tempIP);

        IssueCategory tempIC;
        tempIC = new IssueCategory();
        tempIC.setId(1);
        tempIC.setName("categorie one");
        tempIC.setProjectId(1);
        model.put(IssueCategory.class, tempIC);
        tempIC = new IssueCategory();
        tempIC.setId(2);
        tempIC.setName("categorie two");
        tempIC.setProjectId(1);
        model.put(IssueCategory.class, tempIC);

        News tempN;
        tempN = new News();
        tempN.setId(85);
        tempN.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-17T21:50:26+02:00"));
        tempN.setProjectId(1);
        tempN.setAuthorId(4);
        tempN.setCommentsCount(0);
        tempN.setDescription("description");
        tempN.setSummary("summary");
        tempN.setTitle("title");
        model.put(News.class, tempN);
        tempN = new News();
        tempN.setId(86);
        tempN.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-09-17T21:55:26+02:00"));
        tempN.setProjectId(1);
        tempN.setAuthorId(4);
        tempN.setCommentsCount(0);
        tempN.setDescription("description2");
        tempN.setSummary("summary2");
        tempN.setTitle("title2");
        model.put(News.class, tempN);

        TimeEntry tempE;

        tempE = new TimeEntry();
        tempE.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-12-31T23:02:02+01:00"));
        tempE.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-12-31T23:02:02+01:00"));
        tempE.setSpentOn((Date) RedmineDataConverter.Date.convert("2009-12-31"));
        tempE.setId(1);
        tempE.setProjectId(1);
        tempE.setUserId(4);
        tempE.setIssueId(6);
        tempE.setActivityId(8);

        tempE.setHours(1);
        tempE.setTmonth(12);
        tempE.setTyear(2009);
        tempE.setTweek(53);
        tempE.setComments("Test");
        model.put(TimeEntry.class, tempE);

        tempE = new TimeEntry();
        tempE.setCreatedOn((Date) RedmineDataConverter.Datetime.convert("2009-12-31T23:10:01+01:00"));
        tempE.setUpdatedOn((Date) RedmineDataConverter.Datetime.convert("2009-12-31T23:10:01+01:00"));
        tempE.setSpentOn((Date) RedmineDataConverter.Date.convert("2009-12-31"));
        tempE.setId(2);
        tempE.setProjectId(1);
        tempE.setUserId(4);
        tempE.setIssueId(6);
        tempE.setActivityId(9);

        tempE.setHours(2);
        tempE.setTmonth(12);
        tempE.setTyear(2009);
        tempE.setTweek(53);
        tempE.setComments("deuxième temps");
        model.put(TimeEntry.class, tempE);
    }

}
