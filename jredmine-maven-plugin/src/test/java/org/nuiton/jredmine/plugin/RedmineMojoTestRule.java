package org.nuiton.jredmine.plugin;

/*-
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.model.IssueManagement;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.RedmineFixtures;
import org.nuiton.jredmine.client.RedmineClientAuthConfiguration;
import org.nuiton.jredmine.service.RedmineConfigurationUtil;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.plugin.MojoTestRule;

import java.io.File;
import java.io.IOException;

/**
 * Created on 12/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class RedmineMojoTestRule<P extends AbstractRedmineMojo> extends MojoTestRule<P> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RedmineMojoTestRule.class);

    protected boolean canContinue;

    private final RedmineFixtures fixtures;

    public RedmineMojoTestRule(Class<?> testClass, String goalName, RedmineFixtures fixtures) {
        super(testClass, goalName);
        this.fixtures = fixtures;
    }

    protected abstract RedmineServiceConfiguration getConfiguration() throws IOException;

    protected void beforeMojoInit(P mojo, File pomFile) throws IOException {

        // add a issue management
        IssueManagement i = new IssueManagement();
        i.setSystem(AbstractRedmineMojo.REDMINE_SYSTEM);
        i.setUrl(mojo.getUrl().toString());
        mojo.getProject().setIssueManagement(i);

        RedmineClientAuthConfiguration authConfiguration = getConfiguration().getAuthConfiguration();
        mojo.setUsername(authConfiguration.getUsername());
        mojo.setPassword(authConfiguration.getPassword());
        mojo.setApiKey(authConfiguration.getApiKey());

        if (mojo instanceof RedmineProjectAware) {

            // fill projectId if none is given from pom.xml
            RedmineProjectAware redmineMojoWithProject = (RedmineProjectAware) mojo;

            String projectId = redmineMojoWithProject.getProjectId();
            if (StringUtils.isBlank(projectId)) {

                redmineMojoWithProject.setProjectId(fixtures.projectName());
            }
        }

        if (mojo instanceof RedmineVersionAware) {

            // fill versionId if none is given from pom.xml
            RedmineVersionAware redmineMojoWithProjectAndVersion = (RedmineVersionAware) mojo;

            String versionId = redmineMojoWithProjectAndVersion.getVersionId();
            if (StringUtils.isBlank(versionId)) {

                redmineMojoWithProjectAndVersion.setVersionId(fixtures.versionName());
            }
        }

        if (mojo instanceof DryRunAware) {

            // never authorize to modify any content...
            ((DryRunAware) mojo).setDryRun(true);
        }
    }

    @Override
    protected void setUpMojo(P mojo, File pomFile) throws Exception {

        super.setUpMojo(mojo, pomFile);

        RedmineServiceConfiguration configuration = getConfiguration();

        // copy redmine test server configuration
        RedmineConfigurationUtil.copyConfiguration(configuration, mojo);

        beforeMojoInit(mojo, pomFile);

        try {
            mojo.init();


            canContinue = mojo.checkSkip();
            if (!canContinue) {
                if (log.isInfoEnabled()) {
                    log.info("The goal was marked as to be skip, the test is done.");
                }
                return;
            }
        } catch (Exception e) {
            log.error(e);
            canContinue = false;
        }
        if (canContinue) {
            if (mojo.isVerbose()) {
                log.info("setup done for " + mojo.getProject().getFile().getName());
            }
        } else {
            log.warn("setup was not successfull, will skip this test [" + getClass() + "]");
        }
    }

    @Override
    public boolean initMojo(P mojo) {
        return super.initMojo(mojo);
    }

    @Override
    protected void tearDownMojo(P mojo) {
        super.tearDownMojo(mojo);

        if (mojo != null) {
            mojo.closeService();
        }
    }
}
