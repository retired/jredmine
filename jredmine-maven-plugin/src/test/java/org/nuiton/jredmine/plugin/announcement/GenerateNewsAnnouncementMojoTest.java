/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.announcement;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.jredmine.plugin.AbstractLogguedRedmineMojoTest;
import org.nuiton.jredmine.plugin.RedmineMojoTestRule;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class GenerateNewsAnnouncementMojoTest extends AbstractLogguedRedmineMojoTest<GenerateNewsAnnouncementMojo> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(GenerateNewsAnnouncementMojoTest.class);

    @Rule
    public RedmineMojoTestRule<GenerateNewsAnnouncementMojo> rule = new RedmineMojoTestRule<GenerateNewsAnnouncementMojo>(GenerateNewsAnnouncementMojoTest.class, "generate-news-announcement", classRule.getFixtures()) {

        @Override
        protected RedmineServiceConfiguration getConfiguration() throws IOException {
            return getFixtures().newLogguedConfiguration();
        }

    };

    @Test
    public void generateNewsAnnouncement() throws Exception {

        File outputFile = rule.getMojo().getOutputFile();
        Assert.assertNotNull(outputFile);

        mojoDoAction(rule.getMojo());
        Assert.assertTrue(outputFile.exists());
        if (rule.getMojo().isVerbose()) {

            String content = FileUtils.readFileToString(outputFile);
            if (log.isInfoEnabled()) {
                log.info("Announcement content:\n" + content);
            }
        }
    }

    @Test
    public void skipGenerateNewsAnnouncement() throws Exception {

        mojoDoAction(rule.getMojo());
    }
}
