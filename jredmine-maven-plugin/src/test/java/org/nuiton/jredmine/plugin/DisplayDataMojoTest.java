/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.junit.Rule;
import org.junit.Test;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class DisplayDataMojoTest extends AbstractAnonymousRedmineMojoTest<DisplayDataMojo> {

    @Rule
    public RedmineMojoTestRule<DisplayDataMojo> rule = new RedmineMojoTestRule<DisplayDataMojo>(DisplayDataMojoTest.class, "display-data", classRule.getFixtures()) {

        @Override
        protected RedmineServiceConfiguration getConfiguration() throws IOException {
            return getFixtures().newAnonymousConfiguration();
        }

    };

    @Test
    public void displayProjects() throws Exception {
        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayVersions() throws Exception {
        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayProjectTrackers() throws Exception {
        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayProjectUsers() throws Exception {

        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayProjectIssueCategories() throws Exception {

        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayIssueStatuses() throws Exception {

        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayIssuePriorities() throws Exception {

        mojoDoAction(rule.getMojo());
    }

    @Test
    public void displayAll() throws Exception {

        mojoDoAction(rule.getMojo());
    }
}
