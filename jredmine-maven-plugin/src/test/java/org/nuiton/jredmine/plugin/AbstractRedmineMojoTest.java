/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.nuiton.jredmine.RedmineFixtures;

/**
 * @param <P> type of mojo to test
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public abstract class AbstractRedmineMojoTest<P extends AbstractRedmineMojo> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractRedmineMojoTest.class);

    protected static boolean init;

    protected boolean canContinue;

//    protected abstract RedmineServiceConfiguration getConfiguration() throws IOException;
//
    protected abstract RedmineFixtures getFixtures();

//    protected void beforeMojoInit(P mojo, File pomFile) throws Exception {
//
//        // add a issue management
//        IssueManagement i = new IssueManagement();
//        i.setSystem(AbstractRedmineMojo.REDMINE_SYSTEM);
//        i.setUrl(mojo.getUrl().toString());
//        mojo.getProject().setIssueManagement(i);
//
//        RedmineClientAuthConfiguration authConfiguration =
//                getConfiguration().getAuthConfiguration();
//        mojo.setUsername(authConfiguration.getUsername());
//        mojo.setPassword(authConfiguration.getPassword());
//        mojo.setApiKey(authConfiguration.getApiKey());
//
//        if (mojo instanceof RedmineProjectAware) {
//
//            // fill projectId if none is given from pom.xml
//            RedmineProjectAware redmineMojoWithProject = (RedmineProjectAware) mojo;
//
//            String projectId = redmineMojoWithProject.getProjectId();
//            if (StringUtils.isBlank(projectId)) {
//
//                redmineMojoWithProject.setProjectId(getFixtures().projectName());
//            }
//        }
//
//        if (mojo instanceof RedmineVersionAware) {
//
//            // fill versionId if none is given from pom.xml
//            RedmineVersionAware redmineMojoWithProjectAndVersion = (RedmineVersionAware) mojo;
//
//            String versionId = redmineMojoWithProjectAndVersion.getVersionId();
//            if (StringUtils.isBlank(versionId)) {
//
//                redmineMojoWithProjectAndVersion.setVersionId(getFixtures().versionName());
//            }
//        }
//
//        if (mojo instanceof DryRunAware) {
//
//            // never authorize to modify any content...
//            ((DryRunAware) mojo).setDryRun(true);
//        }
//    }

    protected void mojoDoAction(P mojo) throws Exception {

        if (canContinue) {

            Assert.assertNotNull(mojo);
            try {
                mojo.doAction();
            } finally {
                mojo.afterExecute();
            }
        }
    }
}
