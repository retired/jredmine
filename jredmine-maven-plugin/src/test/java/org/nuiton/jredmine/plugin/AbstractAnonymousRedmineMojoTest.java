/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.junit.ClassRule;
import org.nuiton.jredmine.RedmineAnonymousFixtureClassRule;
import org.nuiton.jredmine.RedmineFixtures;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;

import java.io.IOException;

/**
 * @param <P> type of mojo to test
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public abstract class AbstractAnonymousRedmineMojoTest<P extends AbstractRedmineMojo> extends AbstractRedmineMojoTest<P> {

    @ClassRule
    public static final RedmineAnonymousFixtureClassRule classRule =
            new RedmineAnonymousFixtureClassRule();

    @Override
    protected RedmineFixtures getFixtures() {
        return classRule.getFixtures();
    }

    protected final RedmineServiceConfiguration getConfiguration() throws IOException {
        // by default use a anonymous configuration
        RedmineServiceConfiguration conf = getFixtures().newAnonymousConfiguration();
        return conf;
    }

}
