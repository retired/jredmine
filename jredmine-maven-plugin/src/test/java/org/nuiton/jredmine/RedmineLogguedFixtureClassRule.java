package org.nuiton.jredmine;
/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.jredmine.service.RedmineConfigurationUtil;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;

import java.io.IOException;

/**
 * A class rule that provider the {@link RedmineFixtures} and check that the
 * valid loggued configuration is found.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RedmineLogguedFixtureClassRule implements TestRule {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RedmineLogguedFixtureClassRule.class);

    RedmineFixtures fixtures = new RedmineFixtures();

    RedmineServiceConfiguration conf;

    @Override
    public Statement apply(Statement base, Description description) {

        Class<?> testClass = description.getTestClass();

        try {
            conf = fixtures.obtainRedmineConfiguration();

            if (conf == null) {

                // could not find any configuration
                if (log.isWarnEnabled()) {
                    log.warn("could not connect to server " +
                             fixtures.newAnonymousConfiguration() + ", will skip test " +
                             testClass.getName());
                }
                Assume.assumeTrue(false);
            } else {

                // configuration must NOT be anonymous

                if (!RedmineConfigurationUtil.isLoggued(conf)) {

                    if (log.isWarnEnabled()) {
                        log.warn("A authenticated configuration was required, will skip test " +
                                 testClass.getName());
                    }
                    Assume.assumeTrue(false);
                }
            }

        } catch (IOException e) {
            throw new IllegalStateException("Could not check jredmine configuration in test " + testClass.getName(), e);

        }
        return base;
    }

    public RedmineFixtures getFixtures() {
        return fixtures;
    }

    public RedmineServiceConfiguration getConf() {
        return conf;
    }
}
