package org.nuiton.jredmine;
/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jredmine.service.DefaultRedmineService;
import org.nuiton.jredmine.service.RedmineAnonymousService;
import org.nuiton.jredmine.service.RedmineConfigurationUtil;
import org.nuiton.jredmine.service.RedmineService;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.jredmine.service.RedmineServiceException;

import java.io.IOException;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RedmineFixtures {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RedmineFixtures.class);

    private RedmineServiceConfiguration anonymousConfiguration;

    private RedmineServiceConfiguration logguedConfiguration;

    public String projectName() {
        return "jredmine";
    }

    public String versionName() {
        return "1.3";
    }

    public String versionId() {
        return "295";
    }

    public String issueId() {
        return "1925";
    }

    public RedmineServiceConfiguration newAnonymousConfiguration()
            throws IOException {
        RedmineServiceConfiguration conf = RedmineConfigurationUtil.cloneConfiguration(getAnonymousConfiguration());
        return conf;
    }

    public RedmineServiceConfiguration newLogguedConfiguration()
            throws IOException {
        RedmineServiceConfiguration conf = RedmineConfigurationUtil.cloneConfiguration(getLogguedConfiguration());
        return conf;
    }

    public RedmineAnonymousService newRedmineAnonymousService(RedmineServiceConfiguration configuration)
            throws IOException, RedmineServiceException {
        RedmineAnonymousService service = new DefaultRedmineService();
        service.init(configuration);
        return service;
    }

    public RedmineService newRedmineService(RedmineServiceConfiguration configuration)
            throws IOException, RedmineServiceException {
        RedmineService service = new DefaultRedmineService();
        service.init(configuration);
        return service;
    }

    protected RedmineServiceConfiguration getAnonymousConfiguration()
            throws IOException {
        if (anonymousConfiguration == null) {
            anonymousConfiguration = RedmineConfigurationUtil.newAnonymousConfiguration(
                    "jredmine-test.properties",
                    "/test-config.properties"
            );

        }
        return anonymousConfiguration;
    }

    protected RedmineServiceConfiguration getLogguedConfiguration()
            throws IOException {
        if (logguedConfiguration == null) {

            RedmineServiceConfiguration anoConf = getAnonymousConfiguration();
            logguedConfiguration = RedmineConfigurationUtil.newLogguedConfiguration(anoConf);
        }
        return logguedConfiguration;
    }

    public RedmineServiceConfiguration obtainRedmineConfiguration() throws IOException {

        return RedmineConfigurationUtil.obtainRedmineConfiguration(
                newAnonymousConfiguration());
    }

}
