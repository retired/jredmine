/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.report;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.site.decoration.Body;
import org.apache.maven.doxia.site.decoration.DecorationModel;
import org.apache.maven.doxia.site.decoration.Skin;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.doxia.siterenderer.RendererException;
import org.apache.maven.doxia.siterenderer.RenderingContext;
import org.apache.maven.doxia.siterenderer.SiteRenderingContext;
import org.apache.maven.doxia.siterenderer.sink.SiteRendererSink;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.reporting.MavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.apache.maven.repository.RepositorySystem;
import org.codehaus.plexus.i18n.I18N;
import org.nuiton.jredmine.plugin.AbstractRedmineMojoWithProject;
import org.nuiton.jredmine.plugin.RedmineVersionAware;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Abstract redmine report mojo.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public abstract class AbstractRedmineReport extends AbstractRedmineMojoWithProject implements MavenReport, RedmineVersionAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Redmine version name.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionId", defaultValue = "${project.version}")
    protected String versionId;

    /**
     * Template strings per system that is used to discover the URL to use to display an issue report. Each key in this
     * map denotes the (case-sensitive) identifier of the issue tracking system and its value gives the URL template.
     * <p>
     * There are 2 template tokens you can use. <code>%URL%</code>: this is computed by getting the
     * <code>&lt;issueManagement&gt;/&lt;url&gt;</code> value from the POM, and removing the last '/'
     * and everything that comes after it. <code>%ISSUE%</code>: this is the issue number.
     * </p>
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.issueLinkTemplate", defaultValue = "%URL%/issues/%ISSUE%")
    protected String issueLinkTemplate = "%URL%/issues/%ISSUE%";

    /**
     * Template strings per system that is used to discover the URL to use to display an issue report. Each key in this
     * map denotes the (case-sensitive) identifier of the issue tracking system and its value gives the URL template.
     * <p>
     * There are 2 template tokens you can use. <code>%URL%</code>: this is computed by getting the
     * <code>&lt;issueManagement&gt;/&lt;url&gt;</code> value from the POM, and removing the last '/'
     * and everything that comes after it. <code>%VERSION%</code>: this is the issue number.
     * </p>
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionLinkTemplate", defaultValue = "%URL%/versions/%VERSION%")
    protected String versionLinkTemplate = "%URL%/versions/%VERSION%";

    /**
     * Local Repository.
     *
     * @since 1.0.0
     */
    @Parameter(property = "localRepository", required = true, readonly = true)
    protected ArtifactRepository localRepository;

    /**
     * Report output directory. Note that this parameter is only relevant if the goal is run from the command line or
     * from the default build lifecycle. If the goal is run indirectly as part of a site generation, the output
     * directory configured in the Maven Site Plugin is used instead.
     *
     * @since 1.0.0
     */
    @Parameter(defaultValue = "${project.reporting.outputDirectory}")
    protected File outputDirectory;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo components
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Repository System.
     *
     * @since 1.0.0
     */
    @Component
    protected RepositorySystem repositorySystem;

    /**
     * Internationalization.
     *
     * @since 1.0.0
     */
    @Component
    protected I18N i18n;

    /**
     * Doxia Site Renderer.
     *
     * @since 1.0.0
     */
    @Component
    protected Renderer siteRenderer;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    private Sink sink;

    private File reportOutputDirectory;

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractRedmineReport
    ///////////////////////////////////////////////////////////////////////////

    protected abstract void executeReport(Locale locale) throws MavenReportException;

    /** @return {@code true} if report should be skip */
    protected abstract boolean skipReport();

    protected AbstractRedmineReport() {
        super(false);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineVersionAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public final String getVersionId() {
        return versionId;
    }

    @Override
    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void doAction() throws Exception {

        try {
            DecorationModel model = new DecorationModel();
            model.setBody(new Body());
            Map<String, String> attributes = new HashMap<String, String>();
            attributes.put("outputEncoding", encoding);
            Locale currentLocale = Locale.getDefault();
            SiteRenderingContext siteContext = siteRenderer.createContextForSkin(getSkinArtifactFile(), attributes,
                                                                                 model, getName(currentLocale), currentLocale);

            createDirectoryIfNecessary(outputDirectory);

            String filename = getOutputName() + ".html";
            File file = new File(outputDirectory, filename);

            RenderingContext context = new RenderingContext(outputDirectory, getOutputName() + ".html");

            //SiteRendererSink sink = SinkFactory.createSink(outputDirectory, filename);

            SiteRendererSink sink = new SiteRendererSink(context);


            generate(sink, currentLocale);

            Writer writer = new FileWriter(file);

            try {
                siteRenderer.generateDocument(writer, sink, siteContext);
            } finally {

                writer.close();
            }

            siteRenderer.copyResources(siteContext, new File(project.getBasedir(), "src/site/resources"), outputDirectory);

        } catch (RendererException e) {
            throw new MojoExecutionException(
                    "An error has occurred in " + getName(Locale.ENGLISH) + " report generation.", e);
        } catch (IOException e) {
            throw new MojoExecutionException(
                    "An error has occurred in " + getName(Locale.ENGLISH) + " report generation.", e);
        } catch (MavenReportException e) {
            throw new MojoExecutionException(
                    "An error has occurred in " + getName(Locale.ENGLISH) + " report generation.", e);
        } finally {

            closeService();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// MavenReport
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean canGenerateReport() {
        boolean init;
        if (getLog().isDebugEnabled()) {
            getLog().debug("check can use report");
        }
        if (skipReport()) {
            getLog().info("User ask to skip report \"" + getName(Locale.ENGLISH) + "\".");
            return false;
        }
        if (session.getSettings().isOffline()) {
            // skip when offline
            getLog().info("Skipped \"" + getName(Locale.ENGLISH) + "\" report in offline mode.");
            return false;
        }
        try {
            init();
            init = true;
        } catch (Exception ex) {
            if (isVerbose()) {
                getLog().error("could not init report goal for reason " + ex.getMessage(), ex);
            } else {
                getLog().error("could not init report goal for reason " + ex.getMessage());
            }
            init = false;
        }
        if (!init) {
            getLog().info("Skipped \"" + getName(Locale.ENGLISH) + "\" report, goal could be initialized.");
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug("check can use report : " + init);
        }
        return init;
    }

    @Override
    public void generate(org.codehaus.doxia.sink.Sink sink, Locale locale)
            throws MavenReportException {
        if (sink == null) {
            throw new MavenReportException("You must specify a sink.");
        }

        this.sink = sink;

        executeReport(locale);
    }

    @Override
    public String getName(Locale locale) {
        return getBundle(locale).getString("report.name");
    }

    @Override
    public String getDescription(Locale locale) {
        return getBundle(locale).getString("report.description");
    }

    @Override
    public String getCategoryName() {
        return CATEGORY_PROJECT_REPORTS;
    }

    @Override
    public File getReportOutputDirectory() {
        if (reportOutputDirectory == null) {
            reportOutputDirectory = new File(outputDirectory.getAbsolutePath());
        }
        return reportOutputDirectory;
    }

    @Override
    public void setReportOutputDirectory(File reportOutputDirectory) {
        this.reportOutputDirectory = reportOutputDirectory;
    }

    @Override
    public boolean isExternalReport() {
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected Sink getSink() {
        return sink;
    }

    protected ResourceBundle getBundle(Locale locale) {
        return ResourceBundle.getBundle("redmine-report", locale, getClass().getClassLoader());
    }

    protected File getSkinArtifactFile() throws MojoExecutionException {

        Skin skin = Skin.getDefaultSkin();

        String version = skin.getVersion();
        Artifact artifact;
        if (version == null) {
            version = Artifact.RELEASE_VERSION;
        }

        artifact = repositorySystem.createArtifact(skin.getGroupId(), skin.getArtifactId(), version, "jar");
        ArtifactResolutionRequest request = new ArtifactResolutionRequest();
        request.setArtifact(artifact);
        request.setRemoteRepositories(project.getRemoteArtifactRepositories());
        request.setLocalRepository(localRepository);
        repositorySystem.resolve(request);

        return artifact.getFile();
    }
}
