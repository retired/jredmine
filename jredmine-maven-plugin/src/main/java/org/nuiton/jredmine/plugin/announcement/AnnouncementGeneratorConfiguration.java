/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.announcement;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.nuiton.jredmine.model.Attachment;

import java.io.File;
import java.net.URL;
import java.util.Map;

/**
 * Created: 10 janv. 2010
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @version $Revision$
 * @since 1.2.1
 */
public interface AnnouncementGeneratorConfiguration {

    /** @return the redmine url */
    URL getUrl();

    /** @return the redmine attachment url template */
    String getAttachmentLinkTemplate();

    /** @return logger */
    Log getLog();

    String getArtifactId();

    Map<File, String> getArtifactUrls();

    Map<Attachment, String> getAttachmentUrls();

    String getBasedir();

    String getDeploymentUrl();

    String getDevelopmentTeam();

    String getFinalName();

    String getGroupId();

    String getIntroduction();

    String getPackaging();

    String getProjectUrl();

    String getUrlDownload();

    Map<String, Object> getAnnounceParameters();

    String getVersionId();

    MavenProject getProject();
}
