/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.jredmine.client.RedmineRequest;
import org.nuiton.jredmine.client.RedmineRequestHelper;
import org.nuiton.jredmine.model.I18nAble;
import org.nuiton.jredmine.model.IdAble;
import org.nuiton.jredmine.model.IdAbles;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.News;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.service.AbstractRedmineService;
import org.nuiton.plugin.PluginHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Display in console some data from redmine's server.
 *
 * This goals requires no authentication to the server.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "display-data", requiresOnline = true, requiresProject = true)
public class DisplayDataMojo extends AbstractRedmineMojo implements RedmineProjectAware, RedmineVersionAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Redmine project name.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.projectId", defaultValue = "${project.artifactId}", required = true)
    protected String projectId;

    /**
     * Redmine version name.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionId", defaultValue = "${project.version}")
    protected String versionId;

    /**
     * The data types to display : {@code issueSatus}, {@code issuePriority},
     * {@code issueCategory}, {@code tracker} and {@code user}.
     *
     * {@code issueStatus} and {@code issuePriority} are common for all
     * projects, so for those datas, no extrat configuration is required.
     *
     * {@code isseCategoriy}, {@code tracker} and {@code user} are project
     * specific, so you have to fill also the {@code projectId} parameter to
     * obtain data for those types.
     *
     * Note : you can specifiy several types separated by comma
     *
     * @since 1.0.0
     */
    @Parameter(property = "types", required = true)
    protected String types;

    /**
     * A flag to sort of not retrived data by id.
     *
     * @since 1.0.0
     */
    @Parameter(property = "sortById", defaultValue = "true", required = true)
    protected boolean sortById;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** list of actions to perform */
    protected List<String> actions;

    private final Map<String, Class<? extends IdAble>> namesToType;

    private final Map<String, DataScope> namestoScope;

    private enum DataScope {NONE, PROJECT, VERSION}

    public DisplayDataMojo() {

        namestoScope = Maps.newHashMap();

        namestoScope.put("project", DataScope.NONE);
        namestoScope.put("version", DataScope.PROJECT);
        namestoScope.put("issue", DataScope.VERSION);
        namestoScope.put("news", DataScope.PROJECT);
        namestoScope.put("issuestatus", DataScope.NONE);
        namestoScope.put("issuepriority", DataScope.NONE);
        namestoScope.put("issuecategory", DataScope.PROJECT);
        namestoScope.put("tracker", DataScope.PROJECT);
        namestoScope.put("user", DataScope.PROJECT);


        namesToType = Maps.newHashMap();
        namesToType.put("project", Project.class);
        namesToType.put("version", Version.class);
        namesToType.put("issue", Issue.class);
        namesToType.put("news", News.class);
        namesToType.put("issuestatus", IssueStatus.class);
        namesToType.put("issuepriority", IssuePriority.class);
        namesToType.put("issuecategory", IssueCategory.class);
        namesToType.put("tracker", Tracker.class);
        namesToType.put("user", User.class);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineProjectAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getProjectId() {
        return projectId;
    }

    @Override
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineVersionAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getVersionId() {
        return versionId;
    }

    @Override
    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (versionId != null) {
            // always remove snapshot (TODO-TC20100124 perharps should it be a flag ?)
            versionId = PluginHelper.removeSnapshotSuffix(versionId);
        }

        super.init();

        actions = getDownloadActions();
    }

    @Override
    protected boolean checkSkip() {
        boolean canContinue;
        if (actions == null || actions.isEmpty()) {
            // no data to treate
            getLog().warn("No data types detected, you must fill the " +
                          "required parameter types, will skip goal");
            canContinue = false;
        } else {

            canContinue = super.checkSkip();
        }
        return canContinue;
    }

    @Override
    protected void doAction() throws Exception {

        StringBuilder buffer = new StringBuilder("\n");
        for (String dataTypeName : actions) {

            DataScope scope = namestoScope.get(dataTypeName);

            IdAble[] datas;

            if (DataScope.VERSION.equals(scope)) {

                datas = getDatasWithProjectAndVersion(dataTypeName);

            } else if (DataScope.PROJECT.equals(scope)) {

                datas = getDatasWithProject(dataTypeName);
            } else {

                datas = getDatas(dataTypeName);
            }
            List<IdAble> list = Arrays.asList(datas);
            if (sortById) {
                Collections.sort(list, IdAbles.ID_ABLE_COMPARATOR);
            }
            buffer.append("\nValues of '").append(dataTypeName).append('\'');
            if (DataScope.VERSION.equals(scope) ||
                DataScope.PROJECT.equals(scope)) {
                buffer.append(" for project '").append(projectId).append("'");
            }
            if (DataScope.VERSION.equals(scope)) {
                buffer.append(" and version '").append(versionId).append("'");
            }
            buffer.append('\n');
            for (IdAble data : list) {
                buffer.append("  - ");
                buffer.append(data.getId());
                buffer.append(" = ");
                buffer.append(((I18nAble) data).getName());
                buffer.append('\n');
            }
        }
        System.out.println(buffer.toString());
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected List<String> getDownloadActions() throws MojoFailureException {

        List<String> results = Lists.newArrayList();

        List<String> universeList = Lists.newArrayList(namestoScope.keySet());

        for (String dataType : types.split(",")) {

            dataType = dataType.toLowerCase(Locale.ENGLISH);

            if (!universeList.contains(dataType)) {
                // can not accept these value
                throw new MojoFailureException(
                        "The data type '" + dataType +
                        "' can not be used, Accepted types are : " +
                        universeList
                );
            }

            DataScope scope = namestoScope.get(dataType);

            if (DataScope.VERSION.equals(scope) ||
                DataScope.PROJECT.equals(scope)) {

                // check projectId used
                if (projectId == null || projectId.isEmpty()) {
                    throw new MojoFailureException(
                            "The data type '" + dataType +
                            "', depends on a project, but the 'projectId' was " +
                            "not filled."
                    );
                }
            }
            if (DataScope.VERSION.equals(scope)) {

                // check versionId use
                if (versionId == null || versionId.isEmpty()) {
                    throw new MojoFailureException(
                            "The data type '" + dataType +
                            "', depends on a version, but the 'versionId' was " +
                            "not filled."
                    );
                }
            }
            results.add(dataType);
        }
        return results;
    }

    protected IdAble[] getDatas(String type) throws IOException {

        Class<? extends IdAble> dataType = namesToType.get(type);
        RedmineRequest<? extends IdAble> request = RedmineRequest.get(dataType, "jredmine");
        if ("project".equals(type)) {
            request.path("get_projects.xml");
        } else if ("issuestatus".equals(type)) {
            request.path("get_issue_statuses.xml");
        } else if ("issuepriority".equals(type)) {
            request.path("get_issue_priorities.xml");
        }
        IdAble[] result = ((AbstractRedmineService) service).getClient().executeRequests(request);
        return result;
    }

    protected IdAble[] getDatasWithProject(String type) throws IOException {
        Class<? extends IdAble> dataType = namesToType.get(type);
        String action = null;
        if ("version".equals(type)) {
            action = "get_project_versions.xml";
        } else if ("news".equals(type)) {
            action = "get_project_news.xml";
        } else if ("issuecategory".equals(type)) {
            action = "get_issue_categories.xml";
        } else if ("tracker".equals(type)) {
            action = "get_project_trackers.xml";
        } else if ("user".equals(type)) {
            action = "get_project_users.xml";
        }
        RedmineRequest<? extends IdAble> request = RedmineRequestHelper.actionWithProject(action, dataType, projectId);

        IdAble[] result = ((AbstractRedmineService) service).getClient().executeRequests(request);
        return result;
    }

    protected IdAble[] getDatasWithProjectAndVersion(String type) throws IOException {
        Class<? extends IdAble> dataType = namesToType.get(type);
        String action = null;
        if ("issue".equals(type)) {
            action = "get_version_issues.xml";
        }
        RedmineRequest<? extends IdAble> request = RedmineRequestHelper.actionWithProjectAndVersion(action, dataType, projectId, versionId);
        IdAble[] result = ((AbstractRedmineService) service).getClient().executeRequests(request);
        return result;
    }
}
