package org.nuiton.jredmine.plugin;
/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.Project;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.service.RedmineServiceException;

/**
 * Abstract redmine mojo whihc need a redmine project context.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class AbstractRedmineMojoWithProject extends AbstractRedmineMojo implements RedmineProjectAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Redmine project name.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.projectId", defaultValue = "${project.artifactId}", required = true)
    protected String projectId;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** the project loaded in init. */
    protected Project releaseProject;

    /** the user loaded in init if {@link #requireUser} flag is on */
    protected User releaseUser;

    /** cache of users of a given project loaded in int if {@link #requireUser} flag is on */
    protected User[] users;

    /** flag to load in init a required user using the user loggued to redmine server. */
    private final boolean requireUser;

    protected AbstractRedmineMojoWithProject(boolean requireUser) {
        this.requireUser = requireUser;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineProjectAware
    ///////////////////////////////////////////////////////////////////////////

    public final String getProjectId() {
        return projectId;
    }

    public final void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        super.init();

        if (initOk) {

            // check project exists

            boolean r = initReleaseProject();
            if (!r) {
                failIfSafe("the project '" + projectId +
                           "' could not be retrieve from redmine server.");

                initOk = false;
            }
        }

        if (initOk) {

            // check user exists

            if (requireUser) {
                boolean r = initReleaseUser();
                if (!r) {
                    failIfSafe("the user '" + username +
                               "' could not be retrieve from redmine server.");
                    initOk = false;
                }
            }
        }
    }

    @Override
    protected boolean checkSkip() {

        boolean canContinue = super.checkSkip();

        if (canContinue) {

            if (releaseProject == null) {

                getLog().error("the project '" + projectId + "' could not be retrieve from redmine server, goal is skip");
                canContinue = false;
            }
            if (canContinue && requireUser && releaseUser == null) {

                getLog().error("the user '" + username + "' could not be retrieve from redmine server, goal is skip");
                canContinue = false;
            }
        }
        return canContinue;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected boolean initReleaseProject() throws MojoExecutionException {

        if (StringUtils.isBlank(projectId)) {
            throw new MojoExecutionException("required a projectId parameter");
        }
        try {
            Project p = service.getProject(projectId);

            if (p == null) {

                return false;
            }

            releaseProject = p;
            return true;
        } catch (RedmineServiceException e) {
            getLog().warn("could not retrieve project '" + projectId +
                          "', for reason " + e.getMessage(), e);
            return false;
        }
    }

    protected boolean initReleaseUser() throws MojoExecutionException {

        if (StringUtils.isBlank(username) && StringUtils.isBlank(apiKey)) {
            throw new MojoExecutionException("required a username or apikey parameter");
        }

        try {
            users = service.getProjectMembers(projectId);

            User user = null;

            if (getAuthConfiguration().isUseApiKey()) {

                // get user by his api key
                user = service.getCurrentUser();
            }

            if (user == null && StringUtils.isNotBlank(username)) {

                // try to get user by his login
                user = User.byLogin(username, users);
            }

            if (user == null) {
                return false;
            }


            releaseUser = user;
            return true;
        } catch (RedmineServiceException e) {
            getLog().warn("could not retrieve user", e);
            return false;
        }
    }

}
