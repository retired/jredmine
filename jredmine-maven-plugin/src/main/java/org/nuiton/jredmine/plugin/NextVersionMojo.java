/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.Version;
import org.nuiton.plugin.PluginHelper;

import java.text.ParseException;
import java.util.Date;

/**
 * Prepare the next version to be used.
 *
 * If a previous version is given, then move all unclosed issues from the
 * previous version to the new one.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "next-version", requiresOnline = true, requiresProject = true)
public class NextVersionMojo extends AbstractRedmineMojoWithProjectAndVersion implements DryRunAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The news description to update to redmine server.
     *
     * Note : if not set, no update of the description will be made.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionDescription")
    protected String versionDescription;

    /**
     * The effective date to set on the version.
     *
     * The format of the date is {@code yyyy-mm-dd}.
     *
     * If not Set - will use current day date.
     * flag is on.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.effectiveDate")
    protected String effectiveDate;

    /**
     * The name of a previous version.
     *
     * If not Set - will not move any issues to the new version.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.previousVersionName")
    protected String previousVersionName;

    /**
     * To use the lastest closed version as the previous version.
     *
     * If not Set - will not move any issues to the new version.
     *
     * <strong>Note:</strong> No effect when {@link #previousVersionName}
     * is filled.
     *
     * @since 1.6
     */
    @Parameter(property = "redmine.useLatestClosedVersion")
    protected boolean useLatestClosedVersion;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.skipNextVersion", defaultValue = "false")
    protected boolean skipNextVersion;

    /**
     * A flag to test plugin but send nothing to redmine.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * A flag to restrict only one run in a build (for multi-module context).
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 1.6
     */
    @Parameter(property = "redmine.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** effective date to set */
    private Date date;

    /** flag to mark if a runOnce goal was done */
    protected boolean runOnceDone;

    public NextVersionMojo() {
        super(true, false);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// DryRunAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isDryRun() {
        return dryRun;
    }

    @Override
    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// SkipOrRunOnlyOnceAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getSkipProperty() {
        return "skipNextVersion";
    }

    @Override
    public boolean isGoalSkip() {
        return skipNextVersion;
    }

    @Override
    public boolean isRunOnce() {
        return runOnce;
    }

    @Override
    public boolean isRunOnlyOnRoot() {
        return runOnlyOnRoot;
    }

    @Override
    public boolean isRunOnceDone() {
        return runOnceDone;
    }

    @Override
    public boolean checkRunOnceDone() {
        // compute unique key
        StringBuilder buffer = new StringBuilder("next-version");
        buffer.append("##").append(projectId);
        buffer.append("##").append(versionId);
        buffer.append("##").append(releaseVersion);
        buffer.append("##").append(versionDescription);
        buffer.append("##").append(previousVersionName);

        String key = buffer.toString();

        return !needInvoke(runOnce, runOnlyOnRoot, key);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        if (StringUtils.isBlank(versionId)) {
            throw new MojoExecutionException("required a versionId parameter");
        }

        versionId = PluginHelper.removeSnapshotSuffix(versionId);

        if (runOnceDone = isRunOnce() && checkRunOnceDone()) {
            return;
        }

        if (StringUtils.isNotBlank(effectiveDate)) {
            try {
                date = dateFormat.parse(effectiveDate);
            } catch (ParseException e) {
                throw new MojoExecutionException("could not parse effectivate date " + effectiveDate + " for reason " + e.getMessage(), e);
            }
        }
        super.init();
    }

    @Override
    protected void doAction() throws Exception {

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no data will be send!\n");
        }

        // get version
        Version v = getProjectVersion(versionId);

        String safePreviousVersionName = null;

        if (StringUtils.isNotBlank(previousVersionName)) {

            // retrieve previous version
            Version previousVersion = getProjectVersion(previousVersionName);
            if (previousVersion == null) {
                getLog().warn("can not find the previous version " + previousVersionName);
            } else {
                safePreviousVersionName = previousVersionName;
            }

            if (useLatestClosedVersion) {
                getLog().warn("useLatestClosedVersion flag has no effect when previousVersionName property is filled.");
            }
        } else if (useLatestClosedVersion) {

            // get all version
            Version previousVersion = service.getLastestClosedVersion(projectId);
            if (previousVersion == null) {
                getLog().warn("No latest closed version");
            } else {
                getLog().info("Will use as previous version (the latest closed one): " + previousVersion.getName());
                safePreviousVersionName = previousVersion.getName();
            }
        }

        if (v == null) {
            // version must be created
            v = new Version();
            v.setName(versionId);
        }

        if (StringUtils.isNotBlank(versionDescription)) {
            v.setDescription(versionDescription.trim());
        }

        if (date != null) {
            v.setEffectiveDate(date);
        }

        releaseVersion = v;

        if (!dryRun) {

            // create version
            getLog().info("next version " + releaseVersion.getName() + (safePreviousVersionName != null ? " from previous version " + safePreviousVersionName : ""));

            service.nextVersion(projectId, safePreviousVersionName, releaseVersion);
        }
    }
}
