/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.report;

import org.apache.maven.reporting.MavenReportException;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.plugin.IssueCollectorConfiguration;
import org.nuiton.jredmine.plugin.IssuesCollector;
import org.nuiton.jredmine.service.RedmineServiceException;
import org.nuiton.plugin.PluginHelper;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Abstract Generates a report for issues from Redmine's server
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public abstract class AbstractIssuesReport extends AbstractRedmineReport implements IssueCollectorConfiguration {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The meta to group issues (or null if no grouping)
     *
     * @since 1.0.0
     */
    protected final String group;

    /** cache of collected issues */
    protected static Map<IssueCollectorConfiguration, Issue[]> issuesCache = new HashMap<IssueCollectorConfiguration, Issue[]>();

    /** Issues to used for report */
    protected Issue[] issues;

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractIssuesReport
    ///////////////////////////////////////////////////////////////////////////

    protected abstract String getColumnNames();

    protected abstract Map<String, String> getFilters();

    protected AbstractIssuesReport(String group) {
        this.group = group;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// MavenReport
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public final String getOutputName() {
        return "redmine-report" + (group == null ? "" : "-by-" + group);
    }

    @Override
    public final String getName(Locale locale) {
        return getBundle(locale).getString("report.name" + (group == null ? "" : "-" + group));
    }

    @Override
    public final String getDescription(Locale locale) {
        return getBundle(locale).getString("report.description" + (group == null ? "" : "-" + group));
    }

    ///////////////////////////////////////////////////////////////////////////
    /// IssueCollectionConfiguration
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getCategoryIds() {
        return getFilters() == null ? null : getFilters().get("category");
    }

    @Override
    public String getVersionNames() {
        return getFilters() == null ? null : getFilters().get("version");
    }

    @Override
    public String getPriorityIds() {
        return getFilters() == null ? null : getFilters().get("priority");
    }

    @Override
    public String getStatusIds() {
        return getFilters() == null ? null : getFilters().get("status");
    }

    @Override
    public String getTrackerIds() {
        return getFilters() == null ? null : getFilters().get("tracker");
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void init() throws Exception {

        setVersionId(PluginHelper.removeSnapshotSuffix(getVersionId()));

        super.init();

        issues = issuesCache.get(this);

        if (issues == null) {

            if (isVerbose()) {
                getLog().info("Collect issues for " + this);
            }

            // init issues collector

            IssuesCollector collector = new IssuesCollector(getLog(), verbose);

            try {

                collector.collect(service, this);

                // get issues from collector

                issues = collector.getIssues();

                // store it in cache

                issuesCache.put(this, issues);

            } catch (RedmineServiceException ex) {
                throw new MavenReportException("could not obtains issues for reason " + ex.getMessage(), ex);
            } finally {
                collector.clearFilters();
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractRedmineReport
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void executeReport(Locale locale) throws MavenReportException {

        if (isOnlyCurrentVersion()) {
            getLog().info(getBundle(locale).getString("report.message.only.for.current.version") + " " + getVersionId());
        }
        String id = getProjectId();

        try {

            if (issues == null || issues.length == 0) {
                getLog().warn("no issue to treate, will generate an empty raport.");

                IssueReportGenerator reportGenerator = new IssueReportGenerator();

                reportGenerator.doGenerateEmptyReport(getBundle(locale), getSink());

            } else {
                IssueReportGenerator report = new IssueReportGenerator(getLog(), getColumnNames(), getGroup());

                report.setIssueLinkTemplate(getIssueLinkTemplate());
                report.setVersionLinkTemplate(getVersionLinkTemplate());
                report.setUrl(url.toString());

                report.setIssues(issues);
                report.setUsers(service.getProjectMembers(id));
                report.setIssueCategories(service.getIssueCategories(id));
                report.setIssueStatuses(service.getIssueStatuses());
                report.setIssuePriorities(service.getIssuePriorities());
                report.setTrackers(service.getTrackers(id));
                report.setVersions(service.getVersions(id));

                report.doGenerateReport(getBundle(locale), getSink());
            }

        } catch (MavenReportException mre) {
            // Rethrow this error from RedmineReportGenerator( String, String )
            // so that the build fails
            throw mre;
        } catch (Exception e) {
            getLog().error("Could not execute report", e);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected final String getGroup() {
        return group;
    }

    protected final String getIssueLinkTemplate() {
        return issueLinkTemplate;
    }

    protected final String getVersionLinkTemplate() {
        return versionLinkTemplate;
    }
}
