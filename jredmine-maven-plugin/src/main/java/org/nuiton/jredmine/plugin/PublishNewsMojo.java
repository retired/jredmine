/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.jredmine.model.News;
import org.nuiton.plugin.PluginHelper;

import java.io.File;

/**
 * Publish a news on redmine server.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "publish-news", requiresOnline = true, requiresProject = true)
public class PublishNewsMojo extends AbstractRedmineMojoWithProject implements DryRunAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The content file of the news.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.newsContentFile", required = true)
    protected File newsContentFile;

    /**
     * Short description or introduction of the released artifact.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.newsSummary")
    protected String newsSummary;

    /**
     * The title of the news to create on redmine server.
     *
     * <b>Note : </b> the size can not be more than 60 caracters (due to a
     * redmine limitation).
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.newsTitle", required = true)
    protected String newsTitle;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.skipPublishNews", defaultValue = "false")
    protected boolean skipPublishNews;

    /**
     * A flag to test plugin but send nothing to redmine.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * A flag to restrict only one run in a build (for multi-module context).
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 1.6
     */
    @Parameter(property = "redmine.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    /** flag to mark if a runOnce goal was done */
    protected boolean runOnceDone;

    public PublishNewsMojo() {
        super(true);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// DryRunAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isDryRun() {
        return dryRun;
    }

    @Override
    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// SkipOrRunOnlyOnceAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getSkipProperty() {
        return "skipPublishNews";
    }

    @Override
    public boolean isGoalSkip() {
        return skipPublishNews;
    }

    @Override
    public boolean checkRunOnceDone() {
        // compute unique key
        StringBuilder buffer = new StringBuilder("publish-news");
        buffer.append("##").append(newsTitle);
        buffer.append("##").append(newsSummary);
        buffer.append("##").append(newsContentFile);

        String key = buffer.toString();

        return !needInvoke(runOnce, false, key);
    }

    @Override
    public boolean isRunOnce() {
        return runOnce;
    }

    @Override
    public boolean isRunOnlyOnRoot() {
        return runOnlyOnRoot;
    }

    @Override
    public boolean isRunOnceDone() {
        return runOnceDone;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        runOnceDone = false;

        if (isRunOnce()) {
            runOnceDone = checkRunOnceDone();
            if (runOnceDone) {
                return;
            }
        }

        if (newsSummary == null || newsSummary.trim().isEmpty()) {
            newsSummary = project.getUrl();
        }

        if (!newsContentFile.exists()) {
            // no file to publish...
            throw new MojoExecutionException("could not find the template " + newsContentFile);
        }

        newsTitle = newsTitle.trim();
        if (newsTitle.length() > 60) {
            getLog().warn("News title can not be longer than 60 caracters, but was " + newsTitle.length());
            newsTitle = newsTitle.substring(0, 59);
            getLog().warn("will use the restricted title : " + newsTitle);
        }

        super.init();
    }

    @Override
    protected void doAction() throws Exception {

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no data will be send!\n");
        }

        // create news to publish

        News news = new News();
        news.setAuthorId(releaseUser.getId());
        news.setProjectId(releaseProject.getId());
        news.setTitle(newsTitle);
        news.setSummary(newsSummary);

        String newsContent = PluginHelper.readAsString(newsContentFile, encoding);

        news.setDescription(newsContent);

        if (dryRun) {
            getLog().info("news title   : " + news.getTitle());
            getLog().info("news summary : " + news.getSummary());
            getLog().info("news content :\n" + newsContent);
            return;
        }

        // publish news

        getLog().info("publish news " + news.getTitle());

        if (verbose) {
            getLog().info("redmine announcement :\n" + newsContent);
        }

        news = service.addNews(projectId, news);

        if (verbose) {
            getLog().info("done. news id : " + news.getId());
        }
    }
}
