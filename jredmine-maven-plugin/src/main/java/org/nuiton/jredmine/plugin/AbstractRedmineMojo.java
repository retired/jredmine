/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.IssueManagement;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.jredmine.client.RedmineClientAuthConfiguration;
import org.nuiton.jredmine.service.RedmineService;
import org.nuiton.jredmine.service.RedmineServiceConfiguration;
import org.nuiton.plugin.AbstractPlugin;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Abstract redmine mojo.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public abstract class AbstractRedmineMojo extends AbstractPlugin implements RedmineServiceConfiguration {

    public static final String REDMINE_SYSTEM = "redmine";

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The real basedir redmine url.
     *
     * If no url is given, will use the issue management url.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.url")
    protected URL url;

    /**
     * The redmine's server login apiKey.
     *
     * <b>Note:</b> This parameter (or {@link #username} + {@link #password}) is mandatory if you
     * not use a {@code anonymous} service.
     *
     * @since 1.5
     */
    @Parameter(property = "redmine.apiKey")
    protected String apiKey;

    /**
     * The redmine's server login.
     *
     * <b>Note:</b> This parameter (+ {@link #password}) (or {@link #apiKey}) is mandatory if you
     * not use a {@code anonymous} service.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.username")
    protected String username;

    /**
     * The redmine's server password.
     *
     * <b>Note:</b> This parameter (+ {@link #username}) (or {@link #apiKey}) is mandatory if you
     * not use a {@code anonymous} service.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.password")
    protected String password;

    /**
     * The encoding used to read and write files.
     *
     * <b>Note:</b> If nothing is filled here, we will use the system
     * property {@code file.encoding}.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.encoding", defaultValue = "${project.build.sourceEncoding}")
    protected String encoding;

    /**
     * Flag to activate verbose mode.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * Flag to fail build if configuration is not ok.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.safe", defaultValue = "true")
    protected boolean safe;

    /**
     * Current project.
     *
     * @since 1.0.0
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * Maven session.
     *
     * @since 1.0.0
     */
    @Parameter(defaultValue = "${session}", readonly = true)
    protected MavenSession session;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo components
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Redmine service.
     *
     * @since 1.0.0
     */
    @Component
    protected RedmineService service;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** the date format used to write a date */
    protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /** flag to mark if service was sucessfull init */
    protected boolean serviceInit;

    protected boolean initOk = true;

    private RedmineClientAuthConfiguration authConfiguration;

    ///////////////////////////////////////////////////////////////////////////
    /// Plugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// PluginWithEncoding
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineClientConfiguration
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public RedmineClientAuthConfiguration getAuthConfiguration() {
        return authConfiguration;
    }

    @Override
    public void setAuthConfiguration(RedmineClientAuthConfiguration authConfiguration) {
        this.authConfiguration = authConfiguration;
    }

    @Override
    public URL getUrl() {
        return url;
    }

    @Override
    public void setUrl(URL url) {
        this.url = url;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        // check issue management

        IssueManagement issueManagement = project.getIssueManagement();

        if (issueManagement == null) {
            throw new MojoExecutionException("No Issue Management set.");
        } else if (issueManagement.getUrl() == null || issueManagement.getUrl().trim().equals("")) {
            throw new MojoExecutionException("No URL set in Issue Management.");
        } else if (issueManagement.getSystem() != null && !issueManagement.getSystem().equalsIgnoreCase(REDMINE_SYSTEM)) {
            throw new MojoExecutionException("Redmine's Plugin only supports 'redmine' Issue Management system.");
        }

        // prepare Redmine service configuration

        if (url == null || url.toString().isEmpty()) {

            // no redmine url specified, guess it from issueManagement

            url = new URL(issueManagement.getUrl());

            if (verbose) {
                getLog().info("use the url from issue management : " + url);
            }
        }

        // apply configuration

        setUrl(url);

        authConfiguration = RedmineClientAuthConfiguration.newConf(
                apiKey, username, password
        );

        if (verbose) {

            if (authConfiguration.isUseApiKey()) {
                getLog().info("Redmine configuration :\n>> host     : " +
                              getUrl() + "\n>> apiKey: " +
                              apiKey);
            } else if (authConfiguration.isUseUsername()) {
                getLog().info("Redmine configuration :\n>> host     : " +
                              getUrl() + "\n>> username : " +
                              username);
            } else {
                getLog().info("Redmine anonymous configuration :\n>> host     : " +
                              getUrl());
            }
        }

        // init Redmine service

        try {

            service.init(this);
            serviceInit = true;
        } catch (Exception e) {
            if (safe) {
                throw e;
            }
            serviceInit = false;
            initOk = false;
            getLog().error("could not init Redmine service [" + getUrl() + "]", e);
        }
    }

    @Override
    protected boolean checkSkip() {

        if (this instanceof SkipOrRunOnlyOnceAware) {
            SkipOrRunOnlyOnceAware m = ((SkipOrRunOnlyOnceAware) this);

            if (m.isGoalSkip()) {
                getLog().info("Skipping goal (" + m.getSkipProperty() + " flag is on).");
                return false;
            }
            if (m.isRunOnce() && m.isRunOnceDone()) {
                getLog().info("Skipping goal (runOnce flag is on, and was already executed).");
                return false;
            }
        }

        if (!serviceInit) {
            getLog().error("could not init Redmine service [" + getUrl() + "]");
            return false;
        }

        return true;
    }

    @Override
    protected void afterExecute() {
        closeService();
    }

    /**
     * Re-expose the protected method for test purposes...
     *
     * @throws Exception if any problems
     */
    @Override
    protected abstract void doAction() throws Exception;

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected void closeService() {
        if (service != null) {
            try {
                service.destroy();
            } catch (Exception ex) {
                getLog().error("could not close redmine client for reason " + ex.getMessage(), ex);
            }

        }
    }

    /**
     * Convinient method to throw the given {@code message}
     * if {@link #safe} flag is on.
     *
     * @param message message to throw if safe mode is on
     * @throws MojoExecutionException the error thrown if safe flag is on
     * @since 1.4.1
     */
    protected void failIfSafe(String message) throws MojoExecutionException {
        if (safe) {
            throw new MojoExecutionException(message);
        }
    }
}
