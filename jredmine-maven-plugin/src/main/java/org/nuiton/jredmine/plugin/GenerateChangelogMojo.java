package org.nuiton.jredmine.plugin;

/*-
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.changes.model.Action;
import org.apache.maven.plugins.changes.model.ChangesDocument;
import org.apache.maven.plugins.changes.model.FixedIssue;
import org.apache.maven.plugins.changes.model.Release;
import org.apache.maven.plugins.changes.model.io.xpp3.ChangesXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.nuiton.plugin.AbstractPlugin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.util.Date;
import java.util.List;

/**
 * To generate a changelog.txt file from the changes.xml file.
 *
 * Created on 12/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
@Mojo(name = "generate-changelog")
public class GenerateChangelogMojo extends AbstractPlugin {

    /**
     * The path of the {@code changes.xml} file that will be converted into the changelog.txt.
     */
    @Parameter(property = "redmine.xmlPath", defaultValue = "${basedir}/src/changes/changes.xml", required = true)
    protected File xmlPath;

    /**
     * Where to generate the changelog file.
     */
    @Parameter(property = "redmine.outputFile", defaultValue = "${project.build.directory}/changelog.txt", required = true)
    protected File outputFile;

    /**
     * Flag to activate verbose mode.
     */
    @Parameter(property = "redmine.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to attach the generated changelog file (with <b>changelog</b> classifier).
     */
    @Parameter(property = "redmine.attach")
    protected boolean attach;

    /**
     * A flag to skip the goal.
     */
    @Parameter(property = "redmine.skipGenerateChangelog")
    protected boolean skipGenerateChangelog;

    /**
     * A flag to generate only once in a multi-module project.
     *
     * The changelog.txt file will be generated only once.
     */
    @Parameter(property = "redmine.generateOnce", defaultValue = "true")
    protected boolean generateOnce;

    /**
     * A flag to restrict only to run on root module.
     */
    @Parameter(property = "redmine.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    /**
     * Current project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * Maven session.
     */
    @Parameter(defaultValue = "${session}", readonly = true)
    protected MavenSession session;

    @Component
    private MavenProjectHelper mavenProjectHelper;

    @Override
    protected boolean checkSkip() {

        if (skipGenerateChangelog) {
            getLog().info("Skipping goal skipGenerateChangelog flag is on).");
            return false;
        }

        Date buildStartTime = session == null ? null : session.getStartTime();
        Date newStartTime = outputFile != null && outputFile.exists() ? new Date(outputFile.lastModified()) : null;

        boolean needInvoke = needInvoke(generateOnce, runOnlyOnRoot, buildStartTime, newStartTime);
        if (!needInvoke) {
            getLog().info("Skipping goal (runOnce flag is on, and was already executed).");
        }

        return needInvoke;

    }

    @Override
    protected void init() throws Exception {

        if (!xmlPath.exists()) {
            throw new MojoExecutionException("Could not find xmlPath: " + xmlPath);
        }

    }

    @Override
    protected void doAction() throws Exception {

        getLog().info("Loading changes.xml from file: " + xmlPath);

        ChangesXpp3Reader reader = new ChangesXpp3Reader();
        ChangesDocument changesDocument;
        Reader fileReader = new FileReader(xmlPath);
        try {
            changesDocument = reader.read(fileReader);
            fileReader.close();
        } finally {
            IOUtils.closeQuietly(fileReader);
        }

        getLog().info("Generate changelog in file: " + outputFile);

        String eol = SystemUtils.LINE_SEPARATOR;
        FileWriter writer = new FileWriter(outputFile);

        try {
            List<Release> releases = changesDocument.getBody().getReleases();

            for (Release release : releases) {

                writer.append(eol).append("Version ").append(release.getVersion()).append(" - ").append(release.getDateRelease()).append(eol);

                for (Object o : release.getActions("add")) {
                    Action a = (Action) o;
                    String dev = a.getDev();
                    String dueTo = a.getDueTo();
                    List<FixedIssue> fixedIssues = a.getFixedIssues();
                    String issue = a.getIssue();
                    String action = a.getAction();

                    writer.append(" o Feature #").append(issue).append(" ")
                          .append("due to ").append(dueTo).append(", thanks to ").append(dev)
                          .append(" - ").append(action).append(eol);
                }

                for (Object o : release.getActions("fix")) {
                    Action a = (Action) o;
                    String dev = a.getDev();
                    String dueTo = a.getDueTo();
                    List<FixedIssue> fixedIssues = a.getFixedIssues();
                    String issue = a.getIssue();
                    String action = a.getAction();

                    writer.append(" o Bug     #").append(issue).append(" ")
                          .append("due to ").append(dueTo).append(", thanks to ").append(dev)
                          .append(" - ").append(action).append(eol);
                }

                for (Object o : release.getActions("update")) {
                    Action a = (Action) o;
                    String dev = a.getDev();
                    String dueTo = a.getDueTo();
                    List<FixedIssue> fixedIssues = a.getFixedIssues();
                    String issue = a.getIssue();
                    String action = a.getAction();

                    writer.append(" o Task    #").append(issue).append(" ")
                          .append("due to ").append(dueTo).append(", thanks to ").append(dev)
                          .append(" - ").append(action).append(eol);
                }

            }

            writer.close();
        } finally {
            IOUtils.closeQuietly(writer);
        }

        if (attach) {

            getLog().info("Attach changelog file as *changelog* classifier.");
            mavenProjectHelper.attachArtifact(project, outputFile, "changelog");

        }
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
