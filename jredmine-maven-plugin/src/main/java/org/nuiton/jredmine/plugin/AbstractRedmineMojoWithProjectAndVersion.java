package org.nuiton.jredmine.plugin;
/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.service.RedmineServiceException;

/**
 * Abstract redmine mojo which needs a redmine project and version context.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class AbstractRedmineMojoWithProjectAndVersion extends AbstractRedmineMojoWithProject implements RedmineVersionAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Redmine version name.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionId", defaultValue = "${project.version}")
    protected String versionId;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** the version loaded in init. */
    protected Version releaseVersion;

    /**
     * flag to load in init a required version using the versionId
     * to redmine server.
     *
     * @since 1.4.1
     */
    private final boolean requireVersion;

    /**
     * All versions of the project loaded in init phase.
     *
     * @since 1.4.1
     */
    private Version[] projectVersions;

    protected AbstractRedmineMojoWithProjectAndVersion(boolean requireUser,
                                                       boolean requireVersion) {
        super(requireUser);
        this.requireVersion = requireVersion;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// RedmineVersionAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public final String getVersionId() {
        return versionId;
    }

    @Override
    public final void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        super.init();

        if (initOk) {

            try {
                // load project versions
                projectVersions = service.getVersions(projectId);
            } catch (RedmineServiceException e) {
                getLog().warn("could not retrieve project versions, for reason " + e.getMessage(), e);
                initOk = false;
            }

            if (initOk && requireVersion) {

                // load required version

                boolean r = initReleaseVersion();
                if (!r) {
                    failIfSafe("the version '" + versionId + "' could not be retrieve from redmine server.");
                    initOk = false;
                }
            }
        }
    }

    @Override
    protected boolean checkSkip() {

        boolean canContinue = super.checkSkip();

        if (canContinue && requireVersion) {

            if (releaseVersion == null) {

                getLog().error("the version '" + versionId + "' could not be retrieve from redmine server, goal is skip");
                canContinue = false;
            }
        }
        return canContinue;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected Version[] getProjectVersions() {
        return projectVersions;
    }

    protected Version getProjectVersion(String versionId) {
        Version v = Version.byVersionName(versionId, projectVersions);
        return v;
    }

    protected boolean initReleaseVersion() throws MojoExecutionException {

        if (StringUtils.isBlank(versionId)) {
            throw new MojoExecutionException("required a versionId parameter");
        }

        releaseVersion = getProjectVersion(versionId);

        return releaseVersion != null;
    }
}
