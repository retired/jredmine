/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;


import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.helper.plugin.CollectFilesMojo;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.jredmine.service.RedmineServiceException;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Publish files for a given project and version on redmine server.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "publish-attachments", requiresOnline = true, requiresProject = true)
public class PublishAttachmentsMojo extends AbstractRedmineMojoWithProjectAndVersion implements DryRunAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The path of a properties files where files to deploy are descriped.
     *
     * The key of a property is the sortor key
     *
     * The value of a property is the path of the file to upload.
     *
     * If no Set - will not use this source
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.filesFromProperties")
    protected File filesFromProperties;

    /**
     * A list of files to deploy.
     *
     * If no Set - will not use this source
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.files")
    protected File[] files;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.skipCollectReleaseAttachments", defaultValue = "false")
    protected boolean skipPublishAttachments;

    /**
     * A flag to test plugin but send nothing to redmine.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * A flag to restrict only one run in a build (for multi-module context).
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 1.6
     */
    @Parameter(property = "redmine.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** files to deploy */
    protected List<File> allFiles;

    /** flag to mark if a runOnce goal was done */
    protected boolean runOnceDone;

    public PublishAttachmentsMojo() {
        super(true, true);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// DryRunAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isDryRun() {
        return dryRun;
    }

    @Override
    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// SkipOrRunOnlyOnceAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getSkipProperty() {
        return "skipPublishAttachments";
    }

    @Override
    public boolean isGoalSkip() {
        return skipPublishAttachments;
    }

    @Override
    public boolean isRunOnce() {
        return runOnce;
    }

    @Override
    public boolean isRunOnlyOnRoot() {
        return runOnlyOnRoot;
    }

    @Override
    public boolean isRunOnceDone() {
        return runOnceDone;
    }

    @Override
    public boolean checkRunOnceDone() {

        // compute unique key
        StringBuilder buffer = new StringBuilder("publish-attachments");
        for (File allFile : allFiles) {
            buffer.append("##").append(allFile);
        }

        String key = buffer.toString();

        return !needInvoke(runOnce, false, key);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        versionId = PluginHelper.removeSnapshotSuffix(versionId);

        if (filesFromProperties != null && !filesFromProperties.exists()) {
            // no file to publish...
            getLog().warn("could not find the properties file " + filesFromProperties);
            getLog().warn("will not use this source");
            filesFromProperties = null;
        }

        if (filesFromProperties != null && files != null && files.length > 0) {
            throw new MojoExecutionException("can not use both sources files and filesFromProperties");
        }

        if (filesFromProperties != null) {

            allFiles = getFilesToDeploy();

        } else {
            allFiles = Arrays.asList(files);
        }

        if (allFiles == null || allFiles.isEmpty()) {
            return;
        }

        if (runOnceDone = isRunOnce() && checkRunOnceDone()) {
            return;
        }

        super.init();
    }

    @Override
    protected boolean checkSkip() {
        if (allFiles == null || allFiles.isEmpty()) {
            getLog().warn("No attachment to publish, will skip the goal.");
            return false;
        }

        boolean b = super.checkSkip();
        return b;
    }

    @Override
    protected void doAction() throws Exception {

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no data will be send!\n");
        }

        String basedir = project.getBasedir().getAbsolutePath();
        int basedirLength = basedir.length() + 1;
        for (File f : allFiles) {

            if (!f.exists()) {
                getLog().warn("can not upload a non existing file " + f);
                continue;
            }

            if (f.getAbsolutePath().startsWith(basedir)) {
                getLog().info("upload " + f.getAbsolutePath().substring(basedirLength));
            } else {
                getLog().info("upload " + f.getAbsolutePath());
            }

            if (dryRun) {
                continue;
            }

            // upload the file

            long t0 = System.nanoTime();
            Attachment a = new Attachment();
            a.setFilename(f.getName());
            a.setAuthorId(releaseUser.getId());
            a.setContainerId(releaseVersion.getId());

            a.setToUpload(f);

            // do upload
            try {
                Attachment result = service.addAttachment(projectId, releaseVersion.getName(), a);
                long t1 = System.nanoTime();
                if (isVerbose()) {
                    getLog().info("done in  " + PluginHelper.convertTime(t1 - t0) + ". attachment id : " + result.getId() + ", size : " + PluginHelper.convertMemory(result.getFilesize()));
                }
            } catch (RedmineServiceException e) {

                getLog().warn("");
                String lengthHumanReadable = StringUtil.convertMemory(f.length());
                getLog().warn(String.format("Could not upload attachment %s, may be the file length (%s) exceed the one authorized by the redmine instance.", f, lengthHumanReadable));
                getLog().warn("");
                throw e;

            }

        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Others
    ///////////////////////////////////////////////////////////////////////////

    protected List<File> getFilesToDeploy() throws MojoExecutionException {

        // load definition file
        List<File> incoming;

        try {
            incoming = new CollectFilesMojo().getFiles(filesFromProperties);
            getLog().info("Loaded " + filesFromProperties);
        } catch (IOException ex) {
            throw new MojoExecutionException("could not load file " + filesFromProperties, ex);
        }

        // sort attachments by file names 

        Collections.sort(incoming, new Comparator<File>() {

            @Override
            public int compare(File o1, File o2) {

                return o1.getName().compareTo(o2.getName());
            }
        });

        return incoming;
    }
}

