/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.report;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.Map;

/**
 * Generates a report for issues from Redmine's server group by version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "issues-report-by-version", requiresOnline = true, requiresProject = true, requiresReports = true)
public class IssuesReportByVersion extends AbstractIssuesReport {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Maximum number of entries to be fetched from redmine.
     *
     * <b>Note:</b> use value 0 to have no limits
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.maxEntries", defaultValue = "100")
    protected int maxEntriesByVersion;

    /**
     * If you only want to show issues for the current version in the report.
     * The current version being used is <code>${project.version}</code> minus
     * any "-SNAPSHOT" suffix.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.onlyCurrentVersion", defaultValue = "false")
    protected boolean onlyCurrentVersionByVersion;

    /**
     * Sets some filters on issues to include.
     * the possible keys are :
     * <ul>
     * <li><b>version</b> : to restrict on version fixed </li>
     * <li><b>status</b> : to restrict on some status</li>
     * <li><b>priority</b> : to restrict on some priorties</li>
     * <li><b>category</b> : to restrict on some categories</li>
     * <li><b>tracker</b> : to restrict on some tracker (says type of issues)</li>
     * </ul>
     *
     * Values are Redmine's internal ids (except for the version entry
     * which use Redmine's version names).
     *
     * Multiple values can be separated by commas.
     *
     *
     * <b>Note :</b> If a value is set to empty - that means to not filter on
     * that property.
     *
     *
     * To see the internal ids, use the command :
     * <pre>
     * mvn redmine:display-ids -Dtype=&lt;entry key&gt;
     * </pre>
     *
     * @since 1.0.0
     */
    @Parameter
    protected Map<String, String> filtersByVersion;

    /**
     * Sets the column names that you want to show in the report. The columns
     * will appear in the report in the same order as you specify them here.
     * Multiple values can be separated by commas.
     * <p>
     * Valid columns are: <code>Key</code>, <code>Summary</code>,
     * <code>Status</code>, <code>Assignee</code>,
     * <code>Reporter</code>, <code>Tracker</code>, <code>Priority</code>,
     * <code>Version</code>, <code>Category</code>, <code>Created</code> and
     * <code>Updated</code>.
     * </p>
     *
     * @since 2.0
     */
    @Parameter(property = "redmine.columnNames", defaultValue = "Tracker,Category,Key,Summary,Status,Assignee,Version")
    protected String columnNamesByVersion;

    /**
     * A flag to skip the report.
     *
     * This can be usefull since there is no way to skip a report from an
     * inherited reportSet configuration.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.skipIssueReportByVersion", defaultValue = "false")
    protected boolean skipIssueReportByVersion;

    public IssuesReportByVersion() {
        super("version");
    }

    ///////////////////////////////////////////////////////////////////////////
    /// IssueCollectionConfiguration
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isOnlyCurrentVersion() {
        return onlyCurrentVersionByVersion;
    }

    @Override
    public int getMaxEntries() {
        return maxEntriesByVersion;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractIssuesReport
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected String getColumnNames() {
        return columnNamesByVersion;
    }

    @Override
    protected Map<String, String> getFilters() {
        return filtersByVersion;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractRedmineReport
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected boolean skipReport() {
        return skipIssueReportByVersion;
    }
}
