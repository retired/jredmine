/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.announcement;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.changes.model.Action;
import org.apache.maven.plugins.changes.model.Release;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.Attachment;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class AnnouncementGenerator {

    /** The token any of urls denoting the base URL for the given url. */
    private static final String URL_TOKEN = "%URL%";

    /** The token in {@link #getAttachmentLinkTemplate()} denoting the attachment ID. */
    private static final String ATTACHMENT_TOKEN = "%FILE%";

    private static final Pattern ARTIFACT_PATTERN =
            Pattern.compile("(.+)?--(.+)?");

    private final AnnouncementGeneratorConfiguration configuration;

    public AnnouncementGenerator(AnnouncementGeneratorConfiguration configuration) {
        this.configuration = configuration;
    }

    public String getAttachmentLinkTemplate() {
        return configuration.getAttachmentLinkTemplate();
    }

    public Log getLog() {
        return configuration.getLog();
    }

    public String getUrl() {
        return configuration.getUrl().toString();
    }

    public Map<Attachment, String> getAttachmentsUrls(Attachment[] attachments) {

        // transform attachments urls

        boolean hasAttachmentLinks = canGenerateAttachmentLinks();

        Map<Attachment, String> urls = null;

        if (hasAttachmentLinks) {

            urls = new LinkedHashMap<Attachment, String>();
            for (Attachment a : attachments) {
                String u = parseAttachmentLink(a.getId() + "");
                urls.put(a, u);
            }
        } else {
            getLog().warn("can not render attachments urls");
        }

        return urls;
    }

    private class ArtifactInfo {
        private File src;

        private String groupId;

        private String artifactId;

        private String url;

        private String extension;

        private String filename;

        public ArtifactInfo(String url, String versionId, File srcfile) {

            String artifactInfo = srcfile.getParentFile().getName();
            Matcher matcher = ARTIFACT_PATTERN.matcher(artifactInfo);

            if (!matcher.matches()) {
                getLog().error("no matching for file " + artifactInfo +
                               " (" + srcfile + ")");
            }
            artifactId = matcher.group(2);
            groupId = matcher.group(1).replaceAll("\\.", "/") + "/" + artifactId;
            String filename = srcfile.getName();
            if ("pom.xml".equals(filename)) {
                filename = artifactId + "-" + versionId + ".pom";
                src = new File(filename);
            } else {
                src = srcfile;
            }
            this.url = url + groupId + "/" + versionId + "/" + filename;

            extension = FileUtils.getExtension(src.getName());
            this.filename = src.getName().substring(0, extension.length() - 1);
        }
    }

    protected class ArtifactComparator implements Comparator<ArtifactInfo> {

        final List<String> groupIds;

        public ArtifactComparator(List<String> groupIds) {
            this.groupIds = groupIds;
        }

        @Override
        public int compare(ArtifactInfo o1, ArtifactInfo o2) {
            // sort by groupId
            int result = groupIds.indexOf(o1.groupId) -
                         groupIds.indexOf(o2.groupId);
            if (result != 0) {
                return result;
            }
            result = o1.extension.compareTo(o2.extension);
            if (result != 0) {
                if ("pom".equals(o1.extension)) {
                    return -1;
                }
                if ("pom".equals(o2.extension)) {
                    return 1;
                }
                return result;
            }
            // sort on filename
            result = o1.filename.compareTo(o2.filename);
            return result;
        }
    }

    public Map<File, String> getArtifactsUrls(String url,
                                              String versionId,
                                              boolean verbose,
                                              File[] files) {
        Map<File, String> urls = new LinkedHashMap<File, String>();
        List<String> groupIds = new ArrayList<String>();
        List<ArtifactInfo> list = new ArrayList<ArtifactInfo>();

        for (File f : files) {
            ArtifactInfo artifactInfo = new ArtifactInfo(url, versionId, f);
            list.add(artifactInfo);
            if (!groupIds.contains(artifactInfo.groupId)) {
                if (verbose) {
                    getLog().info("detected groupId " + artifactInfo.groupId);
                }
                groupIds.add(artifactInfo.groupId);
            }
        }

        Collections.sort(list, new ArtifactComparator(groupIds));

        for (ArtifactInfo a : list) {
            if (verbose) {
                getLog().info("artifact file " + a.src.getName() + " --> " +
                              a.url);
            }
            urls.put(a.src, a.url);
        }
        return urls;
    }

    public Context createVelocityContext(List<?> releases) throws MojoExecutionException {

        // prepare velocity context
        Context context = new VelocityContext();
        context.put("releases", releases);
        context.put("groupId", configuration.getGroupId());
        context.put("artifactId", configuration.getArtifactId());
        context.put("version", configuration.getVersionId());
        context.put("packaging", configuration.getPackaging());
        context.put("url", configuration.getUrl());
        context.put("projectUrl", configuration.getProjectUrl());

        Release release = getLatestRelease(releases, configuration.getVersionId());

        context.put("release", release);
        context.put("introduction", configuration.getIntroduction());
        context.put("developmentTeam", configuration.getDevelopmentTeam());
        context.put("finalName", configuration.getFinalName());
        context.put("urlDownload", configuration.getUrlDownload());
        context.put("mavenRepoUrl", configuration.getDeploymentUrl());
        context.put("project", configuration.getProject());

        // add artifacts in context
        Map<File, String> artifactUrls = configuration.getArtifactUrls();
        if (artifactUrls == null || artifactUrls.isEmpty()) {
            context.put("withArtifacts", false);

        } else {
            context.put("withArtifacts", true);
            context.put("artifactUrls", artifactUrls);
        }
        // add attachments in context

        Map<Attachment, String> attachmentUrls =
                configuration.getAttachmentUrls();
        if (attachmentUrls == null || attachmentUrls.isEmpty()) {
            context.put("withAttachments", false);

        } else {
            context.put("withAttachments", true);
            context.put("attachmentUrls", attachmentUrls);
        }

        Map<String, Object> announceParameters =
                configuration.getAnnounceParameters();
        if (announceParameters == null) {
            // empty Map to prevent NPE in velocity execution
            context.put("announceParameters",
                        Collections.<Object, Object>emptyMap());
        } else {
            context.put("announceParameters", announceParameters);
        }
        return context;
    }

    public void doGenerate(VelocityEngine engine,
                           Context context,
                           File out,
                           String templatePath,
                           String templateEncoding) throws MojoExecutionException {

        try {
            PluginHelper.createDirectoryIfNecessary(out.getParentFile());

            Writer writer = new OutputStreamWriter(new FileOutputStream(out),
                                                   templateEncoding
            );

            try {
                Template velocityTemplate = engine.getTemplate(templatePath,
                                                               templateEncoding
                );

                velocityTemplate.merge(context, writer);

                writer.flush();
            } finally {

                writer.close();
            }

        } catch (ResourceNotFoundException rnfe) {
            throw new MojoExecutionException("Resource not found.", rnfe);
        } catch (Exception e) {
            throw new MojoExecutionException(e.toString(), e);
        }
    }

    /**
     * Checks whether links to the issues can be generated.
     *
     * @return {@code true} if issue links can be generated,
     *         {@code false} otherwise.
     */
    public boolean canGenerateAttachmentLinks() {
        String attachmentLinkTemplate = getAttachmentLinkTemplate();
        String url = getUrl();
        return !StringUtils.isBlank(attachmentLinkTemplate) &&
               attachmentLinkTemplate.indexOf(ATTACHMENT_TOKEN) > 0 &&
               (!attachmentLinkTemplate.contains(URL_TOKEN) ||
                !StringUtils.isBlank(url));
    }

    protected String parseAttachmentLink(String id) {
        String parseLink;
        String issueLink = getAttachmentLinkTemplate();
        parseLink = issueLink.replaceFirst(ATTACHMENT_TOKEN, id);

        if (parseLink.contains(URL_TOKEN)) {
//            String u = this.url.substring(0, this.url.lastIndexOf("/"));
//            parseLink = parseLink.replaceFirst(URL_TOKEN, u);
            parseLink = parseLink.replaceFirst(URL_TOKEN, getUrl());
        }

        return parseLink;
    }

    /**
     * Get the latest release by matching the supplied releases
     * with the version from the pom.
     *
     * @param releases       list of releases
     * @param releaseVersion the release version
     * @return A <code>Release</code> that matches the next release of the
     *         current project
     * @throws MojoExecutionException if any pb
     */
    public Release getLatestRelease(List<?> releases, String releaseVersion)
            throws MojoExecutionException {

        Release release;

        String pomVersion = releaseVersion;

        getLog().debug("Found " + releases.size() + " releases.");

        for (Object release1 : releases) {
            release = (Release) release1;
            if (getLog().isDebugEnabled()) {
                getLog().debug(
                        "The release: " + release.getVersion() +
                        " has " + release.getActions().size() + " actions.");
            }

            if (release.getVersion() != null &&
                release.getVersion().equals(pomVersion)) {
                if (getLog().isDebugEnabled()) {
                    getLog().debug("Found the correct release: " +
                                   release.getVersion());
                    logRelease(release);
                }
                return release;
            }
        }

        release = getRelease(releases, pomVersion);
        boolean isFound = release != null;

        if (!isFound) {
            throw new MojoExecutionException(
                    "Couldn't find the release '" + pomVersion +
                    "' among the supplied releases.");
        } else {
        }
        return release;
    }

    protected Release getRelease(List<?> releases, String version) {
        Release release;
        for (Object release1 : releases) {
            release = (Release) release1;
            if (getLog().isDebugEnabled()) {
                getLog().debug("The release: " + release.getVersion() +
                               " has " + release.getActions().size() +
                               " actions."
                );
            }

            if (release.getVersion() != null &&
                release.getVersion().equals(version)) {
                if (getLog().isDebugEnabled()) {
                    getLog().debug("Found the correct release: " +
                                   release.getVersion());
                    logRelease(release);
                }
                return release;
            }
        }
        return null;
    }

    private void logRelease(Release release) {
        Action action;
        for (Object o : release.getActions()) {
            action = (Action) o;
            getLog().debug("o " + action.getType());
            getLog().debug("issue : " + action.getIssue());
            getLog().debug("action : " + action.getAction());
            getLog().debug("dueTo : " + action.getDueTo());
            getLog().debug("dev : " + action.getDev());
        }
    }

}
