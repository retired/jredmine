/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.announcement;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Generate the content of the release email announcement.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "generate-email-announcement", requiresOnline = true, requiresProject = true)
public class GenerateEmailAnnouncementMojo extends AbstractAnnouncementMojo {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The Velocity template used to format the release email announcement.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.emailAnnouncementTemplate", defaultValue = "release-email-announcement.vm", required = true)
    protected String emailAnnouncementTemplate = "release-email-announcement.vm";

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "changes.skipGenerateEmailAnnouncement", defaultValue = "false")
    protected boolean skipGenerateEmailAnnouncement;

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractAnnouncementMojo
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected String getAnnouncementTemplate() {
        return emailAnnouncementTemplate;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// SkipOrRunOnlyOnceAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getSkipProperty() {
        return "skipGenerateEmailAnnouncement";
    }

    @Override
    public boolean isGoalSkip() {
        return skipGenerateEmailAnnouncement;
    }
}
