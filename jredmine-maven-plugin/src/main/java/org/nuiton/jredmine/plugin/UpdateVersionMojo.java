/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.model.VersionStatusEnum;
import org.nuiton.plugin.PluginHelper;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

/**
 * Create or update a version on redmine server.
 *
 * Will add a the version if not existing, otherwise, will update the version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
@Mojo(name = "update-version", requiresOnline = true, requiresProject = true)
public class UpdateVersionMojo extends AbstractRedmineMojoWithProjectAndVersion implements DryRunAware {

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo parameters
    ///////////////////////////////////////////////////////////////////////////

    /**
     * The version description to update to redmine server.
     *
     * Note : if not set, no update of the description will be made.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.versionDescription")
    protected String versionDescription;

    /**
     * A flag to close the version on redmine (will fix effectiveDate).
     *
     * The status of the version will be setted to {@code closed} then.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.closeVersion", defaultValue = "false")
    protected boolean closeVersion;

    /**
     * The effective date to set on the version.
     *
     * The format of the date is {@code yyyy-mm-dd}.
     *
     * If not Set - will use current day date only if {@link #closeVersion}
     * flag is on.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.effectiveDate")
    protected String effectiveDate;

    /**
     * The status to set on the version amoung values {@code open, lock, closed}.
     *
     * If not Set - will use the {@code closed} value only if
     * {@link #closeVersion} flag is on.
     *
     * @since 1.2.1
     */
    @Parameter(property = "redmine.versionStatus")
    protected String versionStatus;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.skipUpdateVersion", defaultValue = "false")
    protected boolean skipUpdateVersion;

    /**
     * A flag to test plugin but send nothing to redmine.
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * A flag to restrict only one run in a build (for multi-module context).
     *
     * @since 1.0.0
     */
    @Parameter(property = "redmine.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 1.6
     */
    @Parameter(property = "redmine.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    ///////////////////////////////////////////////////////////////////////////
    /// Mojo internal attributes
    ///////////////////////////////////////////////////////////////////////////

    /** effective date to set */
    private Date date;

    /** flag to mark if a runOnce goal was done */
    protected boolean runOnceDone;

    /**
     * New status to apply (stay null if nothing is asked).
     *
     * @since 1.2.1
     */
    private VersionStatusEnum newVersionStatus;

    public UpdateVersionMojo() {
        super(true, true);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// DryRunAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isDryRun() {
        return dryRun;
    }

    @Override
    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// SkipOrRunOnlyOnceAware
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public String getSkipProperty() {
        return "skipUpdateVersion";
    }

    @Override
    public boolean isGoalSkip() {
        return skipUpdateVersion;
    }

    @Override
    public boolean isRunOnce() {
        return runOnce;
    }

    @Override
    public boolean isRunOnlyOnRoot() {
        return runOnlyOnRoot;
    }

    @Override
    public boolean isRunOnceDone() {
        return runOnceDone;
    }

    @Override
    public boolean checkRunOnceDone() {
        // compute unique key
        StringBuilder buffer = new StringBuilder("update-version");
        buffer.append("##").append(projectId);
        buffer.append("##").append(versionId);
        buffer.append("##").append(releaseVersion);
        buffer.append("##").append(versionDescription);
        buffer.append("##").append(versionStatus);

        String key = buffer.toString();

        return !needInvoke(runOnce, false, key);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        if (StringUtils.isBlank(versionId)) {
            throw new MojoExecutionException("required a versionId parameter");
        }

        versionId = PluginHelper.removeSnapshotSuffix(versionId);

        if (runOnceDone = isRunOnce() && checkRunOnceDone()) {
            return;
        }

        if (StringUtils.isNotBlank(effectiveDate)) {
            try {
                date = dateFormat.parse(effectiveDate);
            } catch (ParseException e) {
                throw new MojoExecutionException(
                        "could not parse effectivate date " + effectiveDate +
                        " for reason " + e.getMessage(), e
                );
            }
        } else if (closeVersion) {
            date = new Date();
        }

        if (closeVersion) {

            // by default use the closed status when closing a version
            newVersionStatus = VersionStatusEnum.closed;
        }

        if (StringUtils.isNotBlank(versionStatus)) {
            try {
                newVersionStatus = VersionStatusEnum.valueOf(versionStatus);
            } catch (IllegalArgumentException e) {
                throw new MojoExecutionException(
                        "could not parse status " + versionStatus +
                        " for reason " + e.getMessage() +
                        ", should be one of values : " +
                        Arrays.toString(VersionStatusEnum.values()), e
                );
            }

            if (closeVersion) {
                // warns user

                if (getLog().isWarnEnabled()) {
                    getLog().warn("While using the closeVersion flag, you " +
                                  "should not set also the version status " +
                                  ": [" + versionStatus + "]");
                }
            }
        }
        super.init();

    }

    @Override
    protected void doAction() throws Exception {

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no data will be send!\n");
        }

        // get project version
        Version v = getProjectVersion(versionId);

        // is version need to be created ?
        boolean needCreateVersion = v == null;

        if (needCreateVersion) {

            v = new Version();
            v.setName(versionId);
        }

        if (StringUtils.isNotBlank(versionDescription)) {
            v.setDescription(versionDescription.trim());
        }

        if (date != null) {
            v.setEffectiveDate(date);
        }

        if (newVersionStatus != null) {

            // change status of the version
            if (isVerbose()) {
                getLog().info("Will set status " + newVersionStatus);

            }
            v.setStatus(newVersionStatus.name());
        }

        releaseVersion = v;

        // prepare version
        if (needCreateVersion) {

            // create version
            getLog().info("create version " + releaseVersion.getName());

            if (!dryRun) {
                service.addVersion(projectId, releaseVersion);
            }

        } else {

            // update version
            getLog().info("udpate version " + releaseVersion.getName());

            if (!dryRun) {
                service.updateVersion(projectId, releaseVersion);
            }
        }

    }
}
