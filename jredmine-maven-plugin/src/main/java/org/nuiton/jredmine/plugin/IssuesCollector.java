/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin;

import org.apache.maven.plugin.logging.Log;
import org.nuiton.jredmine.model.IdAbles;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.Version;
import org.nuiton.jredmine.service.RedmineService;
import org.nuiton.jredmine.service.RedmineServiceException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A class to collect some issues due to given filters.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class IssuesCollector {

    /** logger */
    protected final Log log;

    /** verbose flag */
    protected final boolean verbose;

    /** to restrict on number of entries to collect */
    protected Integer maxEntries;

    /** the list of version ids collected */
    protected List<Integer> versionIds;

    /** the list of priority ids collected */
    protected List<Integer> priorityIds;

    /** the list of status ids collected */
    protected List<Integer> statusIds;

    /** the list of category ids collected */
    protected List<Integer> categoryIds;

    /** the list of tracker ids collected */
    protected List<Integer> trackerIds;

    /** the list of issues collected */
    protected Issue[] issues;

    public IssuesCollector(Log log, boolean verbose) {
        this.log = log;
        this.verbose = verbose;
    }

    /**
     * Collects the issues given the configuration passed.
     *
     * @param service redmine service to obtain datas
     * @param config  the collector configuration
     * @throws RedmineServiceException if any pb while retreave datas from redmine
     */
    public void collect(RedmineService service, IssueCollectorConfiguration config) throws RedmineServiceException {

        clearFilters();

        prepareFilters(service, config);

        // obtain versions to treate

        Version[] versions;
        String projectId = config.getProjectId();

        if (config.isVerbose()) {
            log.info("report project : " + projectId);
        }

        versions = service.getVersions(projectId);
        if (versionIds != null) {
            // filtre sur version
            versions = IdAbles.byIds(Version.class, versions, versionIds.toArray(new Integer[versionIds.size()]));
        }

        int nbIssues = 0;

        List<Issue> issueList = new ArrayList<Issue>();

        for (Version v : versions) {

            if (verbose) {
                log.info("discover all issues for version " + v.getName());
            }

            Issue[] currentIssues = service.getIssues(projectId, v.getName());
            for (Issue i : currentIssues) {

                if (canIncludeIssue(i)) {
                    issueList.add(i);
                    nbIssues++;
                    if (maxEntries != null && nbIssues >= maxEntries) {
                        break;
                    }
                }
            }
            if (maxEntries != null && nbIssues >= maxEntries) {
                break;
            }
        }

        issues = issueList.toArray(new Issue[issueList.size()]);
    }

    public Integer getMaxEntries() {
        return maxEntries;
    }

    public List<Integer> getPriorityIds() {
        return priorityIds;
    }

    public List<Integer> getStatusIds() {
        return statusIds;
    }

    public List<Integer> getCategoryIds() {
        return categoryIds;
    }

    public List<Integer> getVersionIds() {
        return versionIds;
    }

    public List<Integer> getTrackerIds() {
        return trackerIds;
    }

    public Issue[] getIssues() {
        return issues;
    }

    /**
     * clean all internal states.
     *
     * Note : this method is invoked at the begin of each
     * {@link #collect(RedmineService, org.nuiton.jredmine.plugin.IssueCollectorConfiguration)} method.
     */
    public void clearFilters() {
        maxEntries = null;
        priorityIds = null;
        statusIds = null;
        categoryIds = null;
        trackerIds = null;
        versionIds = null;
        issues = null;
    }

    /**
     * Prepare the collector filters due to the given collector configuration.
     *
     * @param service redmine service to obtain datas
     * @param config  the collector configuration
     * @throws RedmineServiceException if any pb whiile retreaving redmine's datas
     */
    protected void prepareFilters(RedmineService service, IssueCollectorConfiguration config) throws RedmineServiceException {

        // maxEntries filter

        maxEntries = config.getMaxEntries();
        if (maxEntries == 0) {
            // no limit
            maxEntries = null;
        } else {
            if (config.isVerbose()) {
                log.info("limit issues : " + maxEntries);
            }
        }

        String projectId = config.getProjectId();

        String tmp;

        // filter version

        tmp = config.getVersionNames();
        if (log.isDebugEnabled()) {
            log.debug("version to filter : " + tmp);
        }
        if (tmp != null && !tmp.trim().isEmpty()) {
            if (config.isOnlyCurrentVersion()) {
                // not possible
                log.warn("will not filter on versions while using the flag 'onlyCurrentVersion'");
            } else {
                versionIds = new ArrayList<Integer>();
                List<String> svs = Arrays.asList(tmp.split(","));
                for (Version v : service.getVersions(projectId)) {
                    if (svs.contains(v.getName())) {
                        // keep the version
                        versionIds.add(v.getId());
                        if (config.isVerbose()) {
                            log.info("use version " + v.getName());
                        }
                    }
                }
            }
        }

        if (config.isOnlyCurrentVersion()) {
            Version currentVersion = service.getVersion(projectId, config.getVersionId());
            versionIds = Arrays.asList(currentVersion.getId());
        }

        // filter status

        tmp = config.getStatusIds();
        statusIds = null;
        if (tmp != null && !tmp.trim().isEmpty()) {
            // filter on status
            statusIds = new ArrayList<Integer>();
            List<Integer> svs = toIntegerList(tmp.split(","));
            for (IssueStatus s : service.getIssueStatuses()) {
                int id = s.getId();
                if (svs.contains(id)) {
                    // keep this status
                    statusIds.add(id);
                    if (config.isVerbose()) {
                        log.info("use status " + s.getName());
                    }
                }
            }
        }

        // filter priority

        tmp = config.getPriorityIds();
        if (tmp != null && !tmp.trim().isEmpty()) {
            // filter on status
            priorityIds = new ArrayList<Integer>();
            List<Integer> svs = toIntegerList(tmp.split(","));
            for (IssuePriority s : service.getIssuePriorities()) {
                int id = s.getId();
                if (svs.contains(id)) {
                    // keep this status
                    priorityIds.add(id);
                    if (config.isVerbose()) {
                        log.info("use priority " + s.getName());
                    }
                }
            }
        }

        // filter priority

        tmp = config.getCategoryIds();
        if (tmp != null && !tmp.trim().isEmpty()) {
            // filter on status
            categoryIds = new ArrayList<Integer>();
            List<Integer> svs = toIntegerList(tmp.split(","));
            for (IssueCategory s : service.getIssueCategories(projectId)) {
                int id = s.getId();
                if (svs.contains(id)) {
                    // keep this status
                    categoryIds.add(id);
                    if (config.isVerbose()) {
                        log.info("use category " + s.getName());
                    }
                }
            }
        }

        // filter tracker
        tmp = config.getTrackerIds();
        if (tmp != null && !tmp.trim().isEmpty()) {
            // filter on status
            trackerIds = new ArrayList<Integer>();
            List<Integer> svs = toIntegerList(tmp.split(","));
            for (Tracker s : service.getTrackers(projectId)) {
                int id = s.getId();
                if (svs.contains(id)) {
                    // keep this status
                    trackerIds.add(id);
                    if (config.isVerbose()) {
                        log.info("use tracker " + s.getName());
                    }
                }
            }
        }
    }

    /**
     * Test if a given issue can be collected.
     *
     * @param i the issue ti test
     * @return {@code true} if the issue can be collected, {@code false}
     *         otherwise.
     */
    protected boolean canIncludeIssue(Issue i) {
        if (statusIds != null) {
            // filter on status
            if (!statusIds.contains(i.getStatusId())) {
                // reject status
                return false;
            }
        }
        if (priorityIds != null) {
            // filter on priority
            if (!priorityIds.contains(i.getPriorityId())) {
                // reject priority
                return false;
            }
        }
        if (categoryIds != null) {
            // filter on category
            if (!categoryIds.contains(i.getCategoryId())) {
                // reject category
                return false;
            }
        }
        if (trackerIds != null) {
            // filter on tracker
            if (!trackerIds.contains(i.getTrackerId())) {
                // reject tracker
                return false;
            }
        }

        // ok
        return true;
    }

    protected List<Integer> toIntegerList(String[] strIds) {
        int length = strIds.length;
        List<Integer> r = new ArrayList<Integer>(length);
        for (int i = 0; i < length; i++) {
            r.add(Integer.valueOf(strIds[i]));
        }
        return r;
    }
}
