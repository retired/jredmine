/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jredmine.plugin.report;

import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.reporting.MavenReportException;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.jredmine.model.I18nAble;
import org.nuiton.jredmine.model.Issue;
import org.nuiton.jredmine.model.IssueCategory;
import org.nuiton.jredmine.model.IssuePriority;
import org.nuiton.jredmine.model.IssueStatus;
import org.nuiton.jredmine.model.Tracker;
import org.nuiton.jredmine.model.User;
import org.nuiton.jredmine.model.Version;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * Generates a Redmine report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class IssueReportGenerator {

    /** The token any of urls denoting the base URL for the issue management. */
    private static final String URL_TOKEN = "%URL%";

    /** The token in {@link #issueLinkTemplate} denoting the issue ID. */
    private static final String ISSUE_TOKEN = "%ISSUE%";

    /** The token in {@link #versionLinkTemplate} denoting the version ID. */
    private static final String VERSION_TOKEN = "%VERSION%";

    /**
     * Universe of columns of the report.
     *
     * Can retreave th i18n header key by {@link #getI18nKey()} and have the
     * logic of each cell generation in the method
     * {@link #sinkInsideCell(Sink, org.nuiton.jredmine.plugin.report.IssueReportGenerator, Issue)}
     */
    public enum ReportColumn {

        key(false, "report.label.key") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                generator.constructIssueLink(issue, sink);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                throw new UnsupportedOperationException(name() + "does not supports grouping");
            }
        },
        summary(false, "report.label.summary") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                generator.sinkRawText(sink, issue.getDescription());
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                throw new UnsupportedOperationException(name() + "does not supports grouping");
            }
        },
        status(true, "report.label.status") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {

                int id = issue.getStatusId();
//                String t = id + "";
                IssueStatus status = generator.getIssueStatus(id);
//                if (status == null) {
//                    t = id == 0 ? "-" : t;
//                } else {
//                    t = status.getName();
//                }
                generator.sinkI18nAble(sink, id, status);

            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getStatusId();
            }
        },
        assignee(true, "report.label.by") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getAssignedToId();
                User u = generator.getUser(id);
                String t = issue.getAssignedToId() + "";
                if (u == null) {
                    generator.sinkRawText(sink, id == 0 ? " - " : t);
                } else {
                    // ajout d'un lien (mailto:)
                    sink.link("mailto:" + u.getMail());
                    sink.text(u.getLogin());
                    sink.link_();
                }
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getAssignedToId();
            }
        },
        reporter(true, "report.label.reporter") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getAuthorId();
                User u = generator.getUser(id);
                String t = issue.getAuthorId() + "";
                if (u == null) {
                    generator.sinkRawText(sink, id == 0 ? " - " : t);
                } else {
                    // ajout d'un lien (mailto:)
                    sink.link("mailto:" + u.getMail());
                    sink.text(u.getLogin());
                    sink.link_();
                }
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getAuthorId();
            }
        },
        tracker(true, "report.label.type") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getTrackerId();
                Tracker status = generator.getTracker(id);
                generator.sinkI18nAble(sink, id, status);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getTrackerId();
            }
        },
        priority(true, "report.label.priority") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getPriorityId();
                IssuePriority p = generator.getIssuePriority(id);
                generator.sinkI18nAble(sink, id, p);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getPriorityId();
            }
        },
        version(true, "report.label.version") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getFixedVersionId();
                Version v2 = generator.getVersion(id);
                generator.sinkI18nAble(sink, id, v2);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getFixedVersionId();
            }
        },
        category(true, "report.label.category") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                int id = issue.getCategoryId();
                IssueCategory status = generator.getIssueCategory(id);
                generator.sinkI18nAble(sink, id, status);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                return issue.getCategoryId();
            }
        },
        createdon(false, "report.label.created") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                Date d = issue.getCreatedOn();
                generator.sinkDate(sink, d);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                throw new UnsupportedOperationException(name() + "does not supports grouping");
            }
        },
        updatedon(false, "report.label.updated") {
            @Override
            public void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue) {
                Date d = issue.getUpdatedOn();
                generator.sinkDate(sink, d);
            }

            @Override
            protected Integer getGroupId(Issue issue) {
                throw new UnsupportedOperationException(name() + "does not supports grouping");
            }
        };

        private boolean canGroup;

        private String i18nKey;

        ReportColumn(boolean canGroup, String i18nKey) {
            this.canGroup = canGroup;
            this.i18nKey = i18nKey;
        }

        public boolean isCanGroup() {
            return canGroup;
        }

        public String getI18nKey() {
            return i18nKey;
        }

        public abstract void sinkInsideCell(Sink sink, IssueReportGenerator generator, Issue issue);

        protected abstract Integer getGroupId(Issue issue);

        public void collectGroups(Issue[] issues, Map<Integer, List<Issue>> groups) {
            for (Issue issue : issues) {
                Integer id = getGroupId(issue);
                List<Issue> c = groups.get(id);
                if (c == null) {
                    c = new ArrayList<Issue>();
                    groups.put(id, c);
                }
                c.add(issue);
            }
        }
    }

    private ReportColumn[] columns;

    private ReportColumn groupColumn;

    private String url;

    private String issueLinkTemplate;

    private String versionLinkTemplate;

    private Issue[] issues;

    private Map<Integer, IssueCategory> issueCategories;

    private Map<Integer, IssuePriority> issuePriorities;

    private Map<Integer, Version> versions;

    private Map<Integer, IssueStatus> issueStatuses;

    private Map<Integer, User> users;

    private Map<Integer, Tracker> trackers;

    private DateFormat dateFormat;

    public IssueReportGenerator() {
    }

    /**
     * @param log         the logger
     * @param columnNames The names of the columns to include in the report
     * @param groupBy     the group by column name (or null if not grouped)
     * @throws MavenReportException if any pb
     */
    public IssueReportGenerator(Log log, String columnNames, String groupBy)
            throws MavenReportException {

        String[] columnNamesArray = columnNames.split(",");
        int nbCols = columnNamesArray.length;
        List<ReportColumn> tmp = new ArrayList<ReportColumn>(nbCols);
        for (String col : columnNames.split(",")) {
            try {
                ReportColumn valueOf = ReportColumn.valueOf(col.trim().toLowerCase());
                tmp.add(valueOf);
            } catch (Exception e) {
                log.warn(col + " is an unkown column name, authorized : " + Arrays.toString(ReportColumn.values()));
                if (log.isDebugEnabled()) {
                    log.debug(e);
                }
            }
        }
        if (tmp.isEmpty()) {
            // This can happen if the user has configured column names and they are all invalid
            throw new MavenReportException(
                    "maven-redmine-plugin: None of the configured columnNames '" + columnNames + "' are valid.");
        }
        if (groupBy != null && !groupBy.trim().isEmpty()) {
            try {
                groupColumn = ReportColumn.valueOf(groupBy.trim().toLowerCase());

                if (!groupColumn.isCanGroup()) {
                    log.warn(groupColumn + " is not a grouping columne, grouping will be skip.");
                    groupColumn = null;
                } else {
                    // remove the group column from the table
                    tmp.remove(groupColumn);
                }

                log.info("group by " + groupColumn);

            } catch (Exception e) {
                log.warn(groupBy + " is an unkown group column name, grouping will be skip.");
                if (log.isDebugEnabled()) {
                    log.debug(e);
                }
            }
        }

        columns = tmp.toArray(new ReportColumn[tmp.size()]);
    }

    public void doGenerateEmptyReport(ResourceBundle bundle, Sink sink) {
        sinkBeginReport(sink, bundle);

        sink.text(bundle.getString("report.empty"));

        sinkEndReport(sink);
    }

    public void doGenerateReport(ResourceBundle bundle, Sink sink)
            throws MojoExecutionException {

        if (issues != null && issues.length == 0) {
            throw new MojoExecutionException(
                    "maven-redmine-plugin: Can not generate report if no issues, use the doGenerateEmptyReport instead");
        }

        String d = bundle.getString("report.date.format");

        dateFormat = new SimpleDateFormat(d);

        sinkBeginReport(sink, bundle);

        if (groupColumn == null) {

            sink.table();

            constructHeaderRow(sink, bundle);

            constructDetailRows(sink, issues);

            sink.table_();

        } else {

            // group data

            Map<Integer, List<Issue>> groups = new TreeMap<Integer, List<Issue>>();

            groupColumn.collectGroups(issues, groups);

            for (Entry<Integer, List<Issue>> e : groups.entrySet()) {

                List<Issue> currentIssues = e.getValue();

                sink.sectionTitle2();

                sinkRawText(sink, bundle.getString(groupColumn.getI18nKey()) + " : ");

                groupColumn.sinkInsideCell(sink, this, currentIssues.get(0));

                sink.sectionTitle2_();

                sink.table();

                constructHeaderRow(sink, bundle);

                constructDetailRows(sink, currentIssues.toArray(new Issue[currentIssues.size()]));

                sink.table_();
            }

        }

        sinkEndReport(sink);
    }

    /**
     * Checks whether links to the issues can be generated.
     *
     * @return {@code true} if issue links can be generated, {@code false} otherwise.
     */
    public boolean canGenerateIssueLinks() {
        return !StringUtils.isEmpty(issueLinkTemplate) && (!StringUtils.isBlank(getUrl()) || !issueLinkTemplate.contains(URL_TOKEN));
    }

    /**
     * Checks whether links to the issues can be generated.
     *
     * @return {@code true} if issue links can be generated, {@code false} otherwise.
     */
    public boolean canGenerateVersionLinks() {
        return !StringUtils.isEmpty(versionLinkTemplate) && (!StringUtils.isBlank(getUrl()) || !versionLinkTemplate.contains(URL_TOKEN));
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setIssueLinkTemplate(String issueLinkTemplate) {
        this.issueLinkTemplate = issueLinkTemplate;
    }

    public void setVersionLinkTemplate(String versionLinkTemplate) {
        this.versionLinkTemplate = versionLinkTemplate;
    }

    public void setIssueCategories(IssueCategory[] issueCategories) {
        Map<Integer, IssueCategory> t = new TreeMap<Integer, IssueCategory>();
        for (IssueCategory s : issueCategories) {
            t.put(s.getId(), s);
        }
        this.issueCategories = t;
    }

    public void setIssueStatuses(IssueStatus[] issueStatuses) {
        Map<Integer, IssueStatus> t = new TreeMap<Integer, IssueStatus>();
        for (IssueStatus s : issueStatuses) {
            t.put(s.getId(), s);
        }
        this.issueStatuses = t;
    }

    public void setIssuePriorities(IssuePriority[] issuePriorities) {
        Map<Integer, IssuePriority> t = new TreeMap<Integer, IssuePriority>();
        for (IssuePriority s : issuePriorities) {
            t.put(s.getId(), s);
        }
        this.issuePriorities = t;
    }

    public void setUsers(User[] users) {
        Map<Integer, User> t = new TreeMap<Integer, User>();
        for (User s : users) {
            t.put(s.getId(), s);
        }
        this.users = t;
    }

    public void setVersions(Version[] users) {
        Map<Integer, Version> t = new TreeMap<Integer, Version>();
        for (Version s : users) {
            t.put(s.getId(), s);
        }
        versions = t;
    }

    public void setTrackers(Tracker[] users) {
        Map<Integer, Tracker> t = new TreeMap<Integer, Tracker>();
        for (Tracker s : users) {
            t.put(s.getId(), s);
        }
        trackers = t;
    }

    public void setIssues(Issue[] issues) {
        this.issues = issues;
    }

    protected void constructHeaderRow(Sink sink, ResourceBundle bundle) {

        sink.tableRow();

        for (ReportColumn c : columns) {
            sinkHeader(sink, bundle.getString(c.getI18nKey()));
        }

        sink.tableRow_();
    }

    protected void constructDetailRows(Sink sink, Issue[] issues) {

        for (Issue issue : issues) {

            sink.tableRow();

            for (ReportColumn col : columns) {

                sink.tableCell();

                col.sinkInsideCell(sink, this, issue);

                sink.tableCell_();

            }

            sink.tableRow_();
        }
    }

    protected String parseIssueLink(String issue) {
        String parseLink = parseIssueLink(issueLinkTemplate, url, issue);
        return parseLink;
    }

    protected String parseVersionLink(String issue) {
        String parseLink;
        String issueLink = versionLinkTemplate;
        parseLink = issueLink.replaceFirst(VERSION_TOKEN, issue);

        if (parseLink.contains(URL_TOKEN)) {
            parseLink = parseLink.replaceFirst(URL_TOKEN, url);
        }

        return parseLink;
    }

    protected void constructIssueLink(Issue issue, Sink sink) {
        String id = issue.getId() + "";

        if (StringUtils.isNotEmpty(id)) {

            sink.link(parseIssueLink(id));

            sink.text(issue.getSubject());

            sink.link_();
        }
    }

    protected void constructVersionLink(Version v, Sink sink, String prefix) {
        String id = v.getId() + "";

        if (StringUtils.isNotEmpty(id)) {

            sink.link(parseVersionLink(id));

            sink.text(prefix + " " + v.getName());

            sink.link_();
        }
    }

    protected boolean isGroup() {
        return groupColumn != null;
    }

    protected IssueStatus getIssueStatus(int id) {
        return issueStatuses == null ? null : issueStatuses.get(id);
    }

    protected IssueCategory getIssueCategory(int id) {
        return issueCategories == null ? null : issueCategories.get(id);
    }

    protected IssuePriority getIssuePriority(int id) {
        return issuePriorities == null ? null : issuePriorities.get(id);
    }

    protected Version getVersion(int id) {
        return versions == null ? null : versions.get(id);
    }

    protected User getUser(int id) {
        return users == null ? null : users.get(id);
    }

    protected Tracker getTracker(int id) {
        return trackers == null ? null : trackers.get(id);
    }

    protected void sinkBeginReport(Sink sink, ResourceBundle bundle) {
        sink.head();

        String title;
        if (isGroup()) {
            title = bundle.getString("report.header-" + groupColumn.name());
        } else {
            title = bundle.getString("report.header");
        }

        sink.title();

        sink.text(title);

        sink.title_();

        sink.head_();

        sink.body();

        sinkSectionTitle1(sink, title);

    }

    protected void sinkEndReport(Sink sink) {
        sink.body_();

        sink.flush();

        sink.close();
    }

    protected void sinkHeader(Sink sink, String header) {
        sink.tableHeaderCell();

        sink.text(header);

        sink.tableHeaderCell_();
    }

    protected void sinkRawText(Sink sink, String text) {

        if (text != null) {
            sink.rawText(text);
        } else {
            sink.rawText("&nbsp;");
        }
    }

    protected void sinkDate(Sink sink, Date d) {
        String text;
        if (d == null) {
            text = " - ";
        } else {
            text = dateFormat.format(d);
        }
        sinkRawText(sink, text);
    }

    protected void sinkI18nAble(Sink sink, int id, I18nAble obj) {
        String t;
        if (obj == null) {
            t = id == 0 ? " - " : id + "";
        } else {
            t = obj.getName();
            if (obj instanceof Version) {
                constructVersionLink((Version) obj, sink, "");
                return;
            }
        }
        sinkRawText(sink, t);
    }

    protected void sinkSectionTitle1(Sink sink, String text) {
        sink.sectionTitle1();

        sink.text(text);

        sink.sectionTitle1_();
    }

    public static  String parseIssueLink(String issueLinkTemplate, String url, String issue) {
        String parseLink;
        String issueLink = issueLinkTemplate;
        parseLink = issueLink.replaceFirst(IssueReportGenerator.ISSUE_TOKEN, issue);

        if (parseLink.contains(IssueReportGenerator.URL_TOKEN)) {
            parseLink = parseLink.replaceFirst(IssueReportGenerator.URL_TOKEN, url);
        }

        return parseLink;
    }
}
