/*
 * #%L
 * JRedmine :: Maven plugin
 * %%
 * Copyright (C) 2009 - 2016 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
assert new File(basedir, 'target/generated-sources/changes/changes.xml').exists();
assert new File(basedir, 'target/generated-sources/announcement/release-news-announcement.vm').exists();
assert new File(basedir, 'target/generated-sources/announcement/release-email-announcement.vm').exists();
assert new File(basedir, 'target/collect.properties').exists();

assert !new File(basedir, 'module1/target/generated-sources/announcement/release-news-announcement.vm').exists();
assert !new File(basedir, 'module1/target/generated-sources/announcement/release-email-announcement.vm').exists();
assert !new File(basedir, 'module1/target/collect.properties').exists();

assert !new File(basedir, 'module2/target/generated-sources/announcement/release-news-announcement.vm').exists();
assert !new File(basedir, 'module2/target/generated-sources/announcement/release-email-announcement.vm').exists();
assert !new File(basedir, 'module2/target/collect.properties').exists();

assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--pom/pom.xml').exists();
assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--pom/pom.txt').exists();
assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--module1/pom.xml').exists();
assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--module1/module1.txt').exists();
assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--module2/module2-0.jar').exists();
assert new File(basedir, 'target/collect/org.nuiton.jredmine.test.multi--module2/module2.txt').exists();

//content = new File(basedir, 'target/generated-sources/announcement/announcement.vm').text;

//assert content.contains( 'Test report.' );

return true;
